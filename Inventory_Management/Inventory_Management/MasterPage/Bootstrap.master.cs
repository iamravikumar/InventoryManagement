﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class MasterPage_Bootstrap : System.Web.UI.MasterPage
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["PointofSaleConstr"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Cookies["InventMgtCookies"] == null)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            else
            {
                // lbDate.Text = DateTime.Now.ToString("ddd dd-MMM-yyyy hh:mm");
                //  lblusername.Text = Request.QueryString["userID"];

                //For Multiple values in single cookie
                string cookiesValues;
                cookiesValues = Request.Cookies["InventMgtCookies"]["UserID"];
                cookiesValues = cookiesValues + " " + Request.Cookies["InventMgtCookies"]["ShopID"];
                ImgUser.ImageUrl = Request.Cookies["InventMgtCookies"]["UserPhoto"];
                lblusername.Text = Request.Cookies["InventMgtCookies"]["UserID"];
              //  ImgUser2.ImageUrl = Request.Cookies["InventMgtCookies"]["UserPhoto"];
               // lblusername2.Text = Request.Cookies["InventMgtCookies"]["UserID"];

                string pageName = Page.Request.Url.Segments[Page.Request.Url.Segments.Length - 1];
                UserRole(lblusername.Text, pageName);

                TaskNotification(Request.Cookies["InventMgtCookies"]["UserID"]);
                AccessLogDataBind();

            }
        }

    }

    protected void UserRole(string UserName, string PageName)
    {

        //User Role Page Access Permission
        string ChkExist = "";
        using (SqlConnection con = new SqlConnection(ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("SP_POS_UserRole_Access", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@PageName", PageName);

                //outPut Parameter 
                cmd.Parameters.Add("@OutPut", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                ChkExist = cmd.Parameters["@OutPut"].Value.ToString();
            }
        }

        //Access Yes or Not
        if ((String.Compare(ChkExist, "Y") == 0))
        {
            //Access Allow
        }
        else
        {
            //Access Deny
            // lblusername.Text = "Access Deny";
            //  Response.Redirect("~/AccessDeny.aspx");
            Response.Redirect("~/ErrorPage/404.aspx");
        }
    }

    //Task Notification
    public void TaskNotification(string UserID)
    {

        SqlConnection cn = new SqlConnection(ConnectionString);
        SqlCommand cmd = new SqlCommand("SP_POS_DataBind_TaskNotification", cn);
        cmd.CommandType = CommandType.StoredProcedure;
        cn.Open();
        cmd.Parameters.AddWithValue("@userID", UserID);

        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.HasRows)
        {
            rd.Read();
            lbltask.Text = rd["Description"].ToString();
        }
        cn.Close();
    }

    // ///////  Item list Databind
    public void AccessLogDataBind()
    {
        try
        {
            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand("SP_INV_DataBind_AccessLogByUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", Request.Cookies["InventMgtCookies"]["UserID"]);
            cn.Open();

            DTLogs.DataSource = cmd.ExecuteReader();
            DTLogs.DataBind();
            cn.Close();

            lblnoticount.Text = Convert.ToString(DTLogs.Items.Count.ToString());

        }
        catch
        {
          //  lbtotalRow.Text = "No Records Found";
        }
    }

    protected void LinklogOut_Click(object sender, EventArgs e)
    {
        //   Session.Clear();
        ////  Response.Redirect("Login.aspx");

        if (Request.Cookies["InventMgtCookies"] != null)
        {
            Response.Cookies["InventMgtCookies"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("../Login.aspx");  //to refresh the page
        }
    }
}
