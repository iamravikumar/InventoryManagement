﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;

public partial class Items_UploadItems : System.Web.UI.Page
{
   
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }


    

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string csvPath = Server.MapPath("~/CSVFile/") + Path.GetFileName(FileUpload1.PostedFile.FileName);
            FileUpload1.SaveAs(csvPath);

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[13] 
            {               
                new DataColumn("ID", typeof(int)),
                new DataColumn("ItemCode", typeof(string)),
                new DataColumn("ItemName", typeof(string)),
                new DataColumn("PurchasePrice", typeof(decimal)),
                new DataColumn("RetailPrice", typeof(decimal)),
                new DataColumn("ItemQty", typeof(decimal)),
                new DataColumn("ItemCategory", typeof(string)),
                new DataColumn("Tax", typeof(decimal)),
                new DataColumn("Discount", typeof(decimal)),                
                new DataColumn("LogBy", typeof(string)),
                new DataColumn("Lastupdateby", typeof(string)),
                new DataColumn("Photo",typeof(string)) ,
                new DataColumn("Description", typeof(string))
            });
            

            string csvData = File.ReadAllText(csvPath);
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    dt.Rows.Add();
                    int i = 0;
                    foreach (string cell in row.Split(','))
                    {
                        dt.Rows[dt.Rows.Count - 1][i] = cell;
                        i++;
                    }
                }
            }

            string consString = ConfigurationManager.ConnectionStrings["PointofSaleConstr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //Set the database table name
                    sqlBulkCopy.DestinationTableName = "tbl_Item";
                    con.Open();
                    sqlBulkCopy.WriteToServer(dt);
                    con.Close();
                    lblmessage.Text = "Successfully Upload";
                }
            } 
        }
        catch (Exception ex)
        {
            lblmessage.Text = "Already uploaded <br/>"+ ex.Message;
        }
    }
 
}