SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserRole](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[UserName] [varchar](250) NULL,
	[PageID] [int] NULL,
	[PageName] [varchar](250) NULL,
	[Status] [int] NULL,
	[lastUpdate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 =Active , 2 = inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_UserRole', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_user]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_user](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Fname] [varchar](450) NULL,
	[LName] [varchar](450) NULL,
	[UserPhone] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
	[UserAddress] [varchar](550) NULL,
	[Supervisor] [varchar](450) NULL,
	[UserID] [varchar](50) NOT NULL,
	[Password] [varchar](110) NULL,
	[Role] [int] NULL,
	[Designation] [varchar](450) NULL,
	[Department] [varchar](110) NULL,
	[DateofBirth] [varchar](110) NULL,
	[Logtime] [datetime] NULL,
	[Logby] [varchar](110) NULL,
	[Lastupdate] [datetime] NULL,
	[LastupdateBy] [varchar](110) NULL,
	[Status] [int] NULL,
	[ShopID] [varchar](50) NULL,
	[User_Photo] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_user] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 =Active 2 =inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_user', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_terminalLocation]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_terminalLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyID] [bigint] NULL,
	[TerminalID] [varchar](250) NOT NULL,
	[Location] [varchar](450) NULL,
	[Phone] [varchar](450) NULL,
	[EmailAddress] [varchar](450) NULL,
	[VatRate] [decimal](18, 3) NULL,
	[VATRegistration] [varchar](450) NULL,
	[LastUpdate] [datetime] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_tbl_terminalLocation] PRIMARY KEY CLUSTERED 
(
	[TerminalID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active 2 = Inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_terminalLocation', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_tasklist]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_tasklist](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[task_title] [varchar](150) NOT NULL,
	[task_Description] [varchar](550) NULL,
	[taskfrom] [varchar](50) NOT NULL,
	[taskto] [varchar](50) NOT NULL,
	[taskdate] [date] NULL,
	[logtime] [datetime] NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = active , 2 = inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_tasklist', @level2type=N'COLUMN',@level2name=N'status'
GO
/****** Object:  Table [dbo].[tbl_supplier]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_supplier](
	[ID] [bigint] IDENTITY(59101,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Phone] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
	[Address] [varchar](450) NULL,
	[City] [varchar](150) NULL,
	[Type] [varchar](150) NULL,
	[CompanyName] [varchar](150) NULL,
	[Status] [int] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](150) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_settings]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_settings](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](250) NULL,
	[CompanyAddress] [varchar](450) NULL,
	[Phone] [varchar](450) NULL,
	[EmailAddress] [varchar](450) NULL,
	[WebAddress] [varchar](450) NULL,
	[VatRate] [decimal](18, 2) NULL,
	[VATRegistration] [varchar](450) NULL,
	[Footermsg] [nvarchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](250) NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_SalesPayment]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SalesPayment](
	[ID] [bigint] IDENTITY(1001,1) NOT NULL,
	[SalesQty] [decimal](18, 2) NOT NULL,
	[Subtotal] [decimal](18, 2) NOT NULL,
	[Vat] [decimal](18, 2) NULL,
	[totalpayable] [decimal](18, 2) NOT NULL,
	[payType] [varchar](150) NULL,
	[paidAmount] [decimal](18, 2) NOT NULL,
	[changeAmount] [decimal](18, 2) NULL,
	[dueAmount] [decimal](18, 2) NULL,
	[note] [varchar](450) NULL,
	[ShopId] [varchar](50) NULL,
	[CustID] [bigint] NULL,
	[CustName] [varchar](150) NULL,
	[CustContact] [varchar](150) NULL,
	[ServedBy] [varchar](150) NULL,
	[Logtime] [datetime] NULL,
	[Logdate] [date] NULL,
	[ordedate] [varchar](150) NULL,
	[trxtype] [varchar](150) NULL,
	[order_status] [varchar](150) NULL,
	[payment_status] [varchar](150) NULL,
	[comment] [varchar](150) NULL,
	[discount] [decimal](18, 2) NULL,
	[dsirate] [decimal](18, 2) NULL,
	[vatrate] [decimal](18, 2) NULL,
	[shippingFee] [varchar](150) NULL,
	[shippingaddress] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SalesID/Invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_SalesPayment', @level2type=N'COLUMN',@level2name=N'ID'
GO
/****** Object:  Table [dbo].[tbl_salespaid]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_salespaid](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SP_ID] [bigint] NULL,
	[trxtype] [varchar](50) NULL,
	[paydate] [varchar](50) NULL,
	[paidamount] [decimal](18, 2) NULL,
	[paytype] [varchar](50) NULL,
	[receivedby] [varchar](50) NULL,
	[logtime] [datetime] NULL,
	[logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_sales]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_sales](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](150) NULL,
	[ItemName] [varchar](350) NULL,
	[Qty] [decimal](18, 2) NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[DiscRate] [decimal](18, 2) NULL,
	[total] [decimal](18, 2) NULL,
	[Profit] [decimal](18, 2) NULL,
	[SP_ID] [bigint] NULL,
	[logtime] [datetime] NULL,
	[Logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ReturnPayment]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReturnPayment](
	[ID] [bigint] IDENTITY(4001,1) NOT NULL,
	[ReturnQty] [decimal](18, 2) NULL,
	[Subtotal] [decimal](18, 0) NULL,
	[Vat] [decimal](18, 2) NULL,
	[totalReturnable] [decimal](18, 2) NULL,
	[payType] [varchar](150) NULL,
	[ReturnAmount] [decimal](18, 2) NULL,
	[changeAmount] [decimal](18, 2) NULL,
	[dueAmount] [decimal](18, 2) NULL,
	[note] [varchar](450) NULL,
	[ShopId] [varchar](50) NULL,
	[CustID] [bigint] NULL,
	[CustName] [varchar](150) NULL,
	[CustContact] [varchar](150) NULL,
	[ServedBy] [varchar](150) NULL,
	[SalesInvoiceID] [bigint] NULL,
	[Logtime] [datetime] NULL,
	[Logdate] [date] NULL,
	[ordedate] [varchar](150) NULL,
	[trxtype] [varchar](150) NULL,
	[order_status] [varchar](150) NULL,
	[payment_status] [varchar](150) NULL,
	[comment] [varchar](150) NULL,
	[discount] [decimal](18, 2) NULL,
	[dsirate] [decimal](18, 2) NULL,
	[vatrate] [decimal](18, 2) NULL,
	[shippingFee] [varchar](150) NULL,
	[shippingaddress] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SalesID/Invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ReturnPayment', @level2type=N'COLUMN',@level2name=N'ID'
GO
/****** Object:  Table [dbo].[tbl_Return]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Return](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](150) NULL,
	[ItemName] [varchar](350) NULL,
	[Qty] [decimal](18, 2) NULL,
	[Price] [decimal](18, 2) NULL,
	[DiscRate] [decimal](18, 2) NULL,
	[total] [decimal](18, 2) NULL,
	[RP_ID] [bigint] NULL,
	[logtime] [datetime] NULL,
	[Logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_purchase_payment]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_purchase_payment](
	[purchaseCode] [bigint] IDENTITY(99101,1) NOT NULL,
	[supplierid] [varchar](450) NOT NULL,
	[supplier] [varchar](450) NOT NULL,
	[puchase_date] [varchar](450) NOT NULL,
	[itemQty] [decimal](18, 2) NOT NULL,
	[total] [decimal](18, 2) NOT NULL,
	[payment] [decimal](18, 2) NOT NULL,
	[method] [varchar](450) NOT NULL,
	[Comment] [varchar](550) NOT NULL,
	[ShopId] [varchar](50) NOT NULL,
	[purchaseby] [varchar](450) NOT NULL,
	[logtime] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_purchase]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_purchase](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[supplierid] [varchar](450) NULL,
	[purchaseCode] [varchar](450) NULL,
	[puchase_date] [varchar](450) NULL,
	[product_id] [varchar](450) NULL,
	[product_name] [varchar](450) NOT NULL,
	[qty] [decimal](18, 2) NOT NULL,
	[unit_price] [decimal](18, 2) NOT NULL,
	[total] [decimal](18, 2) NOT NULL,
	[expiry_date] [varchar](450) NULL,
	[logtime] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Page]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Page](
	[PageID] [bigint] IDENTITY(101,1) NOT NULL,
	[PageName] [varchar](150) NULL,
	[PageUrl] [varchar](150) NULL,
	[Status] [int] NULL,
	[Module] [varchar](150) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Item]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Item](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](450) NOT NULL,
	[ItemName] [varchar](450) NOT NULL,
	[PurchasePrice] [decimal](18, 2) NOT NULL,
	[RetailPrice] [decimal](18, 2) NOT NULL,
	[ItemQty] [decimal](18, 2) NULL,
	[ItemCategory] [varchar](450) NOT NULL,
	[Tax] [decimal](18, 2) NULL,
	[Discount] [decimal](18, 2) NULL,
	[LogBy] [varchar](150) NULL,
	[Lastupdateby] [varchar](150) NULL,
	[Photo] [varchar](350) NULL,
	[Description] [varchar](499) NULL,
	[Status] [int] NULL,
	[Lastupdate] [datetime] NULL,
	[Logtime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Item] PRIMARY KEY CLUSTERED 
(
	[ItemCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = active  , 2 = Inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Item', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_hit_counter]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_hit_counter](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[userID] [varchar](150) NULL,
	[datetime] [datetime] NULL,
	[timelog] [time](7) NULL,
	[IPaddress] [varchar](150) NULL,
	[count] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_empSalary]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_empSalary](
	[ID] [bigint] IDENTITY(1101,1) NOT NULL,
	[Empid] [varchar](50) NULL,
	[saltype] [varchar](150) NULL,
	[salaryamount] [decimal](18, 2) NULL,
	[saldate] [varchar](50) NULL,
	[salmonth] [varchar](50) NULL,
	[salyr] [varchar](50) NULL,
	[note] [varchar](450) NULL,
	[logdate] [datetime] NULL,
	[paidby] [varchar](50) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_email]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_email](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[mailFrom] [varchar](150) NULL,
	[mailTo] [varchar](150) NULL,
	[Subject] [varchar](350) NULL,
	[mailbody] [text] NULL,
	[maildate] [datetime] NULL,
	[drafts] [int] NULL,
	[trash] [int] NULL,
	[status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Unread 2 = Read 3 = Draft 4 = trash/Delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_email', @level2type=N'COLUMN',@level2name=N'status'
GO
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustName] [varchar](250) NULL,
	[CustPhone] [varchar](150) NULL,
	[CustEmail] [varchar](150) NULL,
	[CustAddress] [varchar](450) NULL,
	[CustType] [varchar](150) NULL,
	[DiscountRate] [decimal](18, 2) NULL,
	[Status] [int] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](150) NULL,
	[CustID] [varchar](150) NOT NULL,
	[Password] [varchar](150) NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active
 ,  2 = inactive/Delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Customer', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_Category]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Category](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCategory] [varchar](50) NULL,
	[Status] [int] NOT NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](50) NULL,
	[Lastupdate] [datetime] NULL,
	[LastupdateBy] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1= active ;
2= inactive
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Category', @level2type=N'COLUMN',@level2name=N'Status'
GO
/****** Object:  Table [dbo].[tbl_accesslog]    Script Date: 07/09/2017 00:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_accesslog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](250) NULL,
	[Operations] [varchar](450) NULL,
	[datetime] [datetime] NULL,
	[timelog] [time](7) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SP_A_Language_chnage]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SP_A_Language_chnage]		  
AS
 	
					 -- do in existing table 
			--Alter TABLE test
			--(
			--		col1 VARCHAR(100) COLLATE Latin1_General_100_CI_AI,
			--		col2 VARCHAR(100) COLLATE Arabic_CI_AI_KS_WS,  -- this one for arabic language
			--		col3 NVARCHAR(100);
			--)
			--INSERT INTO  test VALUES(N'لا أتكلم العربية',N'لا أتكلم العربية',N'لا أتكلم العربية')
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_A_Note]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_A_Note] 	 
	 	     
AS
 
		--	Create Userpanel- >> Customer Search
		--                  -->> Items Search 
			
			
			
		---- Some Quote from Bill Gates	
		---			Your most unhappy customers are your greatest source of learning. -----  Bill Gates
		--			If you can't make it good, at least make it look good.
		----	“If you are born poor its not your mistake, But if you die poor its your mistake.”
GO
/****** Object:  StoredProcedure [dbo].[SP_mail_DataBind_mailBody]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_mail_DataBind_mailBody]	 
	@id	   int 
AS
BEGIN
		select mailbody ,Subject , mailFrom ,mailTo ,maildate from  tbl_email	 where [ID] = @id					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_mail_DataBind_index]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_mail_DataBind_index]
		 	     
AS
BEGIN
		select ID , [Subject]  from dbo.tbl_email order by ID desc	 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Load_ChartSales_ValueXY]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Load_ChartSales_ValueXY]	   
	 @Date1 varchar(111),
	 @Date2 varchar(111),
	 @ReportType int   -- 1 =  for Last 30 days 2 = for Custom report 	     
AS
BEGIN
		SET NOCOUNT ON;
		if(@ReportType = 1)
		Begin
			SELECT  CONVERT(date,[Logtime],113) as [DATE]  ,SUM([totalpayable]) as [Total]
			FROM tbl_SalesPayment		
		    WHERE CONVERT(date,[Logtime],113) between   GETDATE() - 30    and  GETDATE()
			group by CONVERT(date,[Logtime],113)		
		END
		ELSE
		BEGIN
			SELECT CONVERT(date,[Logtime],113) as [DATE]  ,SUM([totalpayable]) as [Total]
			FROM tbl_SalesPayment		
		    WHERE CONVERT(date,[Logtime],113) between @Date1 and @Date2
			group by CONVERT(date,[Logtime],113)
		END
			
			 						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Load_ChartProfit_ValueXY]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Load_ChartProfit_ValueXY]	   
	 @Date1 varchar(111),
	 @Date2 varchar(111)	,
	 @ReportType int   -- 1 =  for Last 30 days 2 = for Custom report 	     
AS
BEGIN
		SET NOCOUNT ON;
			if(@ReportType = 1)
		Begin
			SELECT   CONVERT(date,[Logtime],113) as [DATE] ,SUM(total) as [Total] ,SUM([Profit]) as Profit
			FROM tbl_sales		
		    WHERE CONVERT(date,[Logtime],113) between   GETDATE() - 30    and  GETDATE()
			group by CONVERT(date,[Logtime],113)
			
		END
		ELSE
		BEGIN
			SELECT   CONVERT(date,[Logtime],113) as [DATE] ,SUM(total) as [Total] ,SUM([Profit]) as Profit
			FROM tbl_sales		
		    WHERE CONVERT(date,[Logtime],113) between @Date1 and @Date2
			group by CONVERT(date,[Logtime],113)
		END
				
	 
			 						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Update_Supplier]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_INV_Update_Supplier]
		 @id			bigint
		,@Name			varchar(250)
		,@Phone			varchar(150)
		,@Email			varchar(150)
		,@Address		varchar(450)
		,@Type			varchar(150)		 
		,@LastUpdateBy	varchar(150)
		,@CompanyName	varchar(150)
		,@City			varchar(150)
	
AS
BEGIN
	Update   tbl_supplier set 
	Name  = @Name, Phone  = @Phone ,  Email = @Email , [Address] = @Address , City = @City ,
	[Type] = @Type  , CompanyName = @CompanyName , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(), [Status]  = 1 
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Update_changestatus]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Update_changestatus]        	 
	   @date 				varchar(50)
      ,@orderstatus			varchar(450)
      ,@payment_status		varchar(150)      
      ,@deliveryaddress		varchar(350) 
      ,@comment				varchar(490)
      ,@InvoiceNo			BIGINT
     -- ,@CustID				varchar(50)
     -- ,@CustName			varchar(450)
      ,@ServedBy			varchar(450)
       
AS
BEGIN		
		SET NOCOUNT ON; 	
		 
			Update  tbl_SalesPayment set 
			order_status = @orderstatus , payment_status = @payment_status , ordedate = @date
			,shippingaddress = @deliveryaddress , comment = @comment ,  ServedBy = 'ChangeStatus_' + @ServedBy
			Where  ID =  @InvoiceNo
			
			--- Tracking Log  
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'Change Status : ' + @orderstatus + ' | Invoice No: ' + CAST(@InvoiceNo as varchar(450))  )
			
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_Supplier]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_Supplier]
		 @Name			varchar(250)
		,@Phone			varchar(150)
		,@Email			varchar(250)
		,@Address		varchar(450)
		,@Type			varchar(150)		 
		,@LogBy			varchar(150)
		,@CompanyName	varchar(250)
		,@City			varchar(250)    
AS
BEGIN	
		Insert into	  tbl_supplier
		([Name] ,[Phone],[Email],[Address],[Type],CompanyName,  City ,[LogBy])
Values	(@Name , @Phone ,@Email ,@Address ,@Type,@CompanyName, @City , @LogBy)
				 
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_ReceivePayment]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_ReceivePayment]  -- used in Newsale and NewOrder form
	   @SalesQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
	  ,@discount		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalpayable	decimal(18,2)
      ,@payType			varchar(50)
      ,@paidAmount		decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)	 
	  ,@date 			varchar(50)
      ,@orderstatus		varchar(450)
      ,@paystatus		varchar(150)
      ,@trxtype			varchar(150) 	 
      ,@deliveryaddress			varchar(350)     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_SalesPayment
			([SalesQty],[Subtotal],[discount], [Vat] ,[totalpayable] ,[payType] , [paidAmount] ,[changeAmount] ,[dueAmount]
			,[comment] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy], 
			[ordedate],[order_status] ,[payment_status] ,  [trxtype], shippingaddress )
			values 
			(@SalesQty, @Subtotal ,@discount ,@Vat ,@totalpayable ,@payType ,@paidAmount ,@changeAmount ,
			@dueAmount,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy 
			,@date   ,@orderstatus ,@paystatus ,@trxtype , @deliveryaddress) 
			
			
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_SalesPayment order by ID desc)	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,@trxtype ,@date  ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )

			--- Tracking Log  Sales and order
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , @trxtype + ' Payment |  Invoice No: ' +  CAST(@spid as varchar(450)) + ' | Total amount ' +   CAST(@totalpayable as varchar(450)) ) 
			-- (@ServedBy , @trxtype + ' Payment |  Invoice No: ' + @spid + ' | Total amount ' + @totalpayable) 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_ReceiveDuePayment]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_ReceiveDuePayment]
	   @payType			varchar(150)
      ,@paidAmount		decimal(18,2)
      ,@ServedBy		varchar(150)	 
	  ,@date 			varchar(50)     
      ,@trxtype			varchar(150)    
      ,@spid			bigint   
      ,@dueAmount		decimal(18,2)
AS
BEGIN		
		SET NOCOUNT ON; 	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,@trxtype ,@date  , @paidAmount ,@payType  ,@ServedBy)
			
			--- Tracking Log   
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			 (@ServedBy , @trxtype + ' Payment | Invoice No ' + CAST(@spid as varchar(450)) )
			 
				 
			-- Update   tbl_SalesPayment table
			Update  tbl_SalesPayment set 
			dueAmount = (@dueAmount - @paidAmount)
			Where  ID =  @spid
			
		
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_PurchasePayment]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad  
		--- Email :			citkar@live.com
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_PurchasePayment]	
	   @total			decimal(18,2)
      ,@method			varchar(150)
      ,@paidAmount	    decimal(18,2)  
      ,@Comment			varchar(450)      
      ,@supplierid		varchar(50)
      ,@supplier		varchar(450)      
      ,@purchaseby		varchar(150)	 
	  ,@puchase_date 	varchar(50)  
	  ,@ShopId 			varchar(150)   
	  ,@itemQty			decimal(18,2)
 
AS
BEGIN		
		SET NOCOUNT ON;
			insert into   tbl_purchase_payment
			(supplierid , supplier , puchase_date , total , payment  , method   , purchaseby , Comment , itemQty  , ShopId)
			 values 
			(@supplierid , @supplier , @puchase_date , @total , @paidAmount  , @method   , @purchaseby , @Comment , @itemQty , @ShopId )
	 		
	 		
	 		
			--- Tracking Log 
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    purchaseCode   as purchaseCode  FROM tbl_purchase_payment order by purchaseCode desc)	
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@purchaseby , 'Purchased Payment |  Invoice No: ' + @spid)
	 		
	 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_PurchaseItems]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_PurchaseItems]
	   @product_id			varchar(150)
	  ,@ItemName			varchar(450)
	  ,@Qty					decimal(18,2)
	  ,@Price				decimal(18,2) 
	  ,@Total				decimal(18,2)
	  ,@puchase_date	    varchar(50)	
	  ,@supplierid			varchar(50)	
	  ,@InvoiceNoOutPut		varchar(150) output   
 
AS
BEGIN		
			SET NOCOUNT ON;			 
			Declare @purchaseCode bigint = isnull((Select top 1 purchaseCode + 1  from  tbl_purchase_payment  order by purchaseCode desc),99101) 
			
			insert into   tbl_purchase
			( [product_id] , [product_name]   ,[Qty] , [unit_price]    ,[total] , [purchaseCode]  , [supplierid] , puchase_date) 
			values (@product_id, @ItemName ,@Qty ,@Price ,   @Total , @purchaseCode , @supplierid , @puchase_date)				
			
			
			--Item Quantity Increase in tbl_item 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @product_id) + @Qty 
			where ItemCode = @product_id
			
			-- This is output Parameter send to Invoice number for Invoice printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 purchaseCode   + 1  from tbl_purchase_payment   order by purchaseCode  desc), 99101)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Inactive_Supplier]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_INV_Inactive_Supplier]
		 @id				bigint	 	 
		,@LastUpdateBy		varchar(150)
	
AS
BEGIN
	Update    tbl_supplier	 set 
    [Status] = 2 , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE()
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SuppliersEvent]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SuppliersEvent]	-- 59101
	@ID	varchar(50)     
AS
BEGIN			
			Select ID,  Phone , Name		 
			from  tbl_supplier
			where ID = @ID and Status = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Suppliers_name]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Suppliers_name]	 	     
AS
BEGIN	
			Select distinct '' as Name , '' as ID
			union all 
			Select  Name + ' (' + CompanyName + ')' as Name  , ID		 
			from   tbl_supplier
			where Status = 1						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SupplierListSearch]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SupplierListSearch]
				@value varchar(50)		 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID) As [S.N.], ID,   [Name] , [CompanyName] as [Company Name] , [Phone] ,[Email] ,[Address],[City] ,[Type] 
	 	from  tbl_supplier
	 	where    ID like '%' + @value + '%' or [Name] like '%' + @value + '%'  
	 				or [CompanyName] like '%' + @value + '%' 
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SupplierList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SupplierList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], [ID] ,[Name] ,  [CompanyName] as [Company Name] ,[Phone] ,[Email] ,[Address],[City] ,[Type] 
	 	from  tbl_supplier
	 	where Status = 1
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SoldListToCustomer]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SoldListToCustomer]	 
  	     @custid varchar(50)
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], ID   as [Invoice No] , totalpayable as	Total , CONVERT(varchar(10), Logdate)	  as [Date]	 
	 	from  tbl_SalesPayment
	    where   CustID = 	@custid				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport_Search]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport_Search]	-- 'POS'
	  @value varchar(450)
	 	     
AS
BEGIN
	 	select    ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due, ShopId
	 	from  tbl_SalesPayment	 	
		where CONVERT(varchar(111), ID ) = @value  or   trxtype like '%' + @value + '%' or   ShopId like '%' + @value + '%'
		order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport_DateToDate]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport_DateToDate]	
	  @DateFrom varchar(250)
	 ,@DateTo varchar(250) 	     
AS
BEGIN
	 	select   ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment	 	
		where Logdate between   @DateFrom     and   @DateTo   
		order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport]	 	     
AS
BEGIN
	 	select top 20  ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesPaymentlist]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_SalesPaymentlist] -- show on Sales invoice	 
	@InvoiceNo bigint       
AS
BEGIN
			select ROW_NUMBER() Over (Order by [paydate] desc) As [Payments], [paydate] as [Date]  ,[paidamount] as [Amount]  ,[paytype] + ' | '+ receivedby as [Payment type]
			from    tbl_salespaid
			where  [SP_ID]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Saleslist_search]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Saleslist_search]
	@value varchar(50)	 	     
AS
BEGIN 
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total  , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where ( ID like '%' + @value + '%' and  trxtype = 'sales' ) or (CustName like '%' + @value + '%' and  trxtype = 'sales')						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Salary]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Salary]	
	@value varchar(50) 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], [ID], CONVERT(varchar(11),saldate,106) as [Date] ,[saltype] as [Type] ,  salaryamount as [Amount]  ,salmonth as [Month] , salyr as [Year] , paidby 
	 	from  tbl_empSalary
	 	where Empid =  @value and   Status = 1
	 --	order by ID desc
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseListSupplier]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseListSupplier]	 
  	     @supplierid varchar(50)
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code] , 	Total ,	puchase_date as [Date]	 
	 	from tbl_purchase_payment	
	 	where supplierid = 	@supplierid				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItemList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItemList]	--  DataBind  in Printpage	
	@InvoiceNo bigint      
AS
BEGIN
	SET NOCOUNT ON;
			Select [product_id] as Item , [product_name] as [Description] ,  Qty , [unit_price] as [Unit Price], Total
			from     [tbl_purchase]
			where  [purchaseCode]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItemCustomerPanel]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItemCustomerPanel]	 	
	@CusID varchar(150)     
AS
BEGIN	 	
	    select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where trxtype = 'sales' and CustID =  @CusID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItem_search]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItem_search]
	@value varchar(50)	 	     
AS
BEGIN
		select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code]	
		, 	Supplier,	puchase_date as [Date] , Total, itemQty as Qty	 
		from tbl_purchase_payment
	 	where purchaseCode like '%' + @value + '%' or Supplier like '%' + @value + '%'  
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItem]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItem]	 	     
AS
BEGIN
	SET NOCOUNT ON;
	 	select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code]	
	 	, 	Supplier,	puchase_date as [Date]  , Total, itemQty as Qty
	 	from tbl_purchase_payment
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseInvoice]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

 CREATE    PROCEDURE [dbo].[SP_INV_DataBind_PurchaseInvoice]	  
	      @InvoiceNo bigint 
AS
BEGIN
	SET NOCOUNT ON;
			Select  pp.supplierid ,s.ID , pp.method ,pp.payment, pp.puchase_date, pp.supplier , pp.total, pp.comment
			, s.Name , s.Address ,s.City ,s.CompanyName , s.Email , s.Phone , s.Status 
			from     [tbl_purchase_payment] pp
			left join dbo.tbl_supplier s
			on pp.supplierid = s.ID 
			where  pp.purchaseCode	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Orderlist_search]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Orderlist_search]
	@value varchar(50)	 	     
AS
BEGIN
 
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total
	 	from  tbl_SalesPayment
	 	where ( ID like '%' + @value + '%' and  trxtype = 'order' ) or (CustName like '%' + @value + '%' and  trxtype = 'order')
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderList_CustomerPanel]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

Create   PROCEDURE [dbo].[SP_INV_DataBind_OrderList_CustomerPanel]	
	@custID varchar(150) 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due , order_status as [order status]
	 	from  tbl_SalesPayment
	 	where trxtype = 'order' and CustID = @custID 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_OrderList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due , order_status as [order status]
	 	from  tbl_SalesPayment
	 	where trxtype = 'order'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderItemList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_OrderItemList]	--1030 
	@InvoiceNo bigint      
AS
BEGIN
	SET NOCOUNT ON;
			Select [ID] as Item ,   ItemName as [Description] ,  Qty , Price  as [Unit Price], Total
			from   tbl_sales
			where  [SP_ID]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderInvoice]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

 CREATE    PROCEDURE [dbo].[SP_INV_DataBind_OrderInvoice] -- 1006   
	      @InvoiceNo bigint 
AS 
BEGIN  
			Select  sp.CustID ,c.ID , sp.payType ,(sp.totalpayable - sp.dueAmount) as paidAmount,sp.dueAmount ,  sp.ordedate, sp.CustName , sp.order_status ,sp.payment_status
			, sp.totalpayable,sp.Subtotal, sp.comment	, sp.Vat , sp.vatrate , sp.discount ,sp.dsirate , sp.shippingFee  , sp.shippingaddress
			, c.CustName , c.CustAddress  ,  c.CustEmail , c.CustPhone , c.Status 
			from     tbl_SalesPayment sp
			left join tbl_Customer c
			on sp.CustID = c.ID 
			where  sp.ID	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Inv_DataBind_Item_Purchase]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_Inv_DataBind_Item_Purchase]	--  DataBind team in Sales Register 	
	@category varchar(150)      
AS
BEGIN
SET NOCOUNT ON;
	IF(@category = 'All')
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total , Photo			  
					from    tbl_Item
					where   Status = 1 	
					order by Qty	
								 
		END
	ELSE
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total ,  Photo			  
					from    tbl_Item
					where ItemCategory = @category and Status = 1 
					order by Qty		
								 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_DashBoardSummary]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_INV_DataBind_DashBoardSummary]
AS
BEGIN
	SET NOCOUNT ON;
		select CAST(COUNT(*)as varchar(50)) + ' Customers ' Total from tbl_Customer
		union all
		select CAST(COUNT(*)as varchar(50)) + ' Suppliers ' Total from tbl_supplier
		Union all
		select CAST(COUNT(*)as varchar(50)) + ' Items ' Total from tbl_Item where Status = 1 

		Union all
		select CAST(SUM(total) as varchar(50)) + ' Purchases ' Total from  tbl_purchase_payment 
		Union all
		select CAST(SUM(totalpayable)as varchar(50)) + ' Orders ' Total from tbl_SalesPayment 	 	where trxtype = 'order' 

		Union all
		select CAST(SUM(totalpayable)as varchar(50)) + ' Sales ' Total from tbl_SalesPayment 	 	where trxtype = 'sales' 

		Union all
		select CAST(SUM(totalpayable) as varchar(50)) + ' POS Transactions ' Total from tbl_SalesPayment 	 	where trxtype = 'POS' 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_AccessLogByUser]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_AccessLogByUser]	 
   @UserID varchar(50)
AS
BEGIN
		SET NOCOUNT ON;
		 SELECT top 5 *  FROM 
		 [tbl_accesslog] 
		 where UserID = @UserID
		 order by 1 desc
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_AccessLog]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_AccessLog]	 
   
AS
BEGIN
		SET NOCOUNT ON;
		 SELECT top 1000 *  FROM 
		 [tbl_accesslog] order by 1 desc
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Add_Salary]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_INV_Add_Salary] 	 
	   @Empid		Varchar(30) 
      ,@saltype		Varchar(50) 
      ,@salaryamount decimal(18,2)
      ,@saldate		Varchar(50) 
      ,@salmonth	Varchar(210) 
      ,@salyr		Varchar(20) 
      ,@paidby		Varchar(210) 
      ,@note		Varchar(450)
  
          
AS
BEGIN
   insert into   tbl_empSalary	(Empid ,saltype ,salaryamount ,saldate ,salmonth ,salyr ,paidby, note) 
   values 
   (@Empid ,@saltype ,@salaryamount ,@saldate ,@salmonth ,@salyr ,@paidby , @note )
					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Auto_role_insert]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SP_Auto_role_insert]	
	    @userID		varchar(150),
	    @UserName		varchar(150) 
AS
BEGIN		
			--first truncate tbl_UserRole	
		 		-- Access allow
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '1'  FROM  [tbl_Page]
			where PageID    in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 ,144,145)
			
			-- Access Deny
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '0'  FROM  [tbl_Page]
			where PageID  not in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133 ,142 , 144, 145)
		
					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Authentication_Customer]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_User_Authentication_Customer] 
		@LOGINID as varchar(50),
		@PASSWORD as varchar(50)
 
AS
	SELECT * FROM   tbl_Customer
	WHERE   CustID  = @LOGINID AND [PASSWORD] = @password
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Authentication]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_User_Authentication] 
		@LOGINID as varchar(50),
		@PASSWORD as varchar(50)
 
AS
	SELECT * FROM  tbl_user  
	WHERE  UserID  = @LOGINID AND [PASSWORD] = @password
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_UserRole_UpdateAccess]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- Author:			Tuaha Mohammad 
						---- Company Name:   DynamicSoft    
						---- http://www.citputer.com/
						---- http://codecanyon.net/user/dynamicsoft
						----		We all need people who will give us feedback. That's how we improve.

CREATE    PROCEDURE [dbo].[SP_POS_UserRole_UpdateAccess] 
		@UserName varchar(50)
		,@status   int
		,@PageID  bigint
		  
AS
BEGIN
		  update  tbl_UserRole set
		 [Status] = @status
		 where UserName = @UserName and PageID = @PageID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_UserRole_Access]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- Author:			Tuaha Mohammad 
						---- Company Name:   DynamicSoft    
						---- http://www.citputer.com/
						---- http://codecanyon.net/user/dynamicsoft
						----		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_UserRole_Access] 
	@UserName varchar(50)
	,@PageName	varchar(50)
	,@OutPut varchar(5) Output 
AS
BEGIN
-- occur in Authentication
SET NOCOUNT ON;
		If Exists ((Select  PageName  , *  from  tbl_UserRole where UserName = @UserName and PageName = @PageName and status = 1))
		begin
			set @OutPut = 'Y'
		end
		else
		begin
			set @OutPut = 'N'
		end  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_UserProfile]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_UserProfile]
		 @UserID		varchar(150)
		,@Fname			varchar(250)
		,@LName  		varchar(150)
		,@UserPhone  	varchar(150)	
		,@Password		varchar(120)
		,@OldPassword		varchar(120)
		,@LastUpdateBy	varchar(150)
		,@User_Photo	varchar(250)
		,@ResultOutPut  varchar(120) output 
		 	
AS
BEGIN
		if exists (select [Password] from tbl_user Where UserID =  @UserID and [Password] = @OldPassword )
		 BEGIN
			Update  tbl_user  set  Fname = @Fname, LName =@LName , UserPhone =  @UserPhone , 
			User_Photo = @User_Photo , [Password] = @Password 
			,LastUpdateBy = @LastUpdateBy  ,   LastUpdate = GETDATE()
			Where UserID =  @UserID 
			
			--  Password Compare Validation parameter
			select	@ResultOutPut = 1		 
		END	 
		ELSE
		
		BEGIN
			select	@ResultOutPut = 0		
		END
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_User]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_User]
		 @id			bigint
		,@Fname			varchar(250)
		,@LName  		varchar(150)
		,@UserPhone  	varchar(150)
		,@Designation 	varchar(150)	
		,@Department 	varchar(150)	 
		,@UserAddress	varchar(450)	
		,@Email			varchar(150)			 
		,@LastUpdateBy	varchar(150)
		,@Password		varchar(120)
		,@DOB			varchar(10)
		,@ShopID		varchar(50)
		,@Supervisor	varchar(120)	
		,@User_Photo 	varchar(Max)
AS
BEGIN
		Update   tbl_user  set  Fname = @Fname, LName =@LName , UserPhone =  @UserPhone ,
		Designation = @Designation,   [Password] = @Password  ,	
		UserAddress = @UserAddress , Email = @Email, DateofBirth = @DOB, Department =@Department ,
		User_Photo = @User_Photo  ,ShopID = @ShopID , Supervisor = @Supervisor , 
		LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(), [Status]  = 1 
		Where ID =  @ID	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Settings]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_POS_Update_Settings]      
		  
		 @CompanyName		varchar(250)	
		,@CompanyAddress	varchar(450) 
		,@Phone				varchar(450)	 
		,@EmailAddress		varchar(450)	 
		,@WebAddress		varchar(450)
		,@VatRate           decimal(18,2)	 
		,@VATRegistration	varchar(450)	 
		,@Footermsg			nvarchar(250)	 	 
		,@LastUpdateBy		varchar(250)	     
AS
BEGIN
			Update dbo.[tbl_settings] set	  
			CompanyAddress = @CompanyAddress, CompanyName = @CompanyName,  Phone =	@Phone,	
			[EmailAddress] = @EmailAddress , [WebAddress] = @WebAddress ,[VatRate] = @VatRate ,
			 [VATRegistration] = @VATRegistration,
			[Footermsg] =  @Footermsg , LastUpdate =  GETDATE() , LastUpdateBy =	@LastUpdateBy
			where ID = 1				
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Item]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.



CREATE   PROCEDURE [dbo].[SP_POS_Update_Item]
	 @ItemCode varchar(450)
	,@ItemName varchar(450) 
	--,@ItemQty  decimal(18,2)
	,@PurchasePrice decimal(18,2)
	,@RetailPrice  decimal(18,2)	 
	,@Discount decimal(18,2)
	,@ItemCategory  varchar(250)
	,@Lastupdateby   varchar(150)
	,@Itemphoto     varchar(450)
	,@Description     varchar(499)
AS
BEGIN
			Update  tbl_Item	 set  
			 ItemName		= @ItemName 
			,PurchasePrice  = @PurchasePrice
			,RetailPrice	= @RetailPrice 
			,ItemCategory	= @ItemCategory  
			,Discount		= @Discount
			,Lastupdateby	= @Lastupdateby 
			,Photo			= @Itemphoto 
			,[Description]  = @Description
			,Lastupdate		= GETDATE() 
			,Status			= 1
			where ItemCode = @ItemCode 
			
				--- Tracking Log  
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@Lastupdateby, 'Update Item info : ' + @ItemName + ' | Item Code  : ' +  @ItemCode  )
			
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Customer]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_Customer]
		 @id				bigint
		,@CustName			varchar(250)
		,@CustPhone			varchar(150)
		,@CustEmail			varchar(150)
		,@CustAddress		varchar(450)
		,@CustType			varchar(150)
		,@DiscountRate		decimal(18,2)	 
		,@LastUpdateBy		varchar(150)
		,@custpaword 		varchar(150) 
	
AS
BEGIN
	Update   tbl_Customer	 set 
	CustName = @CustName, CustPhone = @CustPhone , CustEmail = @CustEmail ,CustAddress = @CustAddress ,
	CustType = @CustType  , DiscountRate = @DiscountRate  ,  LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(),[Password] = @custpaword , [Status]  = 1 
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_terminalInfo]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_POS_terminalInfo] --SYS89 -- only for POS printing perpose 
	@ShopID  varchar(500)
AS
BEGIN

	select st.CompanyName  , TL.Location ,  tl.Phone , tl.EmailAddress, tl.VatRate, tl.VATRegistration, usr.ShopID , st.WebAddress , st.Footermsg  
	from tbl_terminalLocation TL
	inner join tbl_user usr
	on
	TL.TerminalID = usr.ShopID
	inner join tbl_settings st
	on 
	TL.CompanyID = st.ID
	where usr.ShopID = @ShopID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_ReturnReport_DateToDate]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_ReturnReport_DateToDate]
	 @DateFrom varchar(250)
	 ,@DateTo varchar(250)
AS
BEGIN 
	
		select distinct ID as Invoice , SalesInvoiceID as [Sales ID], CONVERT(varchar, Logdate,109) as Date, ReturnQty as [Qty] , Subtotal, Vat, totalReturnable as [Total], 
		payType as [Pay Type],  ReturnAmount as [Return Amount], dueAmount as [Due] , note , ShopId , CustContact as [Cust.Contact], ServedBy
		from  tbl_ReturnPayment	  
		where  Logdate between   @DateFrom     and   @DateTo 
		order by  ID desc 
	  
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_SalesDetails]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
								---  Please Don't Forget to Rate Us http://codecanyon.net/downloads
		---		We all need people who will give us feedback. That's how we improve.
		

CREATE   PROCEDURE [dbo].[SP_POS_Report_SalesDetails]
	 @Value varchar(250)
AS
BEGIN
	 Select ItemCode as [Item Code] , ItemName as [Item Name],	Qty,	Price ,	DiscRate as [Disc%],	total as [Total] , Profit	 
	  from tbl_sales 
	 where SP_ID = @Value
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_Monthly]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report_Monthly]
	 
AS
BEGIN
	 --select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type],  paidAmount as [Paid], dueAmount as [Due] , note , ShopId ,  CustContact as [Cust.Contact], ServedBy
	 --from  tbl_SalesPayment  
	 
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID	
		where p.Logdate between   GETDATE() - 30    and  GETDATE() and trxtype = 'POS'
		order by p.ID desc 
	
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_DateToDate]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report_DateToDate]
	 @DateFrom varchar(250)
	 ,@DateTo varchar(250)
AS
BEGIN
	-- select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type] ,  paidAmount as [Paid], dueAmount as [Due] , note , ShopId ,  CustContact as [Cust.Contact], ServedBy
	-- from  tbl_SalesPayment  
	-- where Logdate between   @DateFrom     and   @DateTo  
	---- WHERE fromDate BETWEEN LIKE '%12/06/2012%' AND LIKE '%16/06/2012%'
	---- where CONVERT(VARCHAR(25), Your_DATE, 126) BETWEEN 'Start_date%' AND 'EndDate%'";
	--order by ID desc 
	
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID	 
		where p.Logdate between   @DateFrom     and   @DateTo  and trxtype = 'POS'
		order by p.ID desc 
	  
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/test45
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report]
	 @Value varchar(250)
AS
BEGIN
		--select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type],  paidAmount as [Paid], dueAmount as [Due] , note , ShopId , CustContact as [Cust.Contact], ServedBy
		--from  tbl_SalesPayment
		--where ID like  @Value  + '%' or ServedBy  like  '%' + @Value  + '%' 
		--or CustName   like    @Value  + '%' or ShopId   like '%' + @Value  + '%'   
		--or payType   like '%' + @Value  + '%'  
		---- select * from tbl_SalesPayment
		
	
		--DECLARE @Profit varchar(30) = (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  = 1018)

		--select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], p.ID as [Invoice],  p.payType  as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		--from  tbl_SalesPayment p
		--left join tbl_sales s 
		--on p.ID = s.SP_ID		 
		--where p.ID like  @Value  + '%' or p.ServedBy  like  '%' + @Value  + '%' 
		--or p.CustName   like    @Value  + '%' or p.ShopId   =  @Value     
		--or p.payType =  @Value  
		
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID		 
		where (p.ID =  @Value  and trxtype = 'POS') or (p.ServedBy  like  '%' + @Value  + '%'  and trxtype = 'POS')
		or  (p.CustName   like    @Value  + '%'  and trxtype = 'POS') 
		or (p.ShopId   =  @Value   and trxtype = 'POS')   
		or (p.payType =  @Value  and trxtype = 'POS')
		
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_PageDatabind]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_PageDatabind] 
	  @UserName varchar(150)
AS
BEGIN
	Select p.PageID as [Page ID] , p.Module , p.PageName as [Page Name], 
		case 	ur.Status
			 WHEN 1 THEN 'Allow' 
			 else 'Deny'
		 END as [Access]	 
	from	tbl_Page  p
	left join   
	tbl_UserRole ur on 
	p.PageID = ur.PageID
	where ur.UserName = @UserName 
	and  p.PageID  not in (108,110,111,114,118,119, 144)	 
	order by p.Module

END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_LogStat]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		-- EPOS Link -->  http://codecanyon.net/item/easy-pos-point-of-sale/10141609

CREATE   PROCEDURE [dbo].[SP_POS_LogStat]
       
AS
BEGIN	
		 select ROW_NUMBER() Over (Order by ID desc) As [S.N.], userID as [Log by]	, [datetime], IPaddress	  
		 from tbl_hit_counter	 
		 order by [datetime] desc 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Users]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/
						--- http://codecanyon.net/user/dynamicsoft
						---	We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Users]
		 --@UserID		bigint
		 @UserName		varchar(150)	
		,@Fname  		varchar(150)
		,@LName  		varchar(150)
		,@UserPhone 	varchar(150)
		,@UserAddress 	varchar(150)
		,@Supervisor  	varchar(150)		 
		,@Password 		varchar(150)
		,@Designation  	varchar(150)
		,@Department	varchar(150)	 
		,@DateofBirth 	varchar(150)
		,@ShopID		varchar(150)
		,@Logby			varchar(150)
		,@Email			varchar(150)
		,@User_Photo 	varchar(150)      
AS
BEGIN	 

	----Insert new user  
	Insert into	 tbl_user (Fname ,LName ,UserPhone  ,UserAddress ,Supervisor ,UserID  ,[Password], Designation ,Department ,DateofBirth,[ShopID] , Logby, Email,User_Photo)
	values (@Fname ,@LName,@UserPhone ,@UserAddress ,@Supervisor ,@UserName  ,@Password , @Designation, @Department   , @DateofBirth, @ShopID ,@Logby , @Email,@User_Photo  )
 
		Declare @UserID varchar(150) =  (Select top 1 ID  from tbl_user order by 1 desc) 

--- Default Page access for every user - when  user has been created 

		 		-- Access allow
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '1'  FROM  [tbl_Page]
			where PageID    in	(101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 , 144,145)
			 
			-- Access Deny
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '0'  FROM  [tbl_Page]
			where PageID  not in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 ,144,145)
			
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '101', 'AddItem.aspx' , 1)	
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '102', 'ManageItems.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '103', 'AddCustomer.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '104', 'ManageCustomers.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '105', 'Adduser.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '106', 'ManageUsers.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '107', 'SalesRegister.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '108', 'POS_printPage.aspx' , 1)		
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '109', 'Reports.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '110', 'ProfilePage.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '111', 'Default.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '112', 'Settings.aspx' , 0)
		
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '113', 'Category.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '114', 'TaskList.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '115', 'AddTask.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '116', 'StockItemReport.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '117', 'SalesReports.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '118', '404.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '119', '500.aspx' , 1)
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Task]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---We all need people who will give us feedback. That's how we improve.
 

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Task]
			@task_title		varchar(250)
			,@task_Description  varchar(550)
			,@taskfrom			varchar(150)
			,@taskto			varchar(150)	     
AS
BEGIN	
		Insert into		  tbl_tasklist 
				([task_title] ,	[task_Description] ,[taskfrom],	[taskto]  ) 
		 values  (@task_title,	@task_Description ,	@taskfrom	,@taskto )
		 
	 				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_SRsalesPayment]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_SRsalesPayment]
	   @SalesQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalpayable	decimal(18,2)
      ,@payType			varchar(50)
      ,@paidAmount		decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)
       	 	     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_SalesPayment
			([SalesQty],[Subtotal],[Vat],[totalpayable] ,[payType] , [paidAmount] ,[changeAmount] ,[dueAmount]
			,[note] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy],trxtype) 
			values 
			(@SalesQty, @Subtotal ,@Vat ,@totalpayable ,@payType ,@paidAmount ,@changeAmount ,@dueAmount
			,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy , 'POS') 
			
			
						
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_SalesPayment order by ID desc)	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,'POS' ,CONVERT(date, GETDATE(),120) ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )
	
	
		--- Tracking Log  Sales and order
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'POS' + ' Payment |  Invoice No: ' + @spid )
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_SalesItems]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_SalesItems]
	   @Code		varchar(150)
	  ,@ItemName	varchar(450)
	  ,@Qty			decimal(18,2)
	  ,@Price		decimal(18,2)
	  ,@Disc 		decimal(18,2)
	  ,@Total		decimal(18,2)	
	  ,@InvoiceNoOutPut varchar(150) output   
 
AS
BEGIN		
   SET NOCOUNT ON;
			--DECLARE @Profit varchar(30) = (Select     [RetailPrice] - [PurchasePrice] as  [profit] FROM  [tbl_Item] where ItemCode = @Code);
	 
			-- Profit Calculation is  = [RetailPrice] with [Discount Rate] -  PurchasePrice]
			DECLARE @Profit varchar(30) = (Select  CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2))  - [PurchasePrice] as  [profit] FROM  [tbl_Item] where ItemCode = @Code);
			
			insert into  tbl_sales  
			([ItemCode],[ItemName]  ,[Qty] , [Price], [DiscRate] ,[total],[SP_ID],[Profit]) 
			values (@Code, @ItemName ,@Qty ,@Price , @Disc ,@Total , 
			isnull((Select top 1 ID + 1  from tbl_SalesPayment order by ID desc),1001),@Profit)				
			
			
			--Item Quantity Deduction 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @Code) - @Qty 
			where ItemCode = @Code
			
			-- This is output Parameter send to Invoice number for POS printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 ID + 1  from tbl_SalesPayment order by ID desc),1001)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_ReturnPayment]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_ReturnPayment]
	   @ReturnQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalReturnable	decimal(18,2)
      ,@payType			varchar(50)
      ,@ReturnAmount	decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)
      ,@SalesInvoiceID  bigint
       	 	     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_ReturnPayment
			([ReturnQty],[Subtotal],[Vat],[totalReturnable] ,[payType] , [ReturnAmount] ,[changeAmount] ,[dueAmount]
			,[note] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy],trxtype , SalesInvoiceID) 
			values 
			(@ReturnQty, @Subtotal ,@Vat ,@totalReturnable ,@payType ,@ReturnAmount ,@changeAmount ,@dueAmount
			,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy , 'POS', @SalesInvoiceID) 
			
			
						
			----- first time single payment info record
			--DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_ReturnPayment order by ID desc)	
			--insert into   tbl_salespaid
			--([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
			--	values
			--(@spid ,'POS' ,CONVERT(date, GETDATE(),120) ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )
	
	
		--- Tracking Log  Sales and order
		DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_ReturnPayment order by ID desc)
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'POS' + ' Return Payment |  Invoice No: ' + @spid )
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_ReturnItems]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_ReturnItems]
	   @Code		varchar(150)
	  ,@ItemName	varchar(450)
	  ,@Qty			decimal(18,2)
	  ,@Price		decimal(18,2)
	  ,@Disc 		decimal(18,2)
	  ,@Total		decimal(18,2)	
	  ,@InvoiceNoOutPut varchar(150) output   
 
AS
BEGIN		
   SET NOCOUNT ON;		
		
			insert into  tbl_Return
			([ItemCode],[ItemName]  ,[Qty] , [Price], [DiscRate] ,[total],[RP_ID]) 
			values (@Code, @ItemName ,@Qty ,@Price , @Disc ,@Total , 
			isnull((Select top 1 ID + 1  from tbl_ReturnPayment order by ID desc),4001))				
			 
			
			--Item Quantity Increase  -- for return Policy 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @Code) + @Qty 
			where ItemCode = @Code
			
			-- This is output Parameter send to Invoice number for POS printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 ID + 1  from tbl_ReturnPayment order by ID desc), 4001)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Items]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---We all need people who will give us feedback. That's how we improve.
 

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Items]
		 @ItemCode			varchar(450)
		,@ItemName			varchar(450)
		,@PurchasePrice		decimal(18,2)	
		,@RetailPrice		decimal(18,2)	
		--,@ItemQty			decimal(18,2)	
		,@ItemCategory		varchar(150)		
		,@Discount			decimal(18,2)	
		,@LogBy 			varchar(150)
		,@Itemphoto 		varchar(350)	 
		,@Description  		varchar(499)   
AS
BEGIN	
		Insert into		 tbl_Item 
		([ItemCode],[ItemName],[PurchasePrice] ,[RetailPrice], [ItemCategory], [Discount],[LogBy],[Photo],[Description])
		values  
		(@ItemCode,@ItemName,@PurchasePrice,@RetailPrice, @ItemCategory,@Discount,@LogBy,@Itemphoto,@Description)	


			--- Tracking Log 
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@LogBy ,   ' Added New Item : ' + @ItemName  )
			 
				 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_HitCounter]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_HitCounter]
	@userID			varchar(150),
	@IPaddress		varchar(150)        
AS
BEGIN	
		Insert into	  tbl_hit_counter	 ([userID], [IPaddress]) 
		values (@userID , @IPaddress)
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Customers]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Customers]
	   @CustName		varchar(250)
      ,@CustPhone		varchar(250)
      ,@CustEmail		varchar(250)
      ,@CustAddress		varchar(450)
      ,@CustType		varchar(150) 
      ,@DiscountRate	decimal(18,2)
      ,@LogBy			varchar(150)
	  ,@CustID			varchar(250)
	  ,@CustPassword	varchar(250)	     
AS
BEGIN	
		Insert into	 tbl_Customer 
		([CustName] ,[CustPhone],[CustEmail],[CustAddress],[CustType],[DiscountRate] ,[LogBy] ,CustID  , [Password])
Values	(@CustName , @CustPhone ,@CustEmail ,@CustAddress ,@CustType,@DiscountRate ,  @LogBy,@CustID,@CustPassword)
				 
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Inactive_Customer]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create   PROCEDURE [dbo].[SP_POS_Inactive_Customer]
		 @id				bigint	 	 
		,@LastUpdateBy		varchar(150)
	
AS
BEGIN
	Update   tbl_Customer	 set 
    [Status] = 2 , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE()
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Terminal]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Terminal] 	 
	@TerminalID   varchar(250)
	 	     
AS
BEGIN
		Update tbl_terminalLocation set [Status] = 2, Lastupdate = GETDATE()
		where TerminalID = @TerminalID 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Item]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Item] 	 
	@Code bigint 
   ,@Lastupdateby	varchar(160) 	     
AS
BEGIN
		Update  tbl_Item  set [Status] = 2, Lastupdateby = 'Del_by: ' + @Lastupdateby , Lastupdate = GETDATE()
		where ItemCode = @Code  
		
		--- Tracking Log 
		insert into   tbl_accesslog
		([UserID], [Operations])
		values
		(@Lastupdateby ,   ' Deleted   Item | code: ' +  CAST(@Code as varchar(450)))	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Category]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Category] 	 
	@ID bigint 
	 	     
AS
BEGIN
		Update dbo.tbl_Category set [Status] = 2, Lastupdate = GETDATE()
		where ID = @ID 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersProfileDetails]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[SP_POS_DataBind_UsersProfileDetails]	 
	@UserID varchar(150)	     
AS
BEGIN
			Select  *
			from  tbl_user
			where UserID = @UserID and Status = 1 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersProfile]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_UsersProfile]	 
	@ID bigint	     
AS
BEGIN
		SET NOCOUNT ON;
			SELECT ID, [Fname] + ' ' +  [LName] as EmployeeName  ,[UserPhone] as OfficialPhone  ,[Designation]   ,[Department] , User_Photo , 
			Convert(varchar(11), DateofBirth,113) as DateofBirth ,  Supervisor , Email , 
			UserAddress, [ShopID]    
			FROM  [tbl_user]
			where ID = @ID and Status = 1 	
	 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersDetails]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[SP_POS_DataBind_UsersDetails]	 
	@ID bigint	     
AS
BEGIN
			Select  *
			from  tbl_user
			where ID = @ID and Status = 1 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Users]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Users]	 	     
AS
BEGIN
			Select ID , UserID , Fname + '  ' + LName as [Name] , UserPhone as [Contact] ,
			email, UserAddress as [Address] , Designation ,    ShopID    , User_Photo
			,Department
			--, CASE [Role]
			--	WHEN 1 THEN 'Admin'
			--	WHEN 2 THEN 'User'
			--    Else 'NA'
			--  END AS [Role] 		 
			  
			from  tbl_user
			where  Status = 1 and ID not IN(23)
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UserID]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROCEDURE [dbo].[SP_POS_DataBind_UserID]	 	     
AS
BEGIN
			Select 'All' as UserID 
			union all 
			Select   UserID 	from  tbl_user	where Status = 1	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TerminalList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TerminalList]	 	     
AS
BEGIN
	Select   TerminalID 'Terminal ID' , Location , Phone , EmailAddress Email ,VatRate as VAT, VATRegistration 'VAT NO'
	from    tbl_terminalLocation
	where [Status] = 1
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TaskNotification]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TaskNotification]	
	    @userID		varchar(150) 
AS
BEGIN			
		Select top 1 task_Description as [Description] 		 
		from   tbl_tasklist	
		where 	 taskto =  @userID or taskto = 'all'
		order by ID desc 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TaskList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TaskList]	
	    @userID		varchar(150) 
AS
BEGIN			
			Select taskfrom as [From],	taskto as [To], task_title as [Title] ,	task_Description as [Description],		convert(varchar(50),taskdate,109) as [Date]	 		 
			from   tbl_tasklist	
			where 	 taskto =  @userID or taskto = 'all'
			order by 1 desc 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ShopIdDDL]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_ShopIdDDL]	 	     
AS
BEGIN
	Select  Distinct	  TerminalID
	from    tbl_terminalLocation	
	where [Status] = 1
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_SettingsUpdate]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE PROCEDURE [dbo].[SP_POS_DataBind_SettingsUpdate]      
		-- @ID				bigint	 
		 	     
AS
BEGIN
			Select * from tbl_settings
			where ID = 1			
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_SalesItem_PP]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_SalesItem_PP]	--  DataBind team in Printpage	
	@InvoiceID bigint      
AS
BEGIN
SET NOCOUNT ON;
			Select  ItemName + ' ' +  ItemCode , Qty  , total
			from     tbl_sales
			where  [SP_ID]	= @InvoiceID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ReturnItemBarCodeSearch]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

Create    PROCEDURE [dbo].[SP_POS_DataBind_ReturnItemBarCodeSearch]	--  DataBind team in Sales Register barcode scan	
	@ItemCode  varchar(250)    
AS
BEGIN
	SET NOCOUNT ON;
			Select   ItemCode as [Code],	
			ItemName as [Items Name] , RetailPrice	 as Price,Discount as [Disc%],
			 CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total
			from    tbl_Item
			where  ItemCode = @ItemCode or ItemName like '%'+ @ItemCode +'%'   
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ItemSearch]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft


CREATE   PROCEDURE [dbo].[SP_POS_DataBind_ItemSearch]	
	@value 	varchar(500)     
AS
BEGIN
			Select  ID  	, ItemCode as [Code] ,	ItemName as [ItemName],ItemQty as [Qty], PurchasePrice as [Purchase Price] ,
			RetailPrice as [Retail Price],   Discount as [Discount %],
			CAST((RetailPrice -((RetailPrice * Discount) / 100))* ItemQty	as numeric(18,2))   as Total	,
			ItemCategory as [Category] ,
			CONVERT(VARCHAR(24),Lastupdate,113)    as Lastupdate,  Lastupdateby as [Updated by] ,Photo
			from    tbl_Item
			where ItemCode like '%' + @value + '%'  or
				  ItemName like '%' + @value + '%' or  ItemCategory like '%' + @value + '%'  
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ItemBarCodeSearch]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_ItemBarCodeSearch]	--  DataBind team in Sales Register barcode scan	
	@ItemCode  varchar(250)    
AS
BEGIN
	SET NOCOUNT ON;
			Select   ItemCode as [Code],	
			ItemName as [Items Name] , RetailPrice	 as Price,Discount as [Disc%],
			 CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total
			from    tbl_Item
			where  ItemCode = @ItemCode or ItemName like '%'+ @ItemCode +'%'  and [Status] = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item_SR]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_Item_SR]	--  DataBind team in Sales Register 	
	@category varchar(150),
	@shopID varchar(250)      
AS
BEGIN
SET NOCOUNT ON;
	IF(@category = 'All')
		BEGIN
					Select item.ID, item.ItemCode as [Code],	
					item.ItemName, CONVERT(int, terminal.itemQty) as [Qty], item.RetailPrice	 as Price,item.Discount as [Disc]
					 , CAST((item.RetailPrice -((item.RetailPrice * item.Discount) / 100))	as numeric(18,2)) as Total ,  item.Photo			  
					from    tbl_itemTerminal terminal left join tbl_Item item on terminal.itemCode=item.itemCode
					where   item.Status = 1 AND terminal.TerminalID= @shopID	
					--order by Logtime	
								 
		END
	ELSE
		BEGIN
					Select item.ID, item.ItemCode as [Code],	
					item.ItemName, CONVERT(int, terminal.itemQty) as [Qty], item.RetailPrice	 as Price,item.Discount as [Disc]
					 , CAST((item.RetailPrice -((item.RetailPrice * item.Discount) / 100))	as numeric(18,2)) as Total ,  item.Photo			  
					from    tbl_itemTerminal terminal left join tbl_Item item on terminal.itemCode=item.itemCode
					where terminal.TerminalID=@shopID and item.ItemCategory = @category and item.Status = 1 		
								 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item_Details]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_Item_Details]	 
	@ID bigint 	     
AS
BEGIN
		 	Select * from tbl_Item	
		 	where ID = @ID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Item]	 	     
AS
BEGIN
			Select  ID  	, ItemCode as [Code] ,	ItemName as [Item Name],ItemQty as [Qty], PurchasePrice as [Purchase Price] ,
			RetailPrice as [Retail Price],   Discount as [Discount %],
			CAST((RetailPrice -((RetailPrice * Discount) / 100))* ItemQty	as numeric(18,2))   as Total	,
			ItemCategory as [Category] ,
			 CONVERT(varchar(11),Lastupdate,109)    as Lastupdate,  Lastupdateby as [Updated by] ,Photo
			from    tbl_Item
			where Status = 1 
			order by ID desc 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CustomersEvent]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CustomersEvent]	
	@CustName	varchar(50)     
AS
BEGIN			
			Select ID,  CustPhone , CustAddress		 
			from  tbl_Customer
			where CustName = @CustName and Status = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Customers_name]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Customers_name]	 	     
AS
BEGIN	
			Select distinct 'Guest' as Name , '0' as ID
			union all 
			Select  CustName as Name 	, ID	 
			from  tbl_Customer
			where Status = 1	
		 					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Customers]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Customers]	 	     
AS
BEGIN
			Select   ID as [ID],	CustName as Name,	CustPhone as Contact,
			CustEmail as Email,	CustAddress as [Address] ,
			CustType as [Customer Type],DiscountRate as [Dsicount %] ,
			 CustID , [password],
				Case [Status] when   1 then 'active'
							  when   2 then 'Inactive'
							  ELSE 'Ban' 
				END as Status		 
			from  tbl_Customer 
			where Status = 1 	
			order   by  ID desc 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryList]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryList]	 	     
AS
BEGIN
	Select  ID,	[ItemCategory] as [Category Name], [LogBy] as [Posted by], CONVERT(VARCHAR(24),Lastupdate,6) as Lastupdate ,	[Status]
	from    tbl_Category
	where Status = 1 
	order by 1 desc		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryDDL_SR]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryDDL_SR]	 	     
AS	 
BEGIN
			Select 'All' as ItemCategory
			Union all
			Select  Distinct	[ItemCategory]  
			from      tbl_Item
			where Status = 1 
 				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryDDL]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryDDL]	 	     
AS
BEGIN
	Select  Distinct	[ItemCategory]  
	from    tbl_Category
	where Status = 1 
	order by 1 
	 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Add_Terminal]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Add_Terminal]	
		@TerminalID				Varchar(210),  
		@Location				Varchar(210), 
		@Phone					Varchar(210), 
		@EmailAddress			Varchar(210),
		@VatRate				decimal(18,3),
		@VATRegistration		Varchar(210),   
		@logby					Varchar(210)  
AS
BEGIN
		SET NOCOUNT ON;
			insert into   tbl_terminalLocation	
			( TerminalID ,Location, Phone , EmailAddress , VatRate, VATRegistration , LogBy) 
			values 
		    (@TerminalID ,@Location, @Phone , @EmailAddress , @VatRate , @VATRegistration , @LogBy)						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Add_Category]    Script Date: 07/09/2017 00:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Add_Category] 	 
	@category Varchar(210),
	@logby    Varchar(210) 	     
AS
BEGIN
			insert into   tbl_Category	(ItemCategory , LogBy) values (@category , @logby )
			
						 
END
GO
/****** Object:  Default [DF_tbl_UserRole_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_UserRole] ADD  CONSTRAINT [DF_tbl_UserRole_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_UserRole_lastUpdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_UserRole] ADD  CONSTRAINT [DF_tbl_UserRole_lastUpdate]  DEFAULT (getdate()) FOR [lastUpdate]
GO
/****** Object:  Default [DF_tbl_user_DateofBirth]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_DateofBirth]  DEFAULT (getdate()-(8000)) FOR [DateofBirth]
GO
/****** Object:  Default [DF_tbl_user_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_user_Lastupdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_user_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_user_User_Photo]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_User_Photo]  DEFAULT ('User_Photo/man.png') FOR [User_Photo]
GO
/****** Object:  Default [DF_tbl_terminalLocation_CompanyID]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_CompanyID]  DEFAULT ((1)) FOR [CompanyID]
GO
/****** Object:  Default [DF_tbl_terminalLocation_LastUpdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_terminalLocation_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_terminalLocation_status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_tasklist_taskdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_taskdate]  DEFAULT (getdate()) FOR [taskdate]
GO
/****** Object:  Default [DF_tbl_tasklist_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_tasklist_status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_supplier_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_supplier_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_supplier_LastUpdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_settings_LastUpdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_settings] ADD  CONSTRAINT [DF_tbl_settings_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_settings_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_settings] ADD  CONSTRAINT [DF_tbl_settings_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_SalesPayment_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_SalesPayment] ADD  CONSTRAINT [DF_tbl_SalesPayment_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_SalesPayment_Logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_SalesPayment] ADD  CONSTRAINT [DF_tbl_SalesPayment_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_salespaid_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_salespaid] ADD  CONSTRAINT [DF_tbl_salespaid_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_salespaid_logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_salespaid] ADD  CONSTRAINT [DF_tbl_salespaid_logdate]  DEFAULT (getdate()) FOR [logdate]
GO
/****** Object:  Default [DF_tbl_sales_SP_ID]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_SP_ID]  DEFAULT ((1001)) FOR [SP_ID]
GO
/****** Object:  Default [DF_tbl_sales_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_sales_Logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_ReturnPayment_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_ReturnPayment] ADD  CONSTRAINT [DF_tbl_ReturnPayment_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_ReturnPayment_Logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_ReturnPayment] ADD  CONSTRAINT [DF_tbl_ReturnPayment_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_Return_RP_ID]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_RP_ID]  DEFAULT ((1001)) FOR [RP_ID]
GO
/****** Object:  Default [DF_tbl_Return_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_Return_Logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_purchase_payment_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_purchase_payment] ADD  CONSTRAINT [DF_tbl_purchase_payment_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_purchase_logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_purchase] ADD  CONSTRAINT [DF_tbl_purchase_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_Page_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Page] ADD  CONSTRAINT [DF_tbl_Page_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Item_ItemQty]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_ItemQty]  DEFAULT ((0)) FOR [ItemQty]
GO
/****** Object:  Default [DF_tbl_Item_Tax]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Tax]  DEFAULT ((0)) FOR [Tax]
GO
/****** Object:  Default [DF_tbl_Item_Discount]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Discount]  DEFAULT ((0)) FOR [Discount]
GO
/****** Object:  Default [DF_tbl_Item_Photo]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Photo]  DEFAULT ('ItemsPhoto/item.png') FOR [Photo]
GO
/****** Object:  Default [DF_tbl_Item_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Item_Lastupdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_Item_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_hit_counter_datetime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_datetime]  DEFAULT (getdate()) FOR [datetime]
GO
/****** Object:  Default [DF_tbl_hit_counter_timelog]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_timelog]  DEFAULT (getdate()) FOR [timelog]
GO
/****** Object:  Default [DF_tbl_hit_counter_count]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_count]  DEFAULT ((1)) FOR [count]
GO
/****** Object:  Default [DF_tbl_empSalary_saldate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_saldate]  DEFAULT (getdate()) FOR [saldate]
GO
/****** Object:  Default [DF_tbl_empSalary_logdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_logdate]  DEFAULT (getdate()) FOR [logdate]
GO
/****** Object:  Default [DF_tbl_empSalary_status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_email_maildate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_email] ADD  CONSTRAINT [DF_tbl_email_maildate]  DEFAULT (getdate()) FOR [maildate]
GO
/****** Object:  Default [DF_tbl_email_status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_email] ADD  CONSTRAINT [DF_tbl_email_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_Customer_DiscountRate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_DiscountRate]  DEFAULT ((0)) FOR [DiscountRate]
GO
/****** Object:  Default [DF_tbl_Customer_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Customer_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_Customer_LastUpdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_Category_Status]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Category_Logtime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_Category_Lastupdate]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_accesslog_datetime]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_accesslog] ADD  CONSTRAINT [DF_tbl_accesslog_datetime]  DEFAULT (getdate()) FOR [datetime]
GO
/****** Object:  Default [DF_tbl_accesslog_timelog]    Script Date: 07/09/2017 00:04:11 ******/
ALTER TABLE [dbo].[tbl_accesslog] ADD  CONSTRAINT [DF_tbl_accesslog_timelog]  DEFAULT (getdate()) FOR [timelog]
GO
