SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserRole](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[UserName] [varchar](250) NULL,
	[PageID] [int] NULL,
	[PageName] [varchar](250) NULL,
	[Status] [int] NULL,
	[lastUpdate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 =Active , 2 = inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_UserRole', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_UserRole] ON
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (1, 1, N'jamal', 101, N'AddItem.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (2, 1, N'jamal', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (3, 1, N'jamal', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (4, 1, N'jamal', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (5, 1, N'jamal', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (6, 1, N'jamal', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (7, 1, N'jamal', 111, N'Default.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (8, 1, N'jamal', 114, N'TaskList.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (9, 1, N'jamal', 118, N'404.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (10, 1, N'jamal', 119, N'500.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (11, 1, N'jamal', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (12, 1, N'jamal', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (13, 1, N'jamal', 133, N'NewSale.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (14, 1, N'jamal', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (15, 1, N'jamal', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (16, 1, N'jamal', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A00111F148 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (17, 1, N'jamal', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (18, 1, N'jamal', 105, N'Adduser.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (19, 1, N'jamal', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (20, 1, N'jamal', 109, N'Reports.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (21, 1, N'jamal', 112, N'Settings.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (22, 1, N'jamal', 113, N'Category.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (23, 1, N'jamal', 115, N'AddTask.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (24, 1, N'jamal', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (25, 1, N'jamal', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (26, 1, N'jamal', 120, N'ItemList.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (27, 1, N'jamal', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (28, 1, N'jamal', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (29, 1, N'jamal', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (30, 1, N'jamal', 125, N'Order_history.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (31, 1, N'jamal', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (32, 1, N'jamal', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (33, 1, N'jamal', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (34, 1, N'jamal', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (35, 1, N'jamal', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (36, 1, N'jamal', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (37, 1, N'jamal', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (38, 1, N'jamal', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (39, 1, N'jamal', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (40, 1, N'jamal', 137, N'ShopList.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (41, 1, N'jamal', 138, N'AddSupplier.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (42, 1, N'jamal', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (43, 1, N'jamal', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A00111F15B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (44, 2, N'Kamal', 101, N'AddItem.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (45, 2, N'Kamal', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (46, 2, N'Kamal', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (47, 2, N'Kamal', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (48, 2, N'Kamal', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (49, 2, N'Kamal', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (50, 2, N'Kamal', 111, N'Default.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (51, 2, N'Kamal', 114, N'TaskList.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (52, 2, N'Kamal', 118, N'404.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (53, 2, N'Kamal', 119, N'500.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (54, 2, N'Kamal', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (55, 2, N'Kamal', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (56, 2, N'Kamal', 133, N'NewSale.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (57, 2, N'Kamal', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (58, 2, N'Kamal', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (59, 2, N'Kamal', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A001120F02 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (60, 2, N'Kamal', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (61, 2, N'Kamal', 105, N'Adduser.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (62, 2, N'Kamal', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (63, 2, N'Kamal', 109, N'Reports.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (64, 2, N'Kamal', 112, N'Settings.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (65, 2, N'Kamal', 113, N'Category.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (66, 2, N'Kamal', 115, N'AddTask.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (67, 2, N'Kamal', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (68, 2, N'Kamal', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (69, 2, N'Kamal', 120, N'ItemList.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (70, 2, N'Kamal', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (71, 2, N'Kamal', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (72, 2, N'Kamal', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (73, 2, N'Kamal', 125, N'Order_history.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (74, 2, N'Kamal', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (75, 2, N'Kamal', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (76, 2, N'Kamal', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (77, 2, N'Kamal', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (78, 2, N'Kamal', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (79, 2, N'Kamal', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (80, 2, N'Kamal', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (81, 2, N'Kamal', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (82, 2, N'Kamal', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (83, 2, N'Kamal', 137, N'ShopList.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (84, 2, N'Kamal', 138, N'AddSupplier.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (85, 2, N'Kamal', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (86, 2, N'Kamal', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A001120F14 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (87, 3, N'Neo', 101, N'AddItem.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (88, 3, N'Neo', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (89, 3, N'Neo', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (90, 3, N'Neo', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (91, 3, N'Neo', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (92, 3, N'Neo', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (93, 3, N'Neo', 111, N'Default.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (94, 3, N'Neo', 114, N'TaskList.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (95, 3, N'Neo', 118, N'404.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (96, 3, N'Neo', 119, N'500.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (97, 3, N'Neo', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (98, 3, N'Neo', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (99, 3, N'Neo', 133, N'NewSale.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (100, 3, N'Neo', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
GO
print 'Processed 100 total records'
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (101, 3, N'Neo', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (102, 3, N'Neo', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A001121E68 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (103, 3, N'Neo', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (104, 3, N'Neo', 105, N'Adduser.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (105, 3, N'Neo', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (106, 3, N'Neo', 109, N'Reports.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (107, 3, N'Neo', 112, N'Settings.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (108, 3, N'Neo', 113, N'Category.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (109, 3, N'Neo', 115, N'AddTask.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (110, 3, N'Neo', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (111, 3, N'Neo', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (112, 3, N'Neo', 120, N'ItemList.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (113, 3, N'Neo', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (114, 3, N'Neo', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (115, 3, N'Neo', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (116, 3, N'Neo', 125, N'Order_history.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (117, 3, N'Neo', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (118, 3, N'Neo', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (119, 3, N'Neo', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (120, 3, N'Neo', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (121, 3, N'Neo', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (122, 3, N'Neo', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (123, 3, N'Neo', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (124, 3, N'Neo', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (125, 3, N'Neo', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (126, 3, N'Neo', 137, N'ShopList.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (127, 3, N'Neo', 138, N'AddSupplier.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (128, 3, N'Neo', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (129, 3, N'Neo', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A001121E76 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (130, 22, N'fakim', 101, N'AddItem.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (131, 22, N'fakim', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (132, 22, N'fakim', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (133, 22, N'fakim', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (134, 22, N'fakim', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (135, 22, N'fakim', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (136, 22, N'fakim', 111, N'Default.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (137, 22, N'fakim', 114, N'TaskList.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (138, 22, N'fakim', 118, N'404.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (139, 22, N'fakim', 119, N'500.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (140, 22, N'fakim', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (141, 22, N'fakim', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (142, 22, N'fakim', 133, N'NewSale.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (143, 22, N'fakim', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (144, 22, N'fakim', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (145, 22, N'fakim', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A001122C12 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (146, 22, N'fakim', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (147, 22, N'fakim', 105, N'Adduser.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (148, 22, N'fakim', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (149, 22, N'fakim', 109, N'Reports.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (150, 22, N'fakim', 112, N'Settings.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (151, 22, N'fakim', 113, N'Category.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (152, 22, N'fakim', 115, N'AddTask.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (153, 22, N'fakim', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (154, 22, N'fakim', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (155, 22, N'fakim', 120, N'ItemList.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (156, 22, N'fakim', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (157, 22, N'fakim', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (158, 22, N'fakim', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (159, 22, N'fakim', 125, N'Order_history.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (160, 22, N'fakim', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (161, 22, N'fakim', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (162, 22, N'fakim', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (163, 22, N'fakim', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (164, 22, N'fakim', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (165, 22, N'fakim', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (166, 22, N'fakim', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (167, 22, N'fakim', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (168, 22, N'fakim', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (169, 22, N'fakim', 137, N'ShopList.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (170, 22, N'fakim', 138, N'AddSupplier.aspx', 1, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (171, 22, N'fakim', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (172, 22, N'fakim', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A001122C25 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (173, 23, N'admin', 101, N'AddItem.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (174, 23, N'admin', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (175, 23, N'admin', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (176, 23, N'admin', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (177, 23, N'admin', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (178, 23, N'admin', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (179, 23, N'admin', 111, N'Default.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (180, 23, N'admin', 114, N'TaskList.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (181, 23, N'admin', 118, N'404.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (182, 23, N'admin', 119, N'500.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (183, 23, N'admin', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (184, 23, N'admin', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (185, 23, N'admin', 133, N'NewSale.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (186, 23, N'admin', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (187, 23, N'admin', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (188, 23, N'admin', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (189, 23, N'admin', 102, N'ManageItems.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (190, 23, N'admin', 105, N'Adduser.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (191, 23, N'admin', 106, N'ManageUsers.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (192, 23, N'admin', 109, N'Reports.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (193, 23, N'admin', 112, N'Settings.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (194, 23, N'admin', 113, N'Category.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (195, 23, N'admin', 115, N'AddTask.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (196, 23, N'admin', 116, N'StockItemReport.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (197, 23, N'admin', 117, N'SalesReport.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (198, 23, N'admin', 120, N'ItemList.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (199, 23, N'admin', 121, N'ChartReport.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (200, 23, N'admin', 140, N'EmployeesProfile.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (201, 23, N'admin', 141, N'User_privilege.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
GO
print 'Processed 200 total records'
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (202, 23, N'admin', 125, N'Order_history.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (203, 23, N'admin', 126, N'Order_Invoice.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (204, 23, N'admin', 127, N'TakePayment.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (205, 23, N'admin', 128, N'ChangeStatus.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (206, 23, N'admin', 130, N'Purchase_history.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (207, 23, N'admin', 131, N'Purchase_invoice.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (208, 23, N'admin', 132, N'AccessLog.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (209, 23, N'admin', 134, N'Sales_history.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (210, 23, N'admin', 135, N'Sales_invoice.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (211, 23, N'admin', 136, N'TakePayment.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (212, 23, N'admin', 137, N'ShopList.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (213, 23, N'admin', 138, N'AddSupplier.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (214, 23, N'admin', 139, N'ManageSuppliers.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (215, 23, N'admin', 143, N'ReturnReports.aspx', 1, CAST(0x0000A6A001123830 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (216, 24, N'salauddin', 101, N'AddItem.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (217, 24, N'salauddin', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (218, 24, N'salauddin', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (219, 24, N'salauddin', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (220, 24, N'salauddin', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (221, 24, N'salauddin', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (222, 24, N'salauddin', 111, N'Default.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (223, 24, N'salauddin', 114, N'TaskList.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (224, 24, N'salauddin', 118, N'404.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (225, 24, N'salauddin', 119, N'500.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (226, 24, N'salauddin', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (227, 24, N'salauddin', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (228, 24, N'salauddin', 133, N'NewSale.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (229, 24, N'salauddin', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (230, 24, N'salauddin', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (231, 24, N'salauddin', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A00112443D AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (232, 24, N'salauddin', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (233, 24, N'salauddin', 105, N'Adduser.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (234, 24, N'salauddin', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (235, 24, N'salauddin', 109, N'Reports.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (236, 24, N'salauddin', 112, N'Settings.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (237, 24, N'salauddin', 113, N'Category.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (238, 24, N'salauddin', 115, N'AddTask.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (239, 24, N'salauddin', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (240, 24, N'salauddin', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (241, 24, N'salauddin', 120, N'ItemList.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (242, 24, N'salauddin', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (243, 24, N'salauddin', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (244, 24, N'salauddin', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (245, 24, N'salauddin', 125, N'Order_history.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (246, 24, N'salauddin', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (247, 24, N'salauddin', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (248, 24, N'salauddin', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (249, 24, N'salauddin', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (250, 24, N'salauddin', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (251, 24, N'salauddin', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (252, 24, N'salauddin', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (253, 24, N'salauddin', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (254, 24, N'salauddin', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (255, 24, N'salauddin', 137, N'ShopList.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (256, 24, N'salauddin', 138, N'AddSupplier.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (257, 24, N'salauddin', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (258, 24, N'salauddin', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A001124450 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (259, 37, N'Jeqube', 101, N'AddItem.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (260, 37, N'Jeqube', 103, N'AddCustomer.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (261, 37, N'Jeqube', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (262, 37, N'Jeqube', 107, N'SalesRegister.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (263, 37, N'Jeqube', 108, N'POS_printPage.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (264, 37, N'Jeqube', 110, N'ProfilePage.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (265, 37, N'Jeqube', 111, N'Default.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (266, 37, N'Jeqube', 114, N'TaskList.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (267, 37, N'Jeqube', 118, N'404.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (268, 37, N'Jeqube', 119, N'500.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (269, 37, N'Jeqube', 124, N'NewOrder.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (270, 37, N'Jeqube', 129, N'NewPurchase.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (271, 37, N'Jeqube', 133, N'NewSale.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (272, 37, N'Jeqube', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (273, 37, N'Jeqube', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (274, 37, N'Jeqube', 145, N'UploadItems.aspx', 1, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (275, 37, N'Jeqube', 102, N'ManageItems.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (276, 37, N'Jeqube', 105, N'Adduser.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (277, 37, N'Jeqube', 106, N'ManageUsers.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (278, 37, N'Jeqube', 109, N'Reports.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (279, 37, N'Jeqube', 112, N'Settings.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (280, 37, N'Jeqube', 113, N'Category.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (281, 37, N'Jeqube', 115, N'AddTask.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (282, 37, N'Jeqube', 116, N'StockItemReport.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (283, 37, N'Jeqube', 117, N'SalesReport.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (284, 37, N'Jeqube', 120, N'ItemList.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (285, 37, N'Jeqube', 121, N'ChartReport.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (286, 37, N'Jeqube', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (287, 37, N'Jeqube', 141, N'User_privilege.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (288, 37, N'Jeqube', 125, N'Order_history.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (289, 37, N'Jeqube', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (290, 37, N'Jeqube', 127, N'TakePayment.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (291, 37, N'Jeqube', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (292, 37, N'Jeqube', 130, N'Purchase_history.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (293, 37, N'Jeqube', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (294, 37, N'Jeqube', 132, N'AccessLog.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (295, 37, N'Jeqube', 134, N'Sales_history.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (296, 37, N'Jeqube', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (297, 37, N'Jeqube', 136, N'TakePayment.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (298, 37, N'Jeqube', 137, N'ShopList.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (299, 37, N'Jeqube', 138, N'AddSupplier.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (300, 37, N'Jeqube', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (301, 37, N'Jeqube', 143, N'ReturnReports.aspx', 0, CAST(0x0000A6A0011251F6 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (302, 38, N'new34', 101, N'AddItem.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
GO
print 'Processed 300 total records'
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (303, 38, N'new34', 103, N'AddCustomer.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (304, 38, N'new34', 104, N'ManageCustomers.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (305, 38, N'new34', 107, N'SalesRegister.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (306, 38, N'new34', 108, N'POS_printPage.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (307, 38, N'new34', 110, N'ProfilePage.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (308, 38, N'new34', 111, N'Default.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (309, 38, N'new34', 114, N'TaskList.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (310, 38, N'new34', 118, N'404.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (311, 38, N'new34', 119, N'500.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (312, 38, N'new34', 124, N'NewOrder.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (313, 38, N'new34', 129, N'NewPurchase.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (314, 38, N'new34', 133, N'NewSale.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (315, 38, N'new34', 142, N'ReturnSales_POS.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (316, 38, N'new34', 144, N'ReturnPrintPage.aspx', 1, CAST(0x0000A768014BB10B AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (317, 38, N'new34', 102, N'ManageItems.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (318, 38, N'new34', 105, N'Adduser.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (319, 38, N'new34', 106, N'ManageUsers.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (320, 38, N'new34', 109, N'Reports.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (321, 38, N'new34', 112, N'Settings.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (322, 38, N'new34', 113, N'Category.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (323, 38, N'new34', 115, N'AddTask.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (324, 38, N'new34', 116, N'StockItemReport.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (325, 38, N'new34', 117, N'SalesReport.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (326, 38, N'new34', 120, N'ItemList.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (327, 38, N'new34', 121, N'ChartReport.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (328, 38, N'new34', 140, N'EmployeesProfile.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (329, 38, N'new34', 141, N'User_privilege.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (330, 38, N'new34', 125, N'Order_history.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (331, 38, N'new34', 126, N'Order_Invoice.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (332, 38, N'new34', 127, N'TakePayment.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (333, 38, N'new34', 128, N'ChangeStatus.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (334, 38, N'new34', 130, N'Purchase_history.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (335, 38, N'new34', 131, N'Purchase_invoice.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (336, 38, N'new34', 132, N'AccessLog.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (337, 38, N'new34', 134, N'Sales_history.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (338, 38, N'new34', 135, N'Sales_invoice.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (339, 38, N'new34', 136, N'TakePayment.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (340, 38, N'new34', 137, N'ShopList.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (341, 38, N'new34', 138, N'AddSupplier.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (342, 38, N'new34', 139, N'ManageSuppliers.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (343, 38, N'new34', 143, N'ReturnReports.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
INSERT [dbo].[tbl_UserRole] ([ID], [UserID], [UserName], [PageID], [PageName], [Status], [lastUpdate]) VALUES (344, 38, N'new34', 145, N'UploadItems.aspx', 0, CAST(0x0000A768014BB114 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_UserRole] OFF
/****** Object:  Table [dbo].[tbl_user]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_user](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Fname] [varchar](450) NULL,
	[LName] [varchar](450) NULL,
	[UserPhone] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
	[UserAddress] [varchar](550) NULL,
	[Supervisor] [varchar](450) NULL,
	[UserID] [varchar](50) NOT NULL,
	[Password] [varchar](110) NULL,
	[Role] [int] NULL,
	[Designation] [varchar](450) NULL,
	[Department] [varchar](110) NULL,
	[DateofBirth] [varchar](110) NULL,
	[Logtime] [datetime] NULL,
	[Logby] [varchar](110) NULL,
	[Lastupdate] [datetime] NULL,
	[LastupdateBy] [varchar](110) NULL,
	[Status] [int] NULL,
	[ShopID] [varchar](50) NULL,
	[User_Photo] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_user] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 =Active 2 =inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_user', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_user] ON
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (23, N'system', N'admin', N'98889898', N'admin@gmail.com', N'sys', N'adm', N'admin', N'admin', NULL, N'sysadm', N'Sales', N'1983-02-09', CAST(0x0000A41E01571636 AS DateTime), N'fakim', CAST(0x0000A76801737344 AS DateTime), N'admin', 1, N'SYS89', N'~/User_Photo/23.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (22, N'fakim', N'Mollik', N'913487845', N'fakim@gamil.com', N'New Delhi, India', N'Jeo', N'fakim', N'fakim', NULL, N'IT-Officer', N'Sales', N'1991-02-09', CAST(0x0000A41A017C6E55 AS DateTime), N'', CAST(0x0000A7A20139B360 AS DateTime), N'admin', 1, N'MTQC03', N'~/User_Photo/22.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (1, N'Mohammad', N'Jamal', N'01772176275', N'jamal@live.com', N'H14, Road 18C ,Sector 3 , Montreal, CA', N'Kamal', N'Jamal', N'Jamal', 4, N'SalesMan', N'Sales', N'1981-02-09', CAST(0x0000A3C801800728 AS DateTime), N'Admin', CAST(0x0000A7680141427F AS DateTime), N'admin', 1, N'MTQC03', N'~/User_Photo/1.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (37, N'Michel', N'Jeqube', N'5147779843', N'Jeqube@gmail.com', N'Montreal, QC, canada', N'CEO', N'Jeqube', N'Jeqube', NULL, N'CEO', N'SMT', N'1963-12-19', CAST(0x0000A60600D9578E AS DateTime), N'admin', CAST(0x0000A76A0182DACC AS DateTime), N'admin', 1, N'MTQC03', N'~/User_Photo/37.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (2, N'Amim', N'Kamal', N'39453894769', N'Kamal@citputer.com', N'Uttara,Dhaka', N'Neo', N'Kamal', N'323223', 4, N'Manager', N'HR_Admin', N'1987-10-09', CAST(0x0000A3C80180A462 AS DateTime), N'Admin', CAST(0x0000A76801407137 AS DateTime), N'admin', 1, N'ONBAS22', N'~/User_Photo/2.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (3, N'Jeol', N'Neo', N'4160930777', N'Neo@pos.com', N'Uttara,Dhaka', N'Somon', N'Neo', N'Neo', 2, N'Manager', N'HR_Admin', N'2009-05-10', CAST(0x0000A3C80180A462 AS DateTime), N'Admin', CAST(0x0000A768013E49E2 AS DateTime), N'admin', 1, N'MTQC03', N'~/User_Photo/3.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (38, N'new test', N'hello', N'4593409', N'dfgfdg@skfjsid.com', N'sdasd', N'managers', N'new34', N'new34', NULL, N'sdfs', N'sdf', N'1993-12-19', CAST(0x0000A768014BB10B AS DateTime), N'admin', CAST(0x0000A768014BB10B AS DateTime), NULL, 1, N'ONBAS22', N'~/User_Photo/new34.jpg')
INSERT [dbo].[tbl_user] ([ID], [Fname], [LName], [UserPhone], [Email], [UserAddress], [Supervisor], [UserID], [Password], [Role], [Designation], [Department], [DateofBirth], [Logtime], [Logby], [Lastupdate], [LastupdateBy], [Status], [ShopID], [User_Photo]) VALUES (24, N'salauddin', N'ahmed', N'+97212342344', N'salauddin@gmail.com', N'Toronto, ON', N'N/A', N'salauddin', N'salauddin', NULL, N'General Manager', N'Marketing', N'1993-02-09', CAST(0x0000A42501613C03 AS DateTime), N'admin', CAST(0x0000A7680140F47F AS DateTime), N'admin', 1, N'ONWL01', N'~/User_Photo/24.JPG')
SET IDENTITY_INSERT [dbo].[tbl_user] OFF
/****** Object:  Table [dbo].[tbl_terminalLocation]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_terminalLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyID] [bigint] NULL,
	[TerminalID] [varchar](250) NOT NULL,
	[Location] [varchar](450) NULL,
	[Phone] [varchar](450) NULL,
	[EmailAddress] [varchar](450) NULL,
	[VatRate] [decimal](18, 3) NULL,
	[VATRegistration] [varchar](450) NULL,
	[LastUpdate] [datetime] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_tbl_terminalLocation] PRIMARY KEY CLUSTERED 
(
	[TerminalID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active 2 = Inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_terminalLocation', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_terminalLocation] ON
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (18, 1, N'asdasd', N'asd', N'0015147772335', N'MTQC03@walmart.com', CAST(14.000 AS Decimal(18, 3)), N'RT1238790sdsd', CAST(0x0000A7AA018701D5 AS DateTime), CAST(0x0000A7AA018701D5 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (1, 1, N'MTQC02', N'239 Avenue Downtown, Montreal, QC , H7R 1K2', N'514 7629890', N'shopmtqc02@walmart.com', CAST(14.975 AS Decimal(18, 3)), N'RT342342', CAST(0x0000A608002203D4 AS DateTime), CAST(0x0000A5FD00BF8FB2 AS DateTime), NULL, 2)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (17, 1, N'MTQC03', N'Lachine Montreal', N'+0015147772335', N'MTQC03@walmart.com', CAST(14.975 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60A01067F03 AS DateTime), CAST(0x0000A60A01067F03 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (4, 1, N'ONBAS22', N'London, ON', N'517412423', N'ONMT@walmart.com', CAST(13.000 AS Decimal(18, 3)), N'RT123', CAST(0x0000A5FE00202CFB AS DateTime), CAST(0x0000A5FE00202CFB AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (12, 1, N'ONBAS229', N'asd', N'23456789', N'ONMT@walmart.com', CAST(13.000 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60800244729 AS DateTime), CAST(0x0000A60800243F68 AS DateTime), N'admin', 2)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (14, 1, N'ONBAS2299', N'asd', N'23456789', N'ONMT@walmart.com', CAST(13.000 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60800249406 AS DateTime), CAST(0x0000A60800249406 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (11, 1, N'ONWL01', N'Waterlow , Montreal', N'23456789', N'ONWL01@walmart.com', CAST(13.000 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60400C87876 AS DateTime), CAST(0x0000A60400C87876 AS DateTime), N'invoadmin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (3, 1, N'QCBAS22', N'Hamilton, ON', N'412412423', N'QCMT@walmart.com', CAST(14.975 AS Decimal(18, 3)), N'RT123', CAST(0x0000A5FE00246EAF AS DateTime), CAST(0x0000A5FE001F551D AS DateTime), N'admin', 2)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (10, 1, N'QCMDL01', N'Dorval , Montreal', N'23456789', N'QCMDL01@walmart.com', CAST(14.975 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60400C83C9C AS DateTime), CAST(0x0000A60400C83C9C AS DateTime), N'invoadmin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (6, 1, N'QCMLA22', N'Lachine Montreal', N'23456789', N'QCMLA22@walmart.com', CAST(14.975 AS Decimal(18, 3)), N'RT1238790', CAST(0x0000A60400C7D870 AS DateTime), CAST(0x0000A60400C7D870 AS DateTime), N'invoadmin', 1)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (5, 1, N'sd', N'asd', N'asd', N'as@ads.co', NULL, N'RT123ads', CAST(0x0000A5FE00253BF6 AS DateTime), CAST(0x0000A5FE0024DB59 AS DateTime), N'admin', 2)
INSERT [dbo].[tbl_terminalLocation] ([ID], [CompanyID], [TerminalID], [Location], [Phone], [EmailAddress], [VatRate], [VATRegistration], [LastUpdate], [Logtime], [LogBy], [Status]) VALUES (2, 1, N'SYS89', N'Central Office, NY NER2AE, USA', N'3318976622', N'sales@waltmart.com', CAST(13.000 AS Decimal(18, 3)), N'A3434DA', CAST(0x0000A5FD00C28FD6 AS DateTime), CAST(0x0000A5FD00C28FD6 AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[tbl_terminalLocation] OFF
/****** Object:  Table [dbo].[tbl_tasklist]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_tasklist](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[task_title] [varchar](150) NOT NULL,
	[task_Description] [varchar](550) NULL,
	[taskfrom] [varchar](50) NOT NULL,
	[taskto] [varchar](50) NOT NULL,
	[taskdate] [date] NULL,
	[logtime] [datetime] NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = active , 2 = inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_tasklist', @level2type=N'COLUMN',@level2name=N'status'
GO
SET IDENTITY_INSERT [dbo].[tbl_tasklist] ON
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (1, N'Please input all items ', N'items store to system', N'admin', N'fakim', CAST(0x76390B00 AS Date), CAST(0x0000A41B01814F88 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (2, N'Please input all items ', N'items store to system', N'admin', N'Nikol', CAST(0x76390B00 AS Date), CAST(0x0000A41B01814F88 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (3, N'Please input all items ', N'items store to system', N'admin', N'Kamal', CAST(0x76390B00 AS Date), CAST(0x0000A41B01814F88 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (4, N'All News ', N'items store to system', N'admin', N'all', CAST(0x76390B00 AS Date), CAST(0x0000A41B01814F88 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (5, N'', N'', N'fakim', N'', CAST(0x78390B00 AS Date), CAST(0x0000A41D010585AF AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (6, N'Title', N'Description ', N'fakim', N'', CAST(0x78390B00 AS Date), CAST(0x0000A41D01061064 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (7, N'Title', N'Description ', N'fakim', N'', CAST(0x78390B00 AS Date), CAST(0x0000A41D01065BF2 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (8, N'', N'Description', N'fakim', N'fakim', CAST(0x78390B00 AS Date), CAST(0x0000A41D01068B9A AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (9, N'Work list for Jamal', N'- New Item input
- make daily Report', N'fakim', N'Jamal', CAST(0x78390B00 AS Date), CAST(0x0000A41D015E6F04 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (12, N'Work list for Jamal', N'-new', N'fakim', N'Jamal', CAST(0x78390B00 AS Date), CAST(0x0000A41D015EC5BB AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (14, N'Hello all', N'Hello all good morning everyone 
new Inventory system', N'Jamal', N'All', CAST(0x78390B00 AS Date), CAST(0x0000A41D01619326 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (10, N'df', N'df', N'fakim', N'Jamal', CAST(0x78390B00 AS Date), CAST(0x0000A41D015EB31B AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (11, N'df', N'df', N'fakim', N'Jamal', CAST(0x78390B00 AS Date), CAST(0x0000A41D015EB387 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (13, N'Work list for Jamal', N'- New Item 
- Daily report send to Manager', N'fakim', N'Jamal', CAST(0x78390B00 AS Date), CAST(0x0000A41D015F07AE AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (15, N'', N'Hello Kamal be sincere ', N'admin', N'Kamal', CAST(0x7F390B00 AS Date), CAST(0x0000A42401267B92 AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (16, N'tesst', N'test', N'admin', N'hjjhg', CAST(0x0A3A0B00 AS Date), CAST(0x0000A4AF00A103DA AS DateTime), 1)
INSERT [dbo].[tbl_tasklist] ([ID], [task_title], [task_Description], [taskfrom], [taskto], [taskdate], [logtime], [status]) VALUES (17, N'Hello Every body this is our new inventory system.', N'Hello Every body this is our new inventory system', N'admin', N'All', CAST(0x5F3B0B00 AS Date), CAST(0x0000A604016BAA2C AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tbl_tasklist] OFF
/****** Object:  Table [dbo].[tbl_supplier]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_supplier](
	[ID] [bigint] IDENTITY(59101,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Phone] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
	[Address] [varchar](450) NULL,
	[City] [varchar](150) NULL,
	[Type] [varchar](150) NULL,
	[CompanyName] [varchar](150) NULL,
	[Status] [int] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](150) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_supplier] ON
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59101, N'Jeo barat', N'23456789', N'jeo@hmail.com', N'Montreal, Canada', N'Montreal', N'Dealer', N'CocaCola', 1, CAST(0x0000A60600065987 AS DateTime), N'admin', CAST(0x0000A606000CEB59 AS DateTime), N'admin')
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59102, N'Cosco', N'8765432', N'sales@gmail.com', N'Dettroit , MI, USA', N'Dettroit', N'Company', N'Cosco', 1, CAST(0x0000A606000BB710 AS DateTime), N'admin', CAST(0x0000A76901385F60 AS DateTime), N'admin')
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59103, N'Pepsi', N'4539458', N'pepsi@gmail.com', N'montreal, ca', N'montreal', N'Company', N'pepsi', 1, CAST(0x0000A769011E3079 AS DateTime), N'admin', CAST(0x0000A769011E3079 AS DateTime), NULL)
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59104, N'Cisco system', N'930583487', N'cisco@ymail.com', N'NY,USA', N'NY', N'Company', N'cisco', 1, CAST(0x0000A769011F3534 AS DateTime), N'admin', CAST(0x0000A769011F3534 AS DateTime), NULL)
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59105, N'Google inc', N'875839465', N'google@gmail.com', N'SF,USA
', N'SF', N'Company', N'Google', 1, CAST(0x0000A769011F6832 AS DateTime), N'admin', CAST(0x0000A7700155BC69 AS DateTime), N'admin')
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59106, N'Food Supplier', N'92384329', N'FSQ@gmail.com', N'NY,USA', N'NY', N'Distributor', N'food', 1, CAST(0x0000A769011FA820 AS DateTime), N'admin', CAST(0x0000A770015BC9D8 AS DateTime), N'admin')
INSERT [dbo].[tbl_supplier] ([ID], [Name], [Phone], [Email], [Address], [City], [Type], [CompanyName], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy]) VALUES (59107, N'Food Supplier', N'4539458', N'FSQ@gmail.com', N'gfhg', N'montreal', N'Dealer', N'food', 2, CAST(0x0000A770015133CD AS DateTime), N'admin', CAST(0x0000A7700152949B AS DateTime), N'admin')
SET IDENTITY_INSERT [dbo].[tbl_supplier] OFF
/****** Object:  Table [dbo].[tbl_settings]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_settings](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](250) NULL,
	[CompanyAddress] [varchar](450) NULL,
	[Phone] [varchar](450) NULL,
	[EmailAddress] [varchar](450) NULL,
	[WebAddress] [varchar](450) NULL,
	[VatRate] [decimal](18, 2) NULL,
	[VATRegistration] [varchar](450) NULL,
	[Footermsg] [nvarchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](250) NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_settings] ON
INSERT [dbo].[tbl_settings] ([ID], [CompanyName], [CompanyAddress], [Phone], [EmailAddress], [WebAddress], [VatRate], [VATRegistration], [Footermsg], [LastUpdate], [LastUpdateBy], [Logtime], [LogBy]) VALUES (1, N'Walmart', N'CS
850 Cherry Avenue
San Bruno, CA 94066.
', N'+0015147772335', N'sales@Walmart.com', N'www.Walmart.com', CAST(5.00 AS Decimal(18, 2)), N'A3434DA', N'Item sold will not be refund only EXCHANGE will be executed with in 5 days. ', CAST(0x0000A76A0155E6F2 AS DateTime), N'admin', CAST(0x0000A3C801819775 AS DateTime), N'Admin')
INSERT [dbo].[tbl_settings] ([ID], [CompanyName], [CompanyAddress], [Phone], [EmailAddress], [WebAddress], [VatRate], [VATRegistration], [Footermsg], [LastUpdate], [LastUpdateBy], [Logtime], [LogBy]) VALUES (2, N'Apple inc', N'Apple inc, USA', N'2342344', N'sales@apple.com', N'apple.com', NULL, N'A3434DA--', N'Any Change or Refund should be made with in 3 days & tag should not be removed', CAST(0x0000A3C801819775 AS DateTime), N'Admin', CAST(0x0000A3C801819775 AS DateTime), N'Admin')
SET IDENTITY_INSERT [dbo].[tbl_settings] OFF
/****** Object:  Table [dbo].[tbl_SalesPayment]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SalesPayment](
	[ID] [bigint] IDENTITY(1001,1) NOT NULL,
	[SalesQty] [decimal](18, 2) NOT NULL,
	[Subtotal] [decimal](18, 2) NOT NULL,
	[Vat] [decimal](18, 2) NULL,
	[totalpayable] [decimal](18, 2) NOT NULL,
	[payType] [varchar](150) NULL,
	[paidAmount] [decimal](18, 2) NOT NULL,
	[changeAmount] [decimal](18, 2) NULL,
	[dueAmount] [decimal](18, 2) NULL,
	[note] [varchar](450) NULL,
	[ShopId] [varchar](50) NULL,
	[CustID] [bigint] NULL,
	[CustName] [varchar](150) NULL,
	[CustContact] [varchar](150) NULL,
	[ServedBy] [varchar](150) NULL,
	[Logtime] [datetime] NULL,
	[Logdate] [date] NULL,
	[ordedate] [varchar](150) NULL,
	[trxtype] [varchar](150) NULL,
	[order_status] [varchar](150) NULL,
	[payment_status] [varchar](150) NULL,
	[comment] [varchar](150) NULL,
	[discount] [decimal](18, 2) NULL,
	[dsirate] [decimal](18, 2) NULL,
	[vatrate] [decimal](18, 2) NULL,
	[shippingFee] [varchar](150) NULL,
	[shippingaddress] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SalesID/Invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_SalesPayment', @level2type=N'COLUMN',@level2name=N'ID'
GO
SET IDENTITY_INSERT [dbo].[tbl_SalesPayment] ON
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1001, CAST(3.00 AS Decimal(18, 2)), CAST(11.00 AS Decimal(18, 2)), CAST(1.42 AS Decimal(18, 2)), CAST(12.34 AS Decimal(18, 2)), N'Cash', CAST(15.00 AS Decimal(18, 2)), CAST(2.66 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 2, N'Halim khan', N'0987654678', N'admin', CAST(0x0000A606000D72D7 AS DateTime), CAST(0x613B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1002, CAST(2.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(0.45 AS Decimal(18, 2)), CAST(9.38 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.62 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 2, N'Halim khan', N'0987654678', N'admin', CAST(0x0000A607001006F2 AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-15', N'order', N'Pending', N'Paid', N'done', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Laval, QC, CA')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1003, CAST(2.00 AS Decimal(18, 2)), CAST(13.00 AS Decimal(18, 2)), CAST(0.66 AS Decimal(18, 2)), CAST(13.91 AS Decimal(18, 2)), N'Cash', CAST(14.00 AS Decimal(18, 2)), CAST(0.09 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 1, N'Jamal jeo', N'098765789', N'admin', CAST(0x0000A60700116C25 AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-20', N'sales', N'', N'', N'done', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1004, CAST(2.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(0.86 AS Decimal(18, 2)), CAST(7.45 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(2.55 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60700122F1A AS DateTime), CAST(0x623B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1005, CAST(1.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(0.37 AS Decimal(18, 2)), CAST(7.86 AS Decimal(18, 2)), N'Cash', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 0, N'Bill jeo guer', N'9456674', N'ChangeStatusadmin', CAST(0x0000A60700C54F6B AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-17', N'order', N'Delivered', N'Paid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'DH')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1006, CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.20 AS Decimal(18, 2)), CAST(4.20 AS Decimal(18, 2)), N'Cash', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.10 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60700E2B862 AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-16', N'order', N'Pending', N'Unpaid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Bill Villa, Rue67, Montreal, QQ, Canada')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1007, CAST(1.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(0.37 AS Decimal(18, 2)), CAST(7.86 AS Decimal(18, 2)), N'Cash', CAST(7.86 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60700E6F972 AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-16', N'sales', N'', N'', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1008, CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.10 AS Decimal(18, 2)), CAST(2.10 AS Decimal(18, 2)), N'Cash', CAST(3.00 AS Decimal(18, 2)), CAST(0.90 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'ChangeStatus_admin', CAST(0x0000A60700EDF869 AS DateTime), CAST(0x623B0B00 AS Date), N'2016-05-17', N'order', N'Delivered', N'Paid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'my address')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1009, CAST(2.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(1.23 AS Decimal(18, 2)), CAST(10.70 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.70 AS Decimal(18, 2)), N'', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A6070187B5E5 AS DateTime), CAST(0x623B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1011, CAST(2.00 AS Decimal(18, 2)), CAST(8.00 AS Decimal(18, 2)), CAST(1.04 AS Decimal(18, 2)), CAST(9.04 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.96 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A6090121191B AS DateTime), CAST(0x643B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1012, CAST(0.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.22 AS Decimal(18, 2)), CAST(1.95 AS Decimal(18, 2)), N'Cash', CAST(2.00 AS Decimal(18, 2)), CAST(0.05 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A609012F15EE AS DateTime), CAST(0x643B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1010, CAST(3.00 AS Decimal(18, 2)), CAST(16.00 AS Decimal(18, 2)), CAST(2.09 AS Decimal(18, 2)), CAST(18.15 AS Decimal(18, 2)), N'Cash', CAST(20.00 AS Decimal(18, 2)), CAST(1.85 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x633B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1015, CAST(5.00 AS Decimal(18, 2)), CAST(23.00 AS Decimal(18, 2)), CAST(3.05 AS Decimal(18, 2)), CAST(26.54 AS Decimal(18, 2)), N'Cash', CAST(27.00 AS Decimal(18, 2)), CAST(0.46 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60A012D40AF AS DateTime), CAST(0x653B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1013, CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.10 AS Decimal(18, 2)), CAST(2.08 AS Decimal(18, 2)), N'Cash', CAST(2.08 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'ChangeStatus_admin', CAST(0x0000A6090152ED15 AS DateTime), CAST(0x643B0B00 AS Date), N'2016-05-18', N'order', N'Pending', N'Paid', N'done  Condition', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'1487 Milto St , N7E5D1,Essex, ON CA')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1014, CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(0.75 AS Decimal(18, 2)), CAST(6.51 AS Decimal(18, 2)), N'Cash', CAST(7.00 AS Decimal(18, 2)), CAST(0.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A60A012B8DC1 AS DateTime), CAST(0x653B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1016, CAST(1.50 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.12 AS Decimal(18, 2)), CAST(9.76 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.24 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A60A0162965D AS DateTime), CAST(0x653B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1017, CAST(1.50 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(0.43 AS Decimal(18, 2)), CAST(9.07 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.93 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A60A0162E079 AS DateTime), CAST(0x653B0B00 AS Date), N'2016-05-19', N'order', N'Pending', N'Paid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Montreal, QC')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1018, CAST(5.50 AS Decimal(18, 2)), CAST(19.52 AS Decimal(18, 2)), CAST(2.54 AS Decimal(18, 2)), CAST(22.06 AS Decimal(18, 2)), N'Cash', CAST(25.00 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'done', N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A61201134D6C AS DateTime), CAST(0x6D3B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1019, CAST(7.00 AS Decimal(18, 2)), CAST(29.70 AS Decimal(18, 2)), CAST(1.48 AS Decimal(18, 2)), CAST(31.18 AS Decimal(18, 2)), N'Cheque', CAST(20.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 2, N'Halim khan', N'0987654678', N'ChangeStatus_admin', CAST(0x0000A612011AE297 AS DateTime), CAST(0x6D3B0B00 AS Date), N'2016-05-26', N'order', N'Approved', N'Paid', N'done', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Laval, QC, CA')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1020, CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.14 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), N'Cash', CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 2, N'Halim khan', N'0987654678', N'admin', CAST(0x0000A612011BF2E4 AS DateTime), CAST(0x6D3B0B00 AS Date), N'2016-05-27', N'sales', N'', N'', N'done', CAST(1.14 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1024, CAST(2.00 AS Decimal(18, 2)), CAST(7.99 AS Decimal(18, 2)), CAST(1.04 AS Decimal(18, 2)), CAST(9.03 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.97 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76900025761 AS DateTime), CAST(0xC43C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1025, CAST(2.00 AS Decimal(18, 2)), CAST(5.98 AS Decimal(18, 2)), CAST(0.30 AS Decimal(18, 2)), CAST(6.28 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(3.72 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76900EAA9BF AS DateTime), CAST(0xC43C0B00 AS Date), N'2017-05-05', N'sales', N'', N'', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1027, CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.10 AS Decimal(18, 2)), CAST(2.08 AS Decimal(18, 2)), N'Cash', CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), NULL, N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A769012968A9 AS DateTime), CAST(0xC43C0B00 AS Date), N'2017-05-04', N'sales', N'', N'', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1029, CAST(2.00 AS Decimal(18, 2)), CAST(53.00 AS Decimal(18, 2)), CAST(6.89 AS Decimal(18, 2)), CAST(59.89 AS Decimal(18, 2)), N'Cash', CAST(60.00 AS Decimal(18, 2)), CAST(0.11 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76C000BFD42 AS DateTime), CAST(0xC73C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1031, CAST(2.00 AS Decimal(18, 2)), CAST(6.92 AS Decimal(18, 2)), CAST(0.90 AS Decimal(18, 2)), CAST(7.82 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(2.18 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A770011112A8 AS DateTime), CAST(0xCB3C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1032, CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(2.45 AS Decimal(18, 2)), CAST(51.45 AS Decimal(18, 2)), N'Paypal', CAST(50.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.45 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'admin', CAST(0x0000A7A2016FFCE7 AS DateTime), CAST(0xFD3C0B00 AS Date), N'2017-06-26', N'order', N'Pending', N'Unpaid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Bill Villa, Rue67, Montreal, QQ, Canada')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1033, CAST(3.00 AS Decimal(18, 2)), CAST(54.98 AS Decimal(18, 2)), CAST(2.75 AS Decimal(18, 2)), CAST(57.73 AS Decimal(18, 2)), N'Payza', CAST(12.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(45.73 AS Decimal(18, 2)), NULL, N'SYS89', 6, N'Julkifol', N'232344', N'admin', CAST(0x0000A7A2018461A4 AS DateTime), CAST(0xFD3C0B00 AS Date), N'2017-06-07', N'sales', N'', N'', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1034, CAST(2.00 AS Decimal(18, 2)), CAST(53.00 AS Decimal(18, 2)), CAST(6.89 AS Decimal(18, 2)), CAST(59.89 AS Decimal(18, 2)), N'Cheque', CAST(133.00 AS Decimal(18, 2)), CAST(73.11 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A7A3000C1430 AS DateTime), CAST(0xFE3C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1021, CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.26 AS Decimal(18, 2)), CAST(2.24 AS Decimal(18, 2)), N'Cash', CAST(3.00 AS Decimal(18, 2)), CAST(0.76 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 1, N'Jamal jeo', N'098765789', N'admin', CAST(0x0000A61E013769F1 AS DateTime), CAST(0x793B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1022, CAST(1.50 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.12 AS Decimal(18, 2)), CAST(9.76 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.24 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 3, N'Michel jeo', N'5199991234', N'admin', CAST(0x0000A61E013BF779 AS DateTime), CAST(0x793B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1023, CAST(1.70 AS Decimal(18, 2)), CAST(9.79 AS Decimal(18, 2)), CAST(0.49 AS Decimal(18, 2)), CAST(10.28 AS Decimal(18, 2)), N'Cradit Card', CAST(11.00 AS Decimal(18, 2)), CAST(0.72 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 1, N'Jamal jeo', N'098765789', N'admin', CAST(0x0000A61E013C35CF AS DateTime), CAST(0x793B0B00 AS Date), N'2016-06-24', N'order', N'Pending', N'Paid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Montreal, QC')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1026, CAST(2.00 AS Decimal(18, 2)), CAST(8.93 AS Decimal(18, 2)), CAST(1.16 AS Decimal(18, 2)), CAST(10.09 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.09 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76900EB4D19 AS DateTime), CAST(0xC43C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1035, CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(2.45 AS Decimal(18, 2)), CAST(51.45 AS Decimal(18, 2)), N'Cradit Card', CAST(50.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.45 AS Decimal(18, 2)), NULL, N'SYS89', 5, N'Bill jeo guer', N'9456674', N'5', CAST(0x0000A7A40007E543 AS DateTime), CAST(0xFF3C0B00 AS Date), N'2017-07-12', N'order', N'Pending', N'', N'sddddddddd', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'sd')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1028, CAST(1.00 AS Decimal(18, 2)), CAST(1.26 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(1.32 AS Decimal(18, 2)), N'Cash', CAST(2.00 AS Decimal(18, 2)), CAST(0.68 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76901298DF9 AS DateTime), CAST(0xC43C0B00 AS Date), N'2017-05-05', N'sales', N'', N'', N'aa', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'')
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1030, CAST(2.00 AS Decimal(18, 2)), CAST(5.98 AS Decimal(18, 2)), CAST(0.78 AS Decimal(18, 2)), CAST(6.76 AS Decimal(18, 2)), N'Cash', CAST(10.00 AS Decimal(18, 2)), CAST(3.24 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', CAST(0x0000A76E0154F488 AS DateTime), CAST(0xC93C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_SalesPayment] ([ID], [SalesQty], [Subtotal], [Vat], [totalpayable], [payType], [paidAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (1036, CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.20 AS Decimal(18, 2)), CAST(4.20 AS Decimal(18, 2)), N'Cash', CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.20 AS Decimal(18, 2)), NULL, N'MTQC03', 2, N'Halim khan', N'0987654678', N'fakim', CAST(0x0000A7A400EDD473 AS DateTime), CAST(0xFF3C0B00 AS Date), N'2017-07-12', N'order', N'Pending', N'Unpaid', N'', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, N'Laval, QC, CA')
SET IDENTITY_INSERT [dbo].[tbl_SalesPayment] OFF
/****** Object:  Table [dbo].[tbl_salespaid]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_salespaid](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SP_ID] [bigint] NULL,
	[trxtype] [varchar](50) NULL,
	[paydate] [varchar](50) NULL,
	[paidamount] [decimal](18, 2) NULL,
	[paytype] [varchar](50) NULL,
	[receivedby] [varchar](50) NULL,
	[logtime] [datetime] NULL,
	[logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_salespaid] ON
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (1, 1001, N'POS', N'2016-05-14', CAST(12.34 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A606000D72D7 AS DateTime), CAST(0x613B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (2, 1002, N'order', N'2016-05-15', CAST(9.38 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A607001006F2 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (3, 1003, N'sales', N'2016-05-20', CAST(13.91 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700116C25 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (4, 1004, N'POS', N'2016-05-15', CAST(7.45 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700122F1A AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (5, 1005, N'order', N'2016-05-17', CAST(0.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700C54F6B AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (6, 1005, N'order_Due', N'2016-05-18', CAST(7.86 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700C5D1CE AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (7, 1006, N'order', N'2016-05-16', CAST(4.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700E2B87E AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (8, 1007, N'sales', N'2016-05-16', CAST(7.86 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60700E6F985 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (9, 1008, N'order', N'2016-05-17', CAST(2.10 AS Decimal(18, 2)), N'Cash', N'5', CAST(0x0000A60700EDF869 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (10, 1009, N'POS', N'2016-05-15', CAST(10.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A6070187B5E5 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (12, 1011, N'POS', N'2016-05-17', CAST(9.04 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A6090121191B AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (13, 1012, N'POS', N'2016-05-17', CAST(1.95 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A609012F15EE AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (19, 1018, N'POS', N'2016-05-26', CAST(22.06 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A61201134D6C AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (20, 1019, N'order', N'2016-05-26', CAST(20.00 AS Decimal(18, 2)), N'Cheque', N'admin', CAST(0x0000A612011AE297 AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (21, 1019, N'order_Due', N'2016-05-26', CAST(11.18 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A612011B216A AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (26, 1024, N'POS', N'2017-05-04', CAST(9.03 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76900025761 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (27, 1025, N'sales', N'2017-05-05', CAST(6.28 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76900EAA9BF AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (28, 1026, N'POS', N'2017-05-04', CAST(10.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76900EB4D19 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (29, 1027, N'sales', N'2017-05-04', CAST(2.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A769012968C1 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (31, 1029, N'POS', N'2017-05-07', CAST(59.89 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76C000BFD42 AS DateTime), CAST(0xC73C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (32, 1006, N'order_Due', N'2017-05-17', CAST(0.10 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76D00029124 AS DateTime), CAST(0xC83C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (33, 1027, N'sales_Due', N'2017-05-02', CAST(0.01 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76D0008D679 AS DateTime), CAST(0xC83C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (35, 1031, N'POS', N'2017-05-11', CAST(7.82 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A770011112A8 AS DateTime), CAST(0xCB3C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (36, 1032, N'order', N'2017-06-26', CAST(50.00 AS Decimal(18, 2)), N'Paypal', N'admin', CAST(0x0000A7A2016FFCE7 AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (37, 1033, N'sales', N'2017-06-07', CAST(12.00 AS Decimal(18, 2)), N'Payza', N'admin', CAST(0x0000A7A2018461A4 AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (38, 1034, N'POS', N'2017-07-01', CAST(59.89 AS Decimal(18, 2)), N'Cheque', N'admin', CAST(0x0000A7A3000C143E AS DateTime), CAST(0xFE3C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (39, 1035, N'order', N'2017-07-12', CAST(50.00 AS Decimal(18, 2)), N'Cradit Card', N'5', CAST(0x0000A7A40007E552 AS DateTime), CAST(0xFF3C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (11, 1010, N'POS', N'2016-05-16', CAST(18.15 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x633B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (16, 1015, N'POS', N'2016-05-18', CAST(26.54 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60A012D40AF AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (14, 1013, N'order', N'2016-05-18', CAST(2.08 AS Decimal(18, 2)), N'Cash', N'5', CAST(0x0000A6090152ED28 AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (17, 1016, N'POS', N'2016-05-18', CAST(9.76 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60A01629662 AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (18, 1017, N'order', N'2016-05-19', CAST(9.07 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60A0162E08B AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (30, 1028, N'sales', N'2017-05-05', CAST(1.32 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76901298E23 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (34, 1030, N'POS', N'2017-05-09', CAST(6.76 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A76E0154F488 AS DateTime), CAST(0xC93C0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (15, 1014, N'POS', N'2016-05-18', CAST(6.51 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A60A012B8DC1 AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (22, 1020, N'sales', N'2016-05-27', CAST(3.00 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A612011BF2E4 AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (23, 1021, N'POS', N'2016-06-07', CAST(2.24 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A61E013769F1 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (24, 1022, N'POS', N'2016-06-07', CAST(9.76 AS Decimal(18, 2)), N'Cash', N'admin', CAST(0x0000A61E013BF779 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (25, 1023, N'order', N'2016-06-24', CAST(10.28 AS Decimal(18, 2)), N'Cradit Card', N'admin', CAST(0x0000A61E013C35E2 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_salespaid] ([ID], [SP_ID], [trxtype], [paydate], [paidamount], [paytype], [receivedby], [logtime], [logdate]) VALUES (40, 1036, N'order', N'2017-07-12', CAST(4.00 AS Decimal(18, 2)), N'Cash', N'fakim', CAST(0x0000A7A400EDD48F AS DateTime), CAST(0xFF3C0B00 AS Date))
SET IDENTITY_INSERT [dbo].[tbl_salespaid] OFF
/****** Object:  Table [dbo].[tbl_sales]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_sales](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](150) NULL,
	[ItemName] [varchar](350) NULL,
	[Qty] [decimal](18, 2) NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[DiscRate] [decimal](18, 2) NULL,
	[total] [decimal](18, 2) NULL,
	[Profit] [decimal](18, 2) NULL,
	[SP_ID] [bigint] NULL,
	[logtime] [datetime] NULL,
	[Logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_sales] ON
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (1, N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1001, CAST(0x0000A606000D72D7 AS DateTime), CAST(0x613B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (2, N'32132', N'Razer-Tarantula', CAST(1.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.94 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), 1001, CAST(0x0000A606000D72D7 AS DateTime), CAST(0x613B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (3, N'45111', N'Logitech-G5-Laser-Mouse', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1001, CAST(0x0000A606000D72D7 AS DateTime), CAST(0x613B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (4, N'32132', N'Razer-Tarantula', CAST(1.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.94 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), 1002, CAST(0x0000A607001006EE AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (5, N'3001', N'Money-wallet', CAST(1.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(1.99 AS Decimal(18, 2)), 1002, CAST(0x0000A607001006F2 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (6, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1003, CAST(0x0000A60700116C25 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (10, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1005, CAST(0x0000A60700C54F53 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (11, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1006, CAST(0x0000A60700E2B84A AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (12, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1007, CAST(0x0000A60700E6F968 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (21, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1009, CAST(0x0000A6070187B5E5 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (22, N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1009, CAST(0x0000A6070187B5E5 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (26, N'1200', N'iPhone-Compass-Silver', CAST(2.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(8.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1011, CAST(0x0000A6090121191B AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (27, N'3000200', N'Rice', CAST(-1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(-5.76 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1012, CAST(0x0000A609012F15EE AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (28, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1012, CAST(0x0000A609012F15EE AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (20, N'6677', N'teste', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1008, CAST(0x0000A60700EDF864 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (7, N'3000200', N'Diaper', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1003, CAST(0x0000A60700116C25 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (8, N'45679', N'CocaCola_250ml', CAST(1.00 AS Decimal(18, 2)), CAST(2.90 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(2.81 AS Decimal(18, 2)), CAST(0.81 AS Decimal(18, 2)), 1004, CAST(0x0000A60700122F15 AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (9, N'45680', N'CocaCola_500_ml', CAST(1.00 AS Decimal(18, 2)), CAST(3.78 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3.78 AS Decimal(18, 2)), CAST(0.78 AS Decimal(18, 2)), 1004, CAST(0x0000A60700122F1A AS DateTime), CAST(0x623B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (23, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(2.39 AS Decimal(18, 2)), 1010, CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x633B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (24, N'3000200', N'Rice', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1010, CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x633B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (25, N'45679', N'CocaCola_250ml', CAST(1.00 AS Decimal(18, 2)), CAST(2.90 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(2.81 AS Decimal(18, 2)), CAST(0.81 AS Decimal(18, 2)), 1010, CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x633B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (31, N'3000200', N'Rice', CAST(2.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(11.52 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1015, CAST(0x0000A60A012D40AF AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (32, N'3001', N'Money-wallet', CAST(3.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(11.97 AS Decimal(18, 2)), CAST(1.99 AS Decimal(18, 2)), 1015, CAST(0x0000A60A012D40AF AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (29, N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1013, CAST(0x0000A6090152ED02 AS DateTime), CAST(0x643B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (30, N'3000200', N'Rice', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1014, CAST(0x0000A60A012B8DC1 AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (33, N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1016, CAST(0x0000A60A0162965D AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (34, N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1017, CAST(0x0000A60A0162E05D AS DateTime), CAST(0x653B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (35, N'1235566', N'T-shirt', CAST(3.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(5.94 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1018, CAST(0x0000A61201134D67 AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (36, N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1018, CAST(0x0000A61201134D6C AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (37, N'32132', N'Razer-Tarantula', CAST(1.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.94 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), 1018, CAST(0x0000A61201134D6C AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (38, N'3000200', N'Rice', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1019, CAST(0x0000A612011AE26D AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (39, N'3001', N'Money-wallet', CAST(6.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(23.94 AS Decimal(18, 2)), CAST(1.99 AS Decimal(18, 2)), 1019, CAST(0x0000A612011AE27B AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (44, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1024, CAST(0x0000A76900025753 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (45, N'3001', N'Money-wallet', CAST(1.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(1.99 AS Decimal(18, 2)), 1024, CAST(0x0000A7690002575D AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (46, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1025, CAST(0x0000A76900EAA9AC AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (47, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1025, CAST(0x0000A76900EAA9AC AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (48, N'3001', N'Money-wallet', CAST(1.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(1.99 AS Decimal(18, 2)), 1026, CAST(0x0000A76900EB4D19 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (49, N'32132', N'Razer-Tarantula', CAST(1.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.94 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), 1026, CAST(0x0000A76900EB4D19 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (50, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1027, CAST(0x0000A7690129689B AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (52, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1029, CAST(0x0000A76C000BFD2F AS DateTime), CAST(0xC73C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (53, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1029, CAST(0x0000A76C000BFD3D AS DateTime), CAST(0xC73C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (56, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1031, CAST(0x0000A770011112A4 AS DateTime), CAST(0xCB3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (57, N'32132', N'Razer-Tarantula', CAST(1.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(4.94 AS Decimal(18, 2)), CAST(2.94 AS Decimal(18, 2)), 1031, CAST(0x0000A770011112A8 AS DateTime), CAST(0xCB3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (58, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1032, CAST(0x0000A7A2016FFCE3 AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (59, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1033, CAST(0x0000A7A20184619F AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (60, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1033, CAST(0x0000A7A20184619F AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (61, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1033, CAST(0x0000A7A20184619F AS DateTime), CAST(0xFD3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (62, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1034, CAST(0x0000A7A3000C13F8 AS DateTime), CAST(0xFE3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (40, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1020, CAST(0x0000A612011BF2C3 AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (63, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1034, CAST(0x0000A7A3000C140F AS DateTime), CAST(0xFE3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (41, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1021, CAST(0x0000A61E013769F1 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (42, N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1022, CAST(0x0000A61E013BF779 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (43, N'3000200', N'Rice', CAST(1.70 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(9.79 AS Decimal(18, 2)), CAST(1.76 AS Decimal(18, 2)), 1023, CAST(0x0000A61E013C35B8 AS DateTime), CAST(0x793B0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (51, N'45682', N'Dove_shampoo', CAST(1.00 AS Decimal(18, 2)), CAST(1.33 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(1.26 AS Decimal(18, 2)), CAST(0.26 AS Decimal(18, 2)), 1028, CAST(0x0000A76901298DF0 AS DateTime), CAST(0xC43C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (54, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1030, CAST(0x0000A76E0154F484 AS DateTime), CAST(0xC93C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (55, N'1235566', N'T-shirt', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(0.48 AS Decimal(18, 2)), 1030, CAST(0x0000A76E0154F488 AS DateTime), CAST(0xC93C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (64, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1035, CAST(0x0000A7A40007E50B AS DateTime), CAST(0xFF3C0B00 AS Date))
INSERT [dbo].[tbl_sales] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [Profit], [SP_ID], [logtime], [Logdate]) VALUES (65, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), 1036, CAST(0x0000A7A400EDD45B AS DateTime), CAST(0xFF3C0B00 AS Date))
SET IDENTITY_INSERT [dbo].[tbl_sales] OFF
/****** Object:  Table [dbo].[tbl_ReturnPayment]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReturnPayment](
	[ID] [bigint] IDENTITY(4001,1) NOT NULL,
	[ReturnQty] [decimal](18, 2) NULL,
	[Subtotal] [decimal](18, 0) NULL,
	[Vat] [decimal](18, 2) NULL,
	[totalReturnable] [decimal](18, 2) NULL,
	[payType] [varchar](150) NULL,
	[ReturnAmount] [decimal](18, 2) NULL,
	[changeAmount] [decimal](18, 2) NULL,
	[dueAmount] [decimal](18, 2) NULL,
	[note] [varchar](450) NULL,
	[ShopId] [varchar](50) NULL,
	[CustID] [bigint] NULL,
	[CustName] [varchar](150) NULL,
	[CustContact] [varchar](150) NULL,
	[ServedBy] [varchar](150) NULL,
	[SalesInvoiceID] [bigint] NULL,
	[Logtime] [datetime] NULL,
	[Logdate] [date] NULL,
	[ordedate] [varchar](150) NULL,
	[trxtype] [varchar](150) NULL,
	[order_status] [varchar](150) NULL,
	[payment_status] [varchar](150) NULL,
	[comment] [varchar](150) NULL,
	[discount] [decimal](18, 2) NULL,
	[dsirate] [decimal](18, 2) NULL,
	[vatrate] [decimal](18, 2) NULL,
	[shippingFee] [varchar](150) NULL,
	[shippingaddress] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SalesID/Invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_ReturnPayment', @level2type=N'COLUMN',@level2name=N'ID'
GO
SET IDENTITY_INSERT [dbo].[tbl_ReturnPayment] ON
INSERT [dbo].[tbl_ReturnPayment] ([ID], [ReturnQty], [Subtotal], [Vat], [totalReturnable], [payType], [ReturnAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [SalesInvoiceID], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (4001, CAST(2.00 AS Decimal(18, 2)), CAST(11 AS Decimal(18, 0)), CAST(0.57 AS Decimal(18, 2)), CAST(12.06 AS Decimal(18, 2)), N'Paypal', CAST(12.06 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 2, N'Halim khan', N'0987654678', N'admin', 1001, CAST(0x0000A612011D4212 AS DateTime), CAST(0x6D3B0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_ReturnPayment] ([ID], [ReturnQty], [Subtotal], [Vat], [totalReturnable], [payType], [ReturnAmount], [changeAmount], [dueAmount], [note], [ShopId], [CustID], [CustName], [CustContact], [ServedBy], [SalesInvoiceID], [Logtime], [Logdate], [ordedate], [trxtype], [order_status], [payment_status], [comment], [discount], [dsirate], [vatrate], [shippingFee], [shippingaddress]) VALUES (4002, CAST(1.00 AS Decimal(18, 2)), CAST(49 AS Decimal(18, 0)), CAST(2.45 AS Decimal(18, 2)), CAST(51.45 AS Decimal(18, 2)), N'Cash', CAST(51.45 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'SYS89', 0, N'Guest', N'121', N'admin', 1034, CAST(0x0000A7A30010197B AS DateTime), CAST(0xFE3C0B00 AS Date), NULL, N'POS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_ReturnPayment] OFF
/****** Object:  Table [dbo].[tbl_Return]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Return](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](150) NULL,
	[ItemName] [varchar](350) NULL,
	[Qty] [decimal](18, 2) NULL,
	[Price] [decimal](18, 2) NULL,
	[DiscRate] [decimal](18, 2) NULL,
	[total] [decimal](18, 2) NULL,
	[RP_ID] [bigint] NULL,
	[logtime] [datetime] NULL,
	[Logdate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Return] ON
INSERT [dbo].[tbl_Return] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [RP_ID], [logtime], [Logdate]) VALUES (1, N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 4001, CAST(0x0000A612011D420E AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_Return] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [RP_ID], [logtime], [Logdate]) VALUES (2, N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), 4001, CAST(0x0000A612011D420E AS DateTime), CAST(0x6D3B0B00 AS Date))
INSERT [dbo].[tbl_Return] ([ID], [ItemCode], [ItemName], [Qty], [Price], [DiscRate], [total], [RP_ID], [logtime], [Logdate]) VALUES (3, N'112233', N'Advance POS', CAST(1.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), 4002, CAST(0x0000A7A300101976 AS DateTime), CAST(0xFE3C0B00 AS Date))
SET IDENTITY_INSERT [dbo].[tbl_Return] OFF
/****** Object:  Table [dbo].[tbl_purchase_payment]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_purchase_payment](
	[purchaseCode] [bigint] IDENTITY(99101,1) NOT NULL,
	[supplierid] [varchar](450) NOT NULL,
	[supplier] [varchar](450) NOT NULL,
	[puchase_date] [varchar](450) NOT NULL,
	[itemQty] [decimal](18, 2) NOT NULL,
	[total] [decimal](18, 2) NOT NULL,
	[payment] [decimal](18, 2) NOT NULL,
	[method] [varchar](450) NOT NULL,
	[Comment] [varchar](550) NOT NULL,
	[ShopId] [varchar](50) NOT NULL,
	[purchaseby] [varchar](450) NOT NULL,
	[logtime] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase_payment] ON
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99101, N'59102', N'Cosco', N'2016-05-15', CAST(2.00 AS Decimal(18, 2)), CAST(13.25 AS Decimal(18, 2)), CAST(13.25 AS Decimal(18, 2)), N'Cash', N'Done', N'SYS89', N'admin', CAST(0x0000A6070009AC15 AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99102, N'59101', N'Jeo barat', N'2016-05-15', CAST(4.00 AS Decimal(18, 2)), CAST(7.92 AS Decimal(18, 2)), CAST(7.92 AS Decimal(18, 2)), N'Cash', N'done', N'SYS89', N'admin', CAST(0x0000A6070011A7D7 AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99103, N'59101', N'Jeo barat', N'2016-05-05', CAST(2.00 AS Decimal(18, 2)), CAST(3.96 AS Decimal(18, 2)), CAST(3.96 AS Decimal(18, 2)), N'Cash', N'', N'SYS89', N'admin', CAST(0x0000A609013B4CFD AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99104, N'59101', N'Jeo barat', N'2016-05-17', CAST(6.50 AS Decimal(18, 2)), CAST(20.56 AS Decimal(18, 2)), CAST(20.56 AS Decimal(18, 2)), N'Cash', N'', N'SYS89', N'admin', CAST(0x0000A60901405FC4 AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99105, N'59102', N'Cosco', N'2016-05-18', CAST(2.00 AS Decimal(18, 2)), CAST(11.52 AS Decimal(18, 2)), CAST(11.52 AS Decimal(18, 2)), N'Cash', N'', N'SYS89', N'admin', CAST(0x0000A60901417164 AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99106, N'59101', N'Jeo barat', N'2016-05-18', CAST(5.20 AS Decimal(18, 2)), CAST(28.18 AS Decimal(18, 2)), CAST(28.18 AS Decimal(18, 2)), N'Cash', N'', N'SYS89', N'admin', CAST(0x0000A6090141C568 AS DateTime))
INSERT [dbo].[tbl_purchase_payment] ([purchaseCode], [supplierid], [supplier], [puchase_date], [itemQty], [total], [payment], [method], [Comment], [ShopId], [purchaseby], [logtime]) VALUES (99107, N'59102', N'Cosco', N'2016-05-26', CAST(176.00 AS Decimal(18, 2)), CAST(781.44 AS Decimal(18, 2)), CAST(781.44 AS Decimal(18, 2)), N'Cash', N'All products are good for stock', N'SYS89', N'admin', CAST(0x0000A6120119EB10 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_purchase_payment] OFF
/****** Object:  Table [dbo].[tbl_purchase]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_purchase](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[supplierid] [varchar](450) NULL,
	[purchaseCode] [varchar](450) NULL,
	[puchase_date] [varchar](450) NULL,
	[product_id] [varchar](450) NULL,
	[product_name] [varchar](450) NOT NULL,
	[qty] [decimal](18, 2) NOT NULL,
	[unit_price] [decimal](18, 2) NOT NULL,
	[total] [decimal](18, 2) NOT NULL,
	[expiry_date] [varchar](450) NULL,
	[logtime] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase] ON
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (1, N'59102', N'99101', N'2016-05-15', N'3000200', N'Diaper', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), NULL, CAST(0x0000A6070009AC15 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (2, N'59102', N'99101', N'2016-05-15', N'1245655', N'single-burger-Lg', CAST(1.00 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), NULL, CAST(0x0000A6070009AC15 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (3, N'59101', N'99102', N'2016-05-15', N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), NULL, CAST(0x0000A6070011A7BB AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (4, N'59101', N'99102', N'2016-05-15', N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), NULL, CAST(0x0000A6070011A7D3 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (5, N'59101', N'99102', N'2016-05-15', N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), NULL, CAST(0x0000A6070011A7D3 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (6, N'59101', N'99102', N'2016-05-15', N'1235566', N'tosot', CAST(1.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), NULL, CAST(0x0000A6070011A7D3 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (7, N'59101', N'99103', N'2016-05-05', N'1235566', N'tosot', CAST(2.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(3.96 AS Decimal(18, 2)), NULL, CAST(0x0000A609013B4CE5 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (8, N'59102', N'99104', N'2016-05-17', N'3000200', N'Rice', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), NULL, CAST(0x0000A609013E3655 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (9, N'59102', N'99104', N'2016-05-17', N'3000200', N'Rice', CAST(1.70 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(9.79 AS Decimal(18, 2)), NULL, CAST(0x0000A609013E3667 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (10, N'59101', N'99104', N'2016-05-17', N'1200', N'iPhone-Compass-Silver', CAST(1.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), NULL, CAST(0x0000A60901405FA4 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (11, N'59101', N'99104', N'2016-05-17', N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), NULL, CAST(0x0000A60901405FB2 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (12, N'59101', N'99104', N'2016-05-17', N'1235566', N'tosot', CAST(4.00 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(7.92 AS Decimal(18, 2)), NULL, CAST(0x0000A60901405FC4 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (13, N'59102', N'99105', N'2016-05-18', N'3000200', N'Rice', CAST(2.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(11.52 AS Decimal(18, 2)), NULL, CAST(0x0000A6090141714D AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (14, N'59101', N'99106', N'2016-05-18', N'3000200', N'Rice', CAST(1.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(5.76 AS Decimal(18, 2)), NULL, CAST(0x0000A6090141C53E AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (15, N'59101', N'99106', N'2016-05-18', N'3000200', N'Rice', CAST(1.50 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(8.64 AS Decimal(18, 2)), NULL, CAST(0x0000A6090141C555 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (16, N'59101', N'99106', N'2016-05-18', N'3000200', N'Rice', CAST(1.70 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(9.79 AS Decimal(18, 2)), NULL, CAST(0x0000A6090141C568 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (17, N'59101', N'99106', N'2016-05-18', N'3001', N'Money-wallet', CAST(1.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), NULL, CAST(0x0000A6090141C568 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (18, N'59102', N'99107', N'2016-05-26', N'89099807', N'single-burger-combo', CAST(110.00 AS Decimal(18, 2)), CAST(5.94 AS Decimal(18, 2)), CAST(653.40 AS Decimal(18, 2)), NULL, CAST(0x0000A6120119EAD8 AS DateTime))
INSERT [dbo].[tbl_purchase] ([ID], [supplierid], [purchaseCode], [puchase_date], [product_id], [product_name], [qty], [unit_price], [total], [expiry_date], [logtime]) VALUES (19, N'59102', N'99107', N'2016-05-26', N'89099808', N'burger king', CAST(66.00 AS Decimal(18, 2)), CAST(1.94 AS Decimal(18, 2)), CAST(128.04 AS Decimal(18, 2)), NULL, CAST(0x0000A6120119EB10 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_purchase] OFF
/****** Object:  Table [dbo].[tbl_Page]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Page](
	[PageID] [bigint] IDENTITY(101,1) NOT NULL,
	[PageName] [varchar](150) NULL,
	[PageUrl] [varchar](150) NULL,
	[Status] [int] NULL,
	[Module] [varchar](150) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Page] ON
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (101, N'Add Item', N'AddItem.aspx', 1, N'Items')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (102, N'Manage Items', N'ManageItems.aspx', 1, N'Items')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (103, N'Add Customer', N'AddCustomer.aspx', 1, N'Customers')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (104, N'Manage Customers', N'ManageCustomers.aspx', 1, N'Customers')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (105, N'Add user', N'Adduser.aspx', 1, N'Users')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (106, N'Manage Users', N'ManageUsers.aspx', 1, N'Users')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (107, N'Sales Register', N'SalesRegister.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (108, N'POS_printPage', N'POS_printPage.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (109, N'Reports', N'Reports.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (110, N'Profile Page', N'ProfilePage.aspx', 1, N'Settings')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (111, N'Default', N'Default.aspx', 1, N'Dashboard')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (112, N'Settings', N'Settings.aspx', 1, N'Settings')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (113, N'Category', N'Category.aspx', 1, N'Items')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (114, N'TaskList', N'TaskList.aspx', 1, N'Settings')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (115, N'AddTask', N'AddTask.aspx', 1, N'Settings')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (116, N'Stock Item Report', N'StockItemReport.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (117, N'Sales Report', N'SalesReport.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (118, N'Error404', N'404.aspx', 1, N'ErrorPage')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (119, N'Error500', N'500.aspx', 1, N'ErrorPage')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (120, N'Item List', N'ItemList.aspx', 1, N'Items')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (121, N'Chart Report', N'ChartReport.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (140, N'Employee Profile', N'EmployeesProfile.aspx', 1, N'Users')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (141, N'User privilege', N'User_privilege.aspx', 1, N'Users')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (124, N'New Order', N'NewOrder.aspx', 1, N'Order')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (125, N'Order History', N'Order_history.aspx', 1, N'Order')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (126, N'Order Invoice', N'Order_Invoice.aspx', 1, N'Order')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (127, N'Take Payment', N'TakePayment.aspx', 1, N'Order')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (128, N'Change Status', N'ChangeStatus.aspx', 1, N'Order')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (129, N'New Purchase', N'NewPurchase.aspx', 1, N'Purchase')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (130, N'Purchase History', N'Purchase_history.aspx', 1, N'Purchase')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (131, N'Purchase Invoice', N'Purchase_invoice.aspx', 1, N'Purchase')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (132, N'Page Monitor', N'AccessLog.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (133, N'New Sale', N'NewSale.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (134, N'Sales History', N'Sales_history.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (135, N'Sales Invoice', N'Sales_invoice.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (136, N'Take Payment', N'TakePayment.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (137, N'Terminal Point', N'ShopList.aspx', 1, N'Settings')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (138, N'Add Supplier', N'AddSupplier.aspx', 1, N'Suppliers')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (139, N'Manage Supplier', N'ManageSuppliers.aspx', 1, N'Suppliers')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (142, N'Return Sales', N'ReturnSales_POS.aspx', 1, N'Sales')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (143, N'Return Report', N'ReturnReports.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (144, N'Return Print Page', N'ReturnPrintPage.aspx', 1, N'Report')
INSERT [dbo].[tbl_Page] ([PageID], [PageName], [PageUrl], [Status], [Module]) VALUES (145, N'UploadItems', N'UploadItems.aspx', 1, N'Items')
SET IDENTITY_INSERT [dbo].[tbl_Page] OFF
/****** Object:  Table [dbo].[tbl_Item]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Item](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](450) NOT NULL,
	[ItemName] [varchar](450) NOT NULL,
	[PurchasePrice] [decimal](18, 2) NOT NULL,
	[RetailPrice] [decimal](18, 2) NOT NULL,
	[ItemQty] [decimal](18, 2) NULL,
	[ItemCategory] [varchar](450) NOT NULL,
	[Tax] [decimal](18, 2) NULL,
	[Discount] [decimal](18, 2) NULL,
	[LogBy] [varchar](150) NULL,
	[Lastupdateby] [varchar](150) NULL,
	[Photo] [varchar](350) NULL,
	[Description] [varchar](499) NULL,
	[Status] [int] NULL,
	[Lastupdate] [datetime] NULL,
	[Logtime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Item] PRIMARY KEY CLUSTERED 
(
	[ItemCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = active  , 2 = Inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Item', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_Item] ON
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (28, N'112233', N'Advance POS', CAST(45.00 AS Decimal(18, 2)), CAST(49.00 AS Decimal(18, 2)), CAST(215.00 AS Decimal(18, 2)), N'Miscellaneous', CAST(0.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/112233.jpg', N'', 1, CAST(0x0000A7A701869FB0 AS DateTime), CAST(0x0000A4A800D891E9 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (11, N'1200', N'iPhone-Compass-Silver', CAST(3.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(64.00 AS Decimal(18, 2)), N'Mobile', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'admin', N'~/ItemsPhoto/1200.png', NULL, 1, CAST(0x0000A60700094A4E AS DateTime), CAST(0x0000A410010A4C4F AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (36, N'123444', N'Please enter Item pro', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'invoadmin', N'Del_by: invoadmin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A60400C28236 AS DateTime), CAST(0x0000A60400C17ED7 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (38, N'1234442', N'New Test', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A6060189976E AS DateTime), CAST(0x0000A60601866CE3 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (37, N'12344443', N'Please enter Item pro', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'invoadmin', N'Del_by: invoadmin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A60400C286D2 AS DateTime), CAST(0x0000A60400C1C2CA AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (33, N'1235566', N'T-shirt', CAST(1.50 AS Decimal(18, 2)), CAST(1.98 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), N'Diet foods', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/1235566.png', NULL, 1, CAST(0x0000A611018460DB AS DateTime), CAST(0x0000A58C0118A70B AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (29, N'1245655', N'single-burger-Lg', CAST(5.10 AS Decimal(18, 2)), CAST(7.49 AS Decimal(18, 2)), CAST(88.00 AS Decimal(18, 2)), N'Snack foods', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/1245655.jpg', N'', 1, CAST(0x0000A76801442017 AS DateTime), CAST(0x0000A4AF009C17C6 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (14, N'2100', N'iPhone-Black-Apple', CAST(579.00 AS Decimal(18, 2)), CAST(598.00 AS Decimal(18, 2)), CAST(74.00 AS Decimal(18, 2)), N'Mobile', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'admin', N'~/ItemsPhoto/2100.png', NULL, 1, CAST(0x0000A5A20188CEB7 AS DateTime), CAST(0x0000A410010D66E2 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (67, N'23948998', N'Newimageresize', CAST(12.00 AS Decimal(18, 2)), CAST(22.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Candy', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/23948998.png', N'Descriptdssdfsdf', 2, CAST(0x0000A76801473A61 AS DateTime), CAST(0x0000A76801467FA3 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (26, N'3000200', N'Rice', CAST(4.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(106.70 AS Decimal(18, 2)), N'Rice and rice dishes', CAST(0.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/3000200.jpg', N'', 1, CAST(0x0000A76801495594 AS DateTime), CAST(0x0000A44501081879 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (20, N'3001', N'Money-wallet', CAST(2.00 AS Decimal(18, 2)), CAST(3.99 AS Decimal(18, 2)), CAST(64.00 AS Decimal(18, 2)), N'Drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/3001.png', NULL, 1, CAST(0x0000A44501830F3C AS DateTime), CAST(0x0000A425015F5BCE AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (1, N'32132', N'Razer-Tarantula', CAST(2.00 AS Decimal(18, 2)), CAST(4.99 AS Decimal(18, 2)), CAST(68.00 AS Decimal(18, 2)), N'Computer', CAST(3.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/32132.png', NULL, 1, CAST(0x0000A5090184608E AS DateTime), CAST(0x0000A3CD0174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (27, N'34090', N'Lakmelip', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(21.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/34090.png', NULL, 2, CAST(0x0000A446017E60F3 AS DateTime), CAST(0x0000A4460176961F AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (21, N'43', N'fd', CAST(4.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A44301879069 AS DateTime), CAST(0x0000A442017AE0AD AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (70, N'4345465', N'Newimageresize', CAST(43.00 AS Decimal(18, 2)), CAST(54.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Butter and margarine', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/4345465.png', N'4545', 2, CAST(0x0000A76B014D0EA0 AS DateTime), CAST(0x0000A76B014CFB47 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (69, N'43986593444', N'newtest', CAST(23.00 AS Decimal(18, 2)), CAST(33.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/item.png', N'er', 2, CAST(0x0000A76B014D12A1 AS DateTime), CAST(0x0000A76B014BC1ED AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (15, N'45111', N'Laser-Mouse', CAST(3.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), CAST(112.00 AS Decimal(18, 2)), N'Computer', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'', N'admin', N'~/ItemsPhoto/45111.png', NULL, 1, CAST(0x0000A6120101EA44 AS DateTime), CAST(0x0000A41001169C94 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (2, N'45678', N'CocaCola200ml', CAST(1.00 AS Decimal(18, 2)), CAST(1.73 AS Decimal(18, 2)), CAST(17.60 AS Decimal(18, 2)), N'Drinks', CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/45678.png', NULL, 1, CAST(0x0000A3EA0182D99C AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (4, N'45679', N'CocaCola_250ml', CAST(2.00 AS Decimal(18, 2)), CAST(2.90 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), N'Drinks', CAST(0.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/45679.png', NULL, 1, CAST(0x0000A4460004F3BF AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (5, N'45680', N'CocaCola_500_ml', CAST(3.00 AS Decimal(18, 2)), CAST(3.78 AS Decimal(18, 2)), CAST(38.00 AS Decimal(18, 2)), N'Drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/45678.png', NULL, 1, CAST(0x0000A3DE00062FF5 AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (6, N'45681', N'CocaCola-1litter', CAST(1.00 AS Decimal(18, 2)), CAST(1.30 AS Decimal(18, 2)), CAST(32.60 AS Decimal(18, 2)), N'Drinks', CAST(1.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/45681.png', N'', 1, CAST(0x0000A76B014D889A AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (7, N'45682', N'Dove_shampoo', CAST(1.00 AS Decimal(18, 2)), CAST(1.33 AS Decimal(18, 2)), CAST(104.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(3.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), NULL, N'admin', N'~/ItemsPhoto/45682.png', NULL, 1, CAST(0x0000A5090184905A AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (17, N'5001', N'Gold 21carat', CAST(130.00 AS Decimal(18, 2)), CAST(140.00 AS Decimal(18, 2)), CAST(109.00 AS Decimal(18, 2)), N'Metal', CAST(0.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), N'fakim', N'admin', N'~/ItemsPhoto/5001.png', NULL, 1, CAST(0x0000A4FF00DEE457 AS DateTime), CAST(0x0000A41D00DE89FF AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (18, N'5002', N'Georgette cloth 50mt', CAST(122.00 AS Decimal(18, 2)), CAST(144.00 AS Decimal(18, 2)), CAST(777.00 AS Decimal(18, 2)), N'Cloth', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A441017AF91E AS DateTime), CAST(0x0000A422016CF425 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (8, N'5091', N'CocaCola250mll', CAST(1.00 AS Decimal(18, 2)), CAST(1.77 AS Decimal(18, 2)), CAST(34.60 AS Decimal(18, 2)), N'Drinks', CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A4420170B265 AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (10, N'5092', N'CocaCola250mll', CAST(1.00 AS Decimal(18, 2)), CAST(1.73 AS Decimal(18, 2)), CAST(39.60 AS Decimal(18, 2)), N'Liquid', CAST(3.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A3DD0187CA68 AS DateTime), CAST(0x0000A3D50174A096 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (30, N'6567', N'test', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(127.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A5F80152D171 AS DateTime), CAST(0x0000A58A0105EF68 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (31, N'6677', N'chicken leg_1lb', CAST(2.00 AS Decimal(18, 2)), CAST(2.49 AS Decimal(18, 2)), CAST(19.00 AS Decimal(18, 2)), N'Chicken', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/6677.png', N'', 1, CAST(0x0000A7680149221A AS DateTime), CAST(0x0000A58A01069C54 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (32, N'6688', N'tester', CAST(2.00 AS Decimal(18, 2)), CAST(3.00 AS Decimal(18, 2)), CAST(32.00 AS Decimal(18, 2)), N'Crackers', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/6688.png', NULL, 1, CAST(0x0000A605015605B0 AS DateTime), CAST(0x0000A58A0107FC17 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (25, N'6765', N'Love Candy', CAST(1.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), CAST(685.00 AS Decimal(18, 2)), N'Candy', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/6765.png', N'', 1, CAST(0x0000A7680149E688 AS DateTime), CAST(0x0000A4440006F377 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (16, N'7001', N'Georgette cloth 100mt', CAST(3.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), N'Cloth', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'fakim', N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A441017A88BE AS DateTime), CAST(0x0000A41D00DDD80B AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (68, N'85697569', N'newtest', CAST(12.00 AS Decimal(18, 2)), CAST(14.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Candy', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/85697569.jpg', N'Descriptdssdfsdfsdfdf', 1, CAST(0x0000A76B014D218D AS DateTime), CAST(0x0000A76801475DDB AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (41, N'89099807', N'single-burger-combo', CAST(4.42 AS Decimal(18, 2)), CAST(5.94 AS Decimal(18, 2)), CAST(110.00 AS Decimal(18, 2)), N'Snack foods', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/89099807.jpg', N'', 1, CAST(0x0000A76801438361 AS DateTime), CAST(0x0000A60601888339 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (40, N'89099808', N'burger king', CAST(1.42 AS Decimal(18, 2)), CAST(1.94 AS Decimal(18, 2)), CAST(66.00 AS Decimal(18, 2)), N'Snack foods', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/89099808.png', N'', 1, CAST(0x0000A768014AC9E1 AS DateTime), CAST(0x0000A60601884EFA AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (34, N'89099809', N'New Test', CAST(12.00 AS Decimal(18, 2)), CAST(16.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A5F80116AA66 AS DateTime), CAST(0x0000A5F80115801C AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (35, N'89099888', N'New Test', CAST(12.00 AS Decimal(18, 2)), CAST(16.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', N'~/ItemsPhoto/item.png', NULL, 2, CAST(0x0000A5F801165ED8 AS DateTime), CAST(0x0000A5F80115A3FF AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (42, N'898945', N'test', CAST(12.00 AS Decimal(18, 2)), CAST(13.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Chicken', CAST(0.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), N'admin', NULL, N'~/ItemsPhoto/item.png', N'df', 1, CAST(0x0000A69F01102155 AS DateTime), CAST(0x0000A69F01102155 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (19, N'9003', N'T-Shirt', CAST(33.00 AS Decimal(18, 2)), CAST(35.00 AS Decimal(18, 2)), CAST(771.00 AS Decimal(18, 2)), N'Cloth', CAST(0.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), N'admin', N'Del_by: admin', NULL, NULL, 2, CAST(0x0000A7690146F02F AS DateTime), CAST(0x0000A42401253BAA AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (58, N'909899', N'Advance POS', CAST(4.50 AS Decimal(18, 2)), CAST(5.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Miscellaneous', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/item.png', N'des', 1, CAST(0x0000A6F801232A64 AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (59, N'909900', N'iPhone-Compass-Silver', CAST(5.50 AS Decimal(18, 2)), CAST(6.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Mobile', CAST(0.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/item.png', N'des', 1, CAST(0x0000A6F801232A64 AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (60, N'909901', N'Please enter Item pro', CAST(6.50 AS Decimal(18, 2)), CAST(7.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/item.png', N'des', 1, CAST(0x0000A6F801232A64 AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (61, N'909902', N'New Test', CAST(7.50 AS Decimal(18, 2)), CAST(8.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Cereals', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/item.png', N'des', 1, CAST(0x0000A76901415F8A AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (62, N'909903', N'Chicken', CAST(8.50 AS Decimal(18, 2)), CAST(9.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Alcoholic drinks', CAST(0.00 AS Decimal(18, 2)), CAST(1.20 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/909903.png', N'des', 1, CAST(0x0000A76801489BED AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (63, N'909904', N'Banana', CAST(9.50 AS Decimal(18, 2)), CAST(10.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Diet foods', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/909904.png', N'des', 1, CAST(0x0000A76901390640 AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
INSERT [dbo].[tbl_Item] ([ID], [ItemCode], [ItemName], [PurchasePrice], [RetailPrice], [ItemQty], [ItemCategory], [Tax], [Discount], [LogBy], [Lastupdateby], [Photo], [Description], [Status], [Lastupdate], [Logtime]) VALUES (64, N'909905', N'single-burger-Lg', CAST(10.50 AS Decimal(18, 2)), CAST(11.60 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'Snack foods', CAST(0.00 AS Decimal(18, 2)), CAST(2.30 AS Decimal(18, 2)), N'admin', N'admin', N'~/ItemsPhoto/909905.png', N'description', 1, CAST(0x0000A76B014D9C7F AS DateTime), CAST(0x0000A6F801232A64 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Item] OFF
/****** Object:  Table [dbo].[tbl_hit_counter]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_hit_counter](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[userID] [varchar](150) NULL,
	[datetime] [datetime] NULL,
	[timelog] [time](7) NULL,
	[IPaddress] [varchar](150) NULL,
	[count] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_hit_counter] ON
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (1, N'admin', CAST(0x0000A60601279C79 AS DateTime), CAST(0x07906D0B5B960000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (2, N'admin', CAST(0x0000A606012843C8 AS DateTime), CAST(0x07B06624B0960000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (3, N'admin', CAST(0x0000A606012FF5B6 AS DateTime), CAST(0x07908F199A9A0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (4, N'admin', CAST(0x0000A6060139EC84 AS DateTime), CAST(0x0780A381AB9F0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (5, N'admin', CAST(0x0000A60700BDEE92 AS DateTime), CAST(0x07C0D7B79A600000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (6, N'admin', CAST(0x0000A60700C8CABC AS DateTime), CAST(0x07306CC820660000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (7, N'admin', CAST(0x0000A60700CAD2C0 AS DateTime), CAST(0x0750B04629670000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (10, N'jamal', CAST(0x0000A60700DB5E0F AS DateTime), CAST(0x07702977936F0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (13, N'admin', CAST(0x0000A607017E039A AS DateTime), CAST(0x0770566A4DC20000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (14, N'fakim', CAST(0x0000A607018B76BE AS DateTime), CAST(0x0790D6AF24C90000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (15, N'admin', CAST(0x0000A60800000D5F AS DateTime), CAST(0x072007CD06000000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (16, N'fakim', CAST(0x0000A608000033F4 AS DateTime), CAST(0x07D0AD6C1A000000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (17, N'admin', CAST(0x0000A6080000505E AS DateTime), CAST(0x07407AE028000000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (18, N'fakim', CAST(0x0000A6080000BA4B AS DateTime), CAST(0x07A0E5C05E000000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (19, N'admin', CAST(0x0000A6080000D0C8 AS DateTime), CAST(0x07000F316A000000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (20, N'admin', CAST(0x0000A6080006D96E AS DateTime), CAST(0x074028D77B030000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (22, N'admin', CAST(0x0000A6080024CD53 AS DateTime), CAST(0x07A0ECEEB7120000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (23, N'admin', CAST(0x0000A60800C17A7B AS DateTime), CAST(0x07A04F8168620000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (24, N'admin', CAST(0x0000A60800C69ED0 AS DateTime), CAST(0x0700B60606650000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (25, N'admin', CAST(0x0000A60800C7581E AS DateTime), CAST(0x07F0F94664650000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (26, N'admin', CAST(0x0000A60901316065 AS DateTime), CAST(0x07B0FC91529B0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (27, N'admin', CAST(0x0000A60901463F27 AS DateTime), CAST(0x07D0AD09F0A50000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (28, N'admin', CAST(0x0000A6090153070F AS DateTime), CAST(0x0770093770AC0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (30, N'admin', CAST(0x0000A60A012B7471 AS DateTime), CAST(0x07E04E844F980000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (31, N'admin', CAST(0x0000A60A012D7FF8 AS DateTime), CAST(0x0750E1CB59990000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (32, N'admin', CAST(0x0000A60A0139121E AS DateTime), CAST(0x07904A6C3C9F0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (33, N'admin', CAST(0x0000A60A015808D7 AS DateTime), CAST(0x0720B029FCAE0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (34, N'admin', CAST(0x0000A61101728AE4 AS DateTime), CAST(0x07D0FFB977BC0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (35, N'admin', CAST(0x0000A6110173619B AS DateTime), CAST(0x0750C3EFE4BC0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (36, N'admin', CAST(0x0000A61201019C6C AS DateTime), CAST(0x0730261A07830000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (37, N'admin', CAST(0x0000A612011DE547 AS DateTime), CAST(0x0720D2FD69910000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (38, N'admin', CAST(0x0000A612011E79A6 AS DateTime), CAST(0x0740F974B5910000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (39, N'admin', CAST(0x0000A61E012C1EED AS DateTime), CAST(0x0760CB3AA6980000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (40, N'admin', CAST(0x0000A61E012CC378 AS DateTime), CAST(0x0700A9EBF9980000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (41, N'admin', CAST(0x0000A61E014C6B04 AS DateTime), CAST(0x07300B9913A90000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (42, N'admin', CAST(0x0000A63F0173CE66 AS DateTime), CAST(0x079069451CBD0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (43, N'admin', CAST(0x0000A69F010A3E79 AS DateTime), CAST(0x07902D316B870000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (44, N'admin', CAST(0x0000A69F01177FC0 AS DateTime), CAST(0x0750101A298E0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (45, N'admin', CAST(0x0000A6A000E62328 AS DateTime), CAST(0x07B05ACC0D750000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (46, N'admin', CAST(0x0000A6AE0185AD05 AS DateTime), CAST(0x0760400B33C60000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (47, N'admin', CAST(0x0000A6F8011F2BCE AS DateTime), CAST(0x07F0731210920000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (48, N'admin', CAST(0x0000A6F801679838 AS DateTime), CAST(0x0700D136E6B60000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (49, N'admin', CAST(0x0000A6F8017B0E16 AS DateTime), CAST(0x0790631FCCC00000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (50, N'fakim', CAST(0x0000A6F8017B56BA AS DateTime), CAST(0x07C0DA11F1C00000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (51, N'admin', CAST(0x0000A6F8017CC632 AS DateTime), CAST(0x07104CF9ABC10000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (52, N'admin', CAST(0x0000A6F8017DD6BF AS DateTime), CAST(0x0720BB9936C20000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (53, N'admin', CAST(0x0000A6F8018101F9 AS DateTime), CAST(0x07903D36D3C30000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (54, N'admin', CAST(0x0000A6F801827A43 AS DateTime), CAST(0x0750369A92C40000 AS Time), N'::1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (57, N'admin', CAST(0x0000A73100CA2CC9 AS DateTime), CAST(0x07E0BBDCD4660000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (58, N'admin', CAST(0x0000A73100CA40B3 AS DateTime), CAST(0x07F0A8FDDE660000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (59, N'admin', CAST(0x0000A73A010B27AE AS DateTime), CAST(0x0740A0CEE1870000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (60, N'admin', CAST(0x0000A76800DC4C7D AS DateTime), CAST(0x07B0B1BC0C700000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (61, N'admin', CAST(0x0000A7680140531D AS DateTime), CAST(0x0760B5F0ECA20000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (62, N'admin', CAST(0x0000A76900E9DB77 AS DateTime), CAST(0x0770C42AF2760000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (65, N'admin', CAST(0x0000A76A0107207B AS DateTime), CAST(0x07F0D74FD5850000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (66, N'admin', CAST(0x0000A76A0172754D AS DateTime), CAST(0x0760DFBE6CBC0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (67, N'admin', CAST(0x0000A76A0179B378 AS DateTime), CAST(0x0750F1D21BC00000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (68, N'admin', CAST(0x0000A76B0147862D AS DateTime), CAST(0x0710DB5E96A60000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (69, N'admin', CAST(0x0000A76C017D6E2A AS DateTime), CAST(0x0770546801C20000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (73, N'admin', CAST(0x0000A76E01563EAB AS DateTime), CAST(0x0750F11F13AE0000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (75, N'admin', CAST(0x0000A76E016AEBCA AS DateTime), CAST(0x0710515897B80000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (76, N'fakim', CAST(0x0000A76F0167A740 AS DateTime), CAST(0x07B00FDCEDB60000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (77, N'fakim', CAST(0x0000A76F0169F4B6 AS DateTime), CAST(0x0790AFAC19B80000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (82, N'admin', CAST(0x0000A7A100CF547F AS DateTime), CAST(0x07700B1A74690000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (83, N'admin', CAST(0x0000A7A101377002 AS DateTime), CAST(0x077011C3679E0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (84, N'admin', CAST(0x0000A7A1013B2827 AS DateTime), CAST(0x07701E0C4CA00000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (85, N'admin', CAST(0x0000A7A3015A3E30 AS DateTime), CAST(0x0700AAB61BB00000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (86, N'admin', CAST(0x0000A7A301617ECB AS DateTime), CAST(0x07F01D08CCB30000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (90, N'admin', CAST(0x0000A7A400F31514 AS DateTime), CAST(0x07309958A37B0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (91, N'admin', CAST(0x0000A7A400F35451 AS DateTime), CAST(0x07E0B282C37B0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (92, N'admin', CAST(0x0000A7A400F444E4 AS DateTime), CAST(0x078077DF3D7C0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (93, N'admin', CAST(0x0000A7A50108B05B AS DateTime), CAST(0x07A0F3B2A0860000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (94, N'admin', CAST(0x0000A7A50108D5B1 AS DateTime), CAST(0x07E066B0B3860000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (95, N'admin', CAST(0x0000A7A5010B8F7F AS DateTime), CAST(0x0720639C16880000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (96, N'admin', CAST(0x0000A7A5010F2378 AS DateTime), CAST(0x0700A97FE8890000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (97, N'admin', CAST(0x0000A7A501109BAA AS DateTime), CAST(0x07C06CD7A78A0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (98, N'admin', CAST(0x0000A7A501147638 AS DateTime), CAST(0x070011A19D8C0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (99, N'admin', CAST(0x0000A7A501690304 AS DateTime), CAST(0x07300BBE9EB70000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (100, N'admin', CAST(0x0000A7AA01598DBF AS DateTime), CAST(0x07D092F8C1AF0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (101, N' admin', CAST(0x0000A7AA0159A8A8 AS DateTime), CAST(0x070073A8CFAF0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (104, N'admin', CAST(0x0000A7AA016EB0BA AS DateTime), CAST(0x0770522382BA0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (8, N'admin', CAST(0x0000A60700CD55CC AS DateTime), CAST(0x0730DA5870680000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (9, N'admin', CAST(0x0000A60700D31327 AS DateTime), CAST(0x07D02DB35B6B0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (11, N'admin', CAST(0x0000A60700DBF7F1 AS DateTime), CAST(0x079016BCE16F0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (63, N'admin', CAST(0x0000A76901379A9C AS DateTime), CAST(0x0780186E7D9E0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (12, N'admin', CAST(0x0000A60700E6CD64 AS DateTime), CAST(0x07D02F6264750000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (21, N'admin', CAST(0x0000A608001BD914 AS DateTime), CAST(0x078061092A0E0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (29, N'admin', CAST(0x0000A60A0105FE63 AS DateTime), CAST(0x075052C341850000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (55, N'admin', CAST(0x0000A6F801831478 AS DateTime), CAST(0x07B04009E1C40000 AS Time), N'::1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (56, N'admin', CAST(0x0000A6F80183E24F AS DateTime), CAST(0x07D050BB49C50000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (78, N'admin', CAST(0x0000A770010DAB2B AS DateTime), CAST(0x07A0291A29890000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (64, N'admin', CAST(0x0000A7690137EEE3 AS DateTime), CAST(0x0750C24BA89E0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (70, N'admin', CAST(0x0000A76D010A8873 AS DateTime), CAST(0x07F070D190870000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (71, N'admin', CAST(0x0000A76E01547382 AS DateTime), CAST(0x07C0699529AD0000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (72, N'admin', CAST(0x0000A76E0154D019 AS DateTime), CAST(0x07E061AD58AD0000 AS Time), N'192.168.0.64', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (79, N'admin', CAST(0x0000A7700144D7A2 AS DateTime), CAST(0x07703D2D39A50000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (80, N'admin', CAST(0x0000A770014A809A AS DateTime), CAST(0x07C0FE281AA80000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (81, N'admin', CAST(0x0000A770017A6BFA AS DateTime), CAST(0x07C072AB79C00000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (88, N'fakim', CAST(0x0000A7A400EDA51D AS DateTime), CAST(0x07102D5BDF780000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (89, N'admin', CAST(0x0000A7A400EDF100 AS DateTime), CAST(0x075028F405790000 AS Time), N'127.0.0.1', 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (74, N'admin', CAST(0x0000A76E0156D1ED AS DateTime), CAST(0x071023065EAE0000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (87, N'admin', CAST(0x0000A7A400E9C148 AS DateTime), CAST(0x07007FD9E4760000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (102, N'new34', CAST(0x0000A7AA016039CD AS DateTime), CAST(0x071087BB26B30000 AS Time), N'127.0.0.1', 1)
INSERT [dbo].[tbl_hit_counter] ([ID], [userID], [datetime], [timelog], [IPaddress], [count]) VALUES (103, N'admin', CAST(0x0000A7AA01605084 AS DateTime), CAST(0x0780234932B30000 AS Time), N'127.0.0.1', 1)
SET IDENTITY_INSERT [dbo].[tbl_hit_counter] OFF
/****** Object:  Table [dbo].[tbl_empSalary]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_empSalary](
	[ID] [bigint] IDENTITY(1101,1) NOT NULL,
	[Empid] [varchar](50) NULL,
	[saltype] [varchar](150) NULL,
	[salaryamount] [decimal](18, 2) NULL,
	[saldate] [varchar](50) NULL,
	[salmonth] [varchar](50) NULL,
	[salyr] [varchar](50) NULL,
	[note] [varchar](450) NULL,
	[logdate] [datetime] NULL,
	[paidby] [varchar](50) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_empSalary] ON
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1101, N'1', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-04-04', N'April     ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1102, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-05-04', N'May       ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1103, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-06-04', N'June      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1104, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-07-04', N'July      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1105, N'37', N'Festival Bonus     ', CAST(1000.00 AS Decimal(18, 2)), N'2017-07-04', N'July      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1106, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-08-04', N'August', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1107, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-09-04', N'September', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1108, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-10-04', N'October', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1109, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-11-04', N'November', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1110, N'37', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-12-04', N'December', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1111, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-05-04', N'May       ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1112, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-06-04', N'June      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1113, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-07-04', N'July      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1114, N'22', N'Festival Bonus     ', CAST(1000.00 AS Decimal(18, 2)), N'2017-07-04', N'July      ', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1115, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-08-04', N'August', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1116, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-09-04', N'September', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1117, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-10-04', N'October', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1118, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-11-04', N'November', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1119, N'22', N'Monthly   ', CAST(2000.00 AS Decimal(18, 2)), N'2017-12-04', N'December', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1120, N'37', N'Festival Bonus     ', CAST(15000.00 AS Decimal(18, 2)), N'2017-12-04', N'December', N'2017      ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1121, N'37', N'Monthly   ', CAST(2200.00 AS Decimal(18, 2)), N'2018-01-04', N'January', N'2018     ', NULL, CAST(0x0000A7690173AFC8 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1122, N'1', N'Monthly', CAST(2000.00 AS Decimal(18, 2)), N'2017-05-10', N'May', N'2017', NULL, CAST(0x0000A76A01205D72 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1123, N'22', N'Vacational', CAST(1500.00 AS Decimal(18, 2)), N'2017-12-24', N'December', N'2017', NULL, CAST(0x0000A76A0120E2A4 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1124, N'1', N'Advance', CAST(2500.00 AS Decimal(18, 2)), N'2017-05-11', N'June', N'2017', N'Advance for june 2017', CAST(0x0000A76A0148B738 AS DateTime), N'admin', 1)
INSERT [dbo].[tbl_empSalary] ([ID], [Empid], [saltype], [salaryamount], [saldate], [salmonth], [salyr], [note], [logdate], [paidby], [status]) VALUES (1125, N'22', N'Advance', CAST(2500.00 AS Decimal(18, 2)), N'2017-05-12', N'January', N'2018', N'advance', CAST(0x0000A76A014B1A30 AS DateTime), N'admin', 1)
SET IDENTITY_INSERT [dbo].[tbl_empSalary] OFF
/****** Object:  Table [dbo].[tbl_email]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_email](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[mailFrom] [varchar](150) NULL,
	[mailTo] [varchar](150) NULL,
	[Subject] [varchar](350) NULL,
	[mailbody] [text] NULL,
	[maildate] [datetime] NULL,
	[drafts] [int] NULL,
	[trash] [int] NULL,
	[status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Unread 2 = Read 3 = Draft 4 = trash/Delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_email', @level2type=N'COLUMN',@level2name=N'status'
GO
SET IDENTITY_INSERT [dbo].[tbl_email] ON
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (1, N'jeo', N'leo', N'hell', N'helloo world ...', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (2, N'jeo', N'leo', N'Welcome', N'I''m attempting to have an onclick event added to a row once the data is bound to a gridview webcontrol. The code below is not adding any attributes ', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (3, N'jeo', N'leo', N'everyone', N'4444 helloo world ...', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (4, N'leo', N'jeo', N'Welcome', N'3 . I''m attempting to have an onclick event added to a row once the data is bound to a gridview webcontrol. The code below is not adding any attributes ', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (5, N'jeo', N'leo', N'Welcome', N'4 .I''m attempting to have an onclick event added to a row once the data is bound to a gridview webcontrol. The code below is not adding any attributes ', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tbl_email] ([ID], [mailFrom], [mailTo], [Subject], [mailbody], [maildate], [drafts], [trash], [status]) VALUES (6, N'leo', N'leo', N'Welcome', N'7 I''m attempting to have an onclick event added to a row once the data is bound to a gridview webcontrol. The code below is not adding any attributes ', CAST(0x0000A3EB018058CA AS DateTime), NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[tbl_email] OFF
/****** Object:  Table [dbo].[tbl_Customer]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Customer](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustName] [varchar](250) NULL,
	[CustPhone] [varchar](150) NULL,
	[CustEmail] [varchar](150) NULL,
	[CustAddress] [varchar](450) NULL,
	[CustType] [varchar](150) NULL,
	[DiscountRate] [decimal](18, 2) NULL,
	[Status] [int] NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](250) NULL,
	[LastUpdate] [datetime] NULL,
	[LastUpdateBy] [varchar](150) NULL,
	[CustID] [varchar](150) NOT NULL,
	[Password] [varchar](150) NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[CustID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active
 ,  2 = inactive/Delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Customer', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_Customer] ON
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (5, N'Bill jeo guer', N'9456674', N'sales@walmart.com', N'Bill Villa, Rue67, Montreal, QQ, Canada', N'Power Elite *****', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A6060130537B AS DateTime), N'admin', CAST(0x0000A60700C5AA5F AS DateTime), N'admin', N'bill', N'bill')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (2, N'Halim khan', N'0987654678', N'halimkhan@gmail.com', N'Laval, QC, CA', N'Elite ****', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A60600027A0E AS DateTime), N'admin', CAST(0x0000A606000290D6 AS DateTime), N'admin', N'halim', N'halim')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (4, N'Jaeolal2', N'0987654678', N'Jaeolal@mail.com', N'', N'Power Elite *****', CAST(3.00 AS Decimal(18, 2)), 2, CAST(0x0000A606000372B2 AS DateTime), N'admin', CAST(0x0000A60600037824 AS DateTime), N'admin', N'Jaeolal', N'Jaeolal')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (1, N'Jamal jeo', N'098765789', N'Jamal@mail.com', N'Montreal, QC', N'Power Elite *****', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A60600021434 AS DateTime), N'admin', CAST(0x0000A60600021434 AS DateTime), NULL, N'jamal', N'jamal')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (6, N'Julkifol', N'232344', N'Julkifol@gamail.com', N'Gesdf', N'Ultra ***', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A7690116D98D AS DateTime), N'admin', CAST(0x0000A7690116D98D AS DateTime), NULL, N'Julkifol', N'Julkifol')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (3, N'Michel jeo', N'5199991234', N'michel@gmail.com', N'Windsor, ON, CA', N'Diamond **', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A60600030011 AS DateTime), N'admin', CAST(0x0000A60600030011 AS DateTime), NULL, N'michel', N'michel')
INSERT [dbo].[tbl_Customer] ([ID], [CustName], [CustPhone], [CustEmail], [CustAddress], [CustType], [DiscountRate], [Status], [Logtime], [LogBy], [LastUpdate], [LastUpdateBy], [CustID], [Password]) VALUES (7, N'Tito khan', N'3535', N'tito@ymal.com', N'ertttt', N'Power Elite *****', CAST(3.00 AS Decimal(18, 2)), 1, CAST(0x0000A76901171A60 AS DateTime), N'admin', CAST(0x0000A7A2013941F3 AS DateTime), N'admin', N'tito', N'tio')
SET IDENTITY_INSERT [dbo].[tbl_Customer] OFF
/****** Object:  Table [dbo].[tbl_Category]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Category](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemCategory] [varchar](50) NULL,
	[Status] [int] NOT NULL,
	[Logtime] [datetime] NULL,
	[LogBy] [varchar](50) NULL,
	[Lastupdate] [datetime] NULL,
	[LastupdateBy] [varchar](450) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1= active ;
2= inactive
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Category', @level2type=N'COLUMN',@level2name=N'Status'
GO
SET IDENTITY_INSERT [dbo].[tbl_Category] ON
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (1, N'Alcoholic drinks', 1, CAST(0x0000A4400182BE0B AS DateTime), NULL, CAST(0x0000A4400182BE0B AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (17, N'Desserts, misc.', 1, CAST(0x0000A44001844EB0 AS DateTime), NULL, CAST(0x0000A44001844EB0 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (3, N'Beef', 1, CAST(0x0000A44001839334 AS DateTime), NULL, CAST(0x0000A44001839334 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (4, N'Biscuits', 1, CAST(0x0000A4400183935A AS DateTime), NULL, CAST(0x0000A4400183935A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (5, N'Bread', 1, CAST(0x0000A44001839363 AS DateTime), NULL, CAST(0x0000A44001839363 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (6, N'Butter and margarine', 1, CAST(0x0000A44001839368 AS DateTime), NULL, CAST(0x0000A44001839368 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (7, N'Cakes', 1, CAST(0x0000A44001839371 AS DateTime), NULL, CAST(0x0000A44001839371 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (8, N'Candy', 1, CAST(0x0000A44001839376 AS DateTime), NULL, CAST(0x0000A44001839376 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (9, N'Carbonated drinks', 1, CAST(0x0000A4400183937F AS DateTime), NULL, CAST(0x0000A4400183937F AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (10, N'Cereals', 1, CAST(0x0000A44001839384 AS DateTime), NULL, CAST(0x0000A44001839384 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (11, N'Cheese', 1, CAST(0x0000A4400183938D AS DateTime), NULL, CAST(0x0000A4400183938D AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (12, N'Chicken', 1, CAST(0x0000A44001839396 AS DateTime), NULL, CAST(0x0000A44001839396 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (13, N'Condiments', 1, CAST(0x0000A4400183939B AS DateTime), NULL, CAST(0x0000A4400183939B AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (14, N'Cookies', 1, CAST(0x0000A440018393A9 AS DateTime), NULL, CAST(0x0000A440018393A9 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (15, N'Crackers', 1, CAST(0x0000A440018393AE AS DateTime), NULL, CAST(0x0000A440018393AE AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (16, N'Cream', 1, CAST(0x0000A440018393B7 AS DateTime), NULL, CAST(0x0000A440018393B7 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (18, N'Diet foods', 1, CAST(0x0000A44001844EBE AS DateTime), NULL, CAST(0x0000A44001844EBE AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (19, N'Drink mixes', 1, CAST(0x0000A44001844EC3 AS DateTime), NULL, CAST(0x0000A44001844EC3 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (20, N'Drinks, misc.', 1, CAST(0x0000A44001844F09 AS DateTime), NULL, CAST(0x0000A44001844F09 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (21, N'Duck', 1, CAST(0x0000A44001844F0E AS DateTime), NULL, CAST(0x0000A44001844F0E AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (22, N'Eggs', 1, CAST(0x0000A44001844F17 AS DateTime), NULL, CAST(0x0000A44001844F17 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (23, N'Entrées', 1, CAST(0x0000A44001844F1C AS DateTime), NULL, CAST(0x0000A44001844F1C AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (24, N'Fast foods', 1, CAST(0x0000A44001844F25 AS DateTime), NULL, CAST(0x0000A44001844F25 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (25, N'Fats and oils', 1, CAST(0x0000A44001844F2A AS DateTime), NULL, CAST(0x0000A44001844F2A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (26, N'Fish and seafood', 1, CAST(0x0000A44001844F2F AS DateTime), NULL, CAST(0x0000A44001844F2F AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (27, N'Flour', 1, CAST(0x0000A44001844F38 AS DateTime), NULL, CAST(0x0000A44001844F38 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (28, N'Fowl, misc.', 1, CAST(0x0000A44001844F3D AS DateTime), NULL, CAST(0x0000A44001844F3D AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (29, N'Frozen dinners', 1, CAST(0x0000A44001844F46 AS DateTime), NULL, CAST(0x0000A44001844F46 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (30, N'Fruit', 1, CAST(0x0000A44001844F4B AS DateTime), NULL, CAST(0x0000A44001844F4B AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (31, N'Grains', 1, CAST(0x0000A44001844F54 AS DateTime), NULL, CAST(0x0000A44001844F54 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (32, N'Herbs and spices', 1, CAST(0x0000A44001844F59 AS DateTime), NULL, CAST(0x0000A44001844F59 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (33, N'Hot drinks', 1, CAST(0x0000A44001844F62 AS DateTime), NULL, CAST(0x0000A44001844F62 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (34, N'Ice cream, etc.', 1, CAST(0x0000A44001844F67 AS DateTime), NULL, CAST(0x0000A44001844F67 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (35, N'Icings', 1, CAST(0x0000A44001844F6C AS DateTime), NULL, CAST(0x0000A44001844F6C AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (36, N'Jellies, jams, etc.', 1, CAST(0x0000A44001844F75 AS DateTime), NULL, CAST(0x0000A44001844F75 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (37, N'Juices ', 1, CAST(0x0000A44001844F7A AS DateTime), NULL, CAST(0x0000A44001844F7A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (38, N'Lamb', 1, CAST(0x0000A44001844F83 AS DateTime), NULL, CAST(0x0000A44001844F83 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (39, N'Lunch meats', 1, CAST(0x0000A44001844F91 AS DateTime), NULL, CAST(0x0000A44001844F91 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (40, N'Meats, misc.', 1, CAST(0x0000A44001844F96 AS DateTime), NULL, CAST(0x0000A44001844F96 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (41, N'Medicinal', 1, CAST(0x0000A44001844F9A AS DateTime), NULL, CAST(0x0000A44001844F9A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (42, N'Milk', 1, CAST(0x0000A44001844F9F AS DateTime), NULL, CAST(0x0000A44001844F9F AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (43, N'Milk drinks', 1, CAST(0x0000A44001844FA9 AS DateTime), NULL, CAST(0x0000A44001844FA9 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (44, N'Miscellaneous', 1, CAST(0x0000A44001844FAD AS DateTime), NULL, CAST(0x0000A44001844FAD AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (45, N'Muffins', 1, CAST(0x0000A44001844FB2 AS DateTime), NULL, CAST(0x0000A44001844FB2 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (46, N'Nuts and seeds', 1, CAST(0x0000A44001844FBB AS DateTime), NULL, CAST(0x0000A44001844FBB AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (47, N'Pancakes, etc.', 1, CAST(0x0000A44001844FC0 AS DateTime), NULL, CAST(0x0000A44001844FC0 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (48, N'Pasta', 1, CAST(0x0000A44001844FCE AS DateTime), NULL, CAST(0x0000A44001844FCE AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (49, N'Pastries', 1, CAST(0x0000A44001844FD3 AS DateTime), NULL, CAST(0x0000A44001844FD3 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (50, N'Pies', 1, CAST(0x0000A44001844FD9 AS DateTime), NULL, CAST(0x0000A44001844FD9 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (51, N'Pizza', 1, CAST(0x0000A44001844FDD AS DateTime), NULL, CAST(0x0000A44001844FDD AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (52, N'Pork', 1, CAST(0x0000A44001844FE7 AS DateTime), NULL, CAST(0x0000A44001844FE7 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (53, N'Puddings', 1, CAST(0x0000A44001844FEB AS DateTime), NULL, CAST(0x0000A44001844FEB AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (54, N'Rice and rice dishes', 1, CAST(0x0000A44001844FF5 AS DateTime), NULL, CAST(0x0000A44001844FF5 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (55, N'Rolls and buns', 1, CAST(0x0000A44001844FFA AS DateTime), NULL, CAST(0x0000A44001844FFA AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (56, N'Salad dressings', 1, CAST(0x0000A44001845003 AS DateTime), NULL, CAST(0x0000A44001845003 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (57, N'Salads', 1, CAST(0x0000A44001845008 AS DateTime), NULL, CAST(0x0000A44001845008 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (58, N'Sandwiches', 1, CAST(0x0000A44001845011 AS DateTime), NULL, CAST(0x0000A44001845011 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (59, N'Sauces and gravies', 1, CAST(0x0000A4400184501A AS DateTime), NULL, CAST(0x0000A4400184501A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (60, N'Sausage', 1, CAST(0x0000A4400184501F AS DateTime), NULL, CAST(0x0000A4400184501F AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (61, N'Snack foods', 1, CAST(0x0000A44001845024 AS DateTime), NULL, CAST(0x0000A44001845024 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (62, N'Soufflés', 1, CAST(0x0000A4400184502D AS DateTime), NULL, CAST(0x0000A4400184502D AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (63, N'Soups', 1, CAST(0x0000A44001845032 AS DateTime), NULL, CAST(0x0000A44001845032 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (64, N'Spreads, misc.', 1, CAST(0x0000A4400184503B AS DateTime), NULL, CAST(0x0000A4400184503B AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (65, N'Stuffings', 1, CAST(0x0000A44001845040 AS DateTime), NULL, CAST(0x0000A44001845040 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (66, N'Sugar and syrups', 1, CAST(0x0000A44001845049 AS DateTime), NULL, CAST(0x0000A44001845049 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (67, N'Toppings', 1, CAST(0x0000A4400184504E AS DateTime), NULL, CAST(0x0000A4400184504E AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (68, N'Turkey', 1, CAST(0x0000A44001845053 AS DateTime), NULL, CAST(0x0000A44001845053 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (69, N'Veal', 1, CAST(0x0000A4400184505C AS DateTime), NULL, CAST(0x0000A4400184505C AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (70, N'Vegetables', 1, CAST(0x0000A44001845061 AS DateTime), NULL, CAST(0x0000A44001845061 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (71, N'Yogurt', 1, CAST(0x0000A4400184506A AS DateTime), NULL, CAST(0x0000A4400184506A AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (72, N'Drinks', 1, CAST(0x0000A4410172A6F2 AS DateTime), N'admin', CAST(0x0000A4410172A6F2 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (73, N'Cloth', 1, CAST(0x0000A441017AD4D9 AS DateTime), N'admin', CAST(0x0000A441017AD4D9 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (74, N'Mobile', 1, CAST(0x0000A44201605E09 AS DateTime), N'admin', CAST(0x0000A44201605E09 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (75, N'Computer', 1, CAST(0x0000A442016E097F AS DateTime), N'admin', CAST(0x0000A442016E097F AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (76, N'ss', 2, CAST(0x0000A4AE0171E16D AS DateTime), N'admin', CAST(0x0000A4AF009C5F9B AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (77, N'Software', 1, CAST(0x0000A4AF009C71A6 AS DateTime), N'admin', CAST(0x0000A4AF009C71A6 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (78, N'Metal', 1, CAST(0x0000A4FF00DED20C AS DateTime), N'admin', CAST(0x0000A4FF00DED20C AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (79, N'sdfs', 2, CAST(0x0000A59100232C59 AS DateTime), N'admin', CAST(0x0000A59100232F97 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (80, N'dfdfdf', 2, CAST(0x0000A5DB0134EFEE AS DateTime), N'admin', CAST(0x0000A5DB0134FB84 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (81, N'dfdfdf', 2, CAST(0x0000A5DB0134F5A6 AS DateTime), N'admin', CAST(0x0000A5DB0134FD27 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (82, N'dfdfdf', 2, CAST(0x0000A5DB0134F7E0 AS DateTime), N'admin', CAST(0x0000A5DB0134FE32 AS DateTime), NULL)
INSERT [dbo].[tbl_Category] ([ID], [ItemCategory], [Status], [Logtime], [LogBy], [Lastupdate], [LastupdateBy]) VALUES (83, N'dfdf', 2, CAST(0x0000A612011475CE AS DateTime), N'admin', CAST(0x0000A61201147991 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tbl_Category] OFF
/****** Object:  Table [dbo].[tbl_accesslog]    Script Date: 07/08/2017 23:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_accesslog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](250) NULL,
	[Operations] [varchar](450) NULL,
	[datetime] [datetime] NULL,
	[timelog] [time](7) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_accesslog] ON
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (1, N'admin', N'POS Payment |  Invoice No: 1001', CAST(0x0000A606000D72D7 AS DateTime), CAST(0x0720701ED7060000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (2, N'admin', N' Added New Item : New Test', CAST(0x0000A60601866CE3 AS DateTime), CAST(0x075002A294C60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (3, N'admin', N' Added New Item : burger king', CAST(0x0000A60601884209 AS DateTime), CAST(0x07900B4183C70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (4, N'admin', N' Added New Item : burger king', CAST(0x0000A60601884EFE AS DateTime), CAST(0x07900ED889C70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (7, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 1245655', CAST(0x0000A6070001A5FA AS DateTime), CAST(0x0770EAA0D6000000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (10, N'admin', N'Update Item info : Diaper | Item Code  : 3000200', CAST(0x0000A6070007E815 AS DateTime), CAST(0x0710668005040000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (11, N'admin', N'Update Item info : iPhone-Compass-Silver | Item Code  : 1200', CAST(0x0000A6070008140C AS DateTime), CAST(0x07D0E2DC1B040000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (12, N'admin', N'Update Item info : iPhone-Compass-Silver | Item Code  : 1200', CAST(0x0000A60700094A4E AS DateTime), CAST(0x07F023ABB9040000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (13, N'admin', N'Purchased Payment |  Invoice No: 99101', CAST(0x0000A6070009AC15 AS DateTime), CAST(0x07B07666EB040000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (18, N'admin', N'order Payment |  Invoice No: 1005 | Total amount 7.86', CAST(0x0000A60700C54F6B AS DateTime), CAST(0x0750996F5B640000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (19, N'admin', N'order_Due Payment | Invoice No 1005', CAST(0x0000A60700C5D1CE AS DateTime), CAST(0x0740FCC09D640000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (20, N'admin', N'Change Status : Delivered | Invoice No: 1005', CAST(0x0000A60700C5F218 AS DateTime), CAST(0x07503D2DAE640000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (21, N'admin', N'order Payment |  Invoice No: 1006 | Total amount 4.20', CAST(0x0000A60700E2B87E AS DateTime), CAST(0x07F0CDEB50730000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (22, N'admin', N'sales Payment |  Invoice No: 1007 | Total amount 7.86', CAST(0x0000A60700E6F985 AS DateTime), CAST(0x076030D47A750000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (23, N'admin', N'Update Item info : Rice | Item Code  : 3000200', CAST(0x0000A60700EAF1FA AS DateTime), CAST(0x07706AD37F770000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (24, N'5', N'order Payment |  Invoice No: 1008 | Total amount 2.10', CAST(0x0000A60700EDF869 AS DateTime), CAST(0x07901FB909790000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (25, N'admin', N'Change Status : Approved | Invoice No: 1008', CAST(0x0000A60700EF1C54 AS DateTime), CAST(0x078039339E790000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (26, N'admin', N'Change Status : Delivered | Invoice No: 1008', CAST(0x0000A60700F16E42 AS DateTime), CAST(0x07C0D149CC7A0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (27, N'admin', N'POS Payment |  Invoice No: 1009', CAST(0x0000A6070187B5E5 AS DateTime), CAST(0x076084F93BC70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (30, N'admin', N'POS Payment |  Invoice No: 1011', CAST(0x0000A6090121191B AS DateTime), CAST(0x075053FA0A930000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (31, N'admin', N'POS Payment |  Invoice No: 1012', CAST(0x0000A609012F15EE AS DateTime), CAST(0x07906047289A0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (33, N'admin', N'Purchased Payment |  Invoice No: 99104', CAST(0x0000A60901405FC9 AS DateTime), CAST(0x0730A462F3A20000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (34, N'admin', N'Purchased Payment |  Invoice No: 99105', CAST(0x0000A60901417177 AS DateTime), CAST(0x077004967EA30000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (35, N'admin', N'Purchased Payment |  Invoice No: 99106', CAST(0x0000A6090141C568 AS DateTime), CAST(0x0750E347A9A30000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (42, N'admin', N'Update Item info : T-shirt | Item Code  : 1235566', CAST(0x0000A611018460E0 AS DateTime), CAST(0x0700441B8AC50000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (43, N'admin', N'Update Item info : Laser-Mouse | Item Code  : 45111', CAST(0x0000A6120101EA44 AS DateTime), CAST(0x07D0F3B12E830000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (45, N'admin', N'Update Item info : single-burger-combo | Item Code  : 89099807', CAST(0x0000A61201144676 AS DateTime), CAST(0x07F0A656858C0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (46, N'admin', N'Purchased Payment |  Invoice No: 99107', CAST(0x0000A6120119EB10 AS DateTime), CAST(0x0750B619648F0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (47, N'admin', N'order Payment |  Invoice No: 1019 | Total amount 31.18', CAST(0x0000A612011AE297 AS DateTime), CAST(0x07D0EFFFE18F0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (48, N'admin', N'order_Due Payment | Invoice No 1019', CAST(0x0000A612011B216A AS DateTime), CAST(0x07702CF401900000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (49, N'admin', N'Change Status : Approved | Invoice No: 1019', CAST(0x0000A612011B42B6 AS DateTime), CAST(0x0740A7E312900000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (50, N'admin', N'sales Payment |  Invoice No: 1020 | Total amount 3.00', CAST(0x0000A612011BF2E4 AS DateTime), CAST(0x0780B77F6C900000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (51, N'admin', N'POS Return Payment |  Invoice No: 4001', CAST(0x0000A612011D4212 AS DateTime), CAST(0x07C0E7FA16910000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (52, N'admin', N'POS Payment |  Invoice No: 1021', CAST(0x0000A61E013769F1 AS DateTime), CAST(0x07E01EAD649E0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (53, N'admin', N'POS Payment |  Invoice No: 1022', CAST(0x0000A61E013BF779 AS DateTime), CAST(0x07900D7FB5A00000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (54, N'admin', N'order Payment |  Invoice No: 1023 | Total amount 10.28', CAST(0x0000A61E013C35E2 AS DateTime), CAST(0x0710463DD5A00000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (55, N'admin', N'Update Item info : chicken leg_1lb | Item Code  : 6677', CAST(0x0000A61E013F4A6B AS DateTime), CAST(0x0750394F66A20000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (56, N'admin', N'Update Item info : chicken leg_1lb | Item Code  : 6677', CAST(0x0000A61E013F5072 AS DateTime), CAST(0x07C0FB5F69A20000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (57, N'admin', N' Added New Item : test', CAST(0x0000A69F0110215A AS DateTime), CAST(0x07705EA1698A0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (58, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 909905', CAST(0x0000A6A0010BE274 AS DateTime), CAST(0x078015CE40880000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (59, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 909905', CAST(0x0000A6A00112D553 AS DateTime), CAST(0x07A0EC95C98B0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (60, N'admin', N'Update Item info : single-burger-combo | Item Code  : 89099807', CAST(0x0000A76801438361 AS DateTime), CAST(0x0790581D8CA40000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (61, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 1245655', CAST(0x0000A76801442017 AS DateTime), CAST(0x077050D2DBA40000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (62, N'admin', N' Added New Item : Newimageresize', CAST(0x0000A76801467FAC AS DateTime), CAST(0x07D08EDA10A60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (63, N'admin', N' Deleted   Item | code: 23948998', CAST(0x0000A76801473A61 AS DateTime), CAST(0x079078D16FA60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (64, N'admin', N' Added New Item : newtest', CAST(0x0000A76801475DDF AS DateTime), CAST(0x0770BFDE81A60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (66, N'admin', N'Update Item info : Banana | Item Code  : 909904', CAST(0x0000A76801481166 AS DateTime), CAST(0x0740C12EDDA60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (67, N'admin', N'Update Item info : Chicken | Item Code  : 909903', CAST(0x0000A76801489BF8 AS DateTime), CAST(0x07B0D0A923A70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (68, N'admin', N'Update Item info : chicken leg_1lb | Item Code  : 6677', CAST(0x0000A7680149221A AS DateTime), CAST(0x077006E367A70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (71, N'admin', N'Update Item info : Love Candy | Item Code  : 6765', CAST(0x0000A7680149E688 AS DateTime), CAST(0x07B0CECBCBA70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (72, N'admin', N'Update Item info : burger king | Item Code  : 89099808', CAST(0x0000A768014AC9E1 AS DateTime), CAST(0x07E0506E3FA80000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (76, N'admin', N'POS Payment |  Invoice No: 1026', CAST(0x0000A76900EB4D19 AS DateTime), CAST(0x07300A2CAE770000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (80, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 909905', CAST(0x0000A76901415214 AS DateTime), CAST(0x0780419F6EA30000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (82, N'admin', N' Deleted   Item | code: 9003', CAST(0x0000A7690146F02F AS DateTime), CAST(0x077085144AA60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (90, N'admin', N'POS Payment |  Invoice No: 1029', CAST(0x0000A76C000BFD4B AS DateTime), CAST(0x07A0851F19060000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (91, N'admin', N'order_Due Payment | Invoice No 1006', CAST(0x0000A76D00029153 AS DateTime), CAST(0x075024554E010000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (92, N'admin', N'sales_Due Payment | Invoice No 1027', CAST(0x0000A76D0008D68C AS DateTime), CAST(0x073082CA7E040000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (94, N'admin', N'POS Payment |  Invoice No: 1031', CAST(0x0000A770011112A8 AS DateTime), CAST(0x0700335DE48A0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (95, N'admin', N'order Payment |  Invoice No: 1032 | Total amount 51.45', CAST(0x0000A7A2016FFCE7 AS DateTime), CAST(0x077046172BBB0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (96, N'admin', N'sales Payment |  Invoice No: 1033 | Total amount 57.73', CAST(0x0000A7A2018461A4 AS DateTime), CAST(0x07D0E77E8AC50000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (97, N'admin', N'POS Payment |  Invoice No: 1034', CAST(0x0000A7A3000C1443 AS DateTime), CAST(0x07A03ECE24060000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (5, N'admin', N' Added New Item : single-burger-combo', CAST(0x0000A60601888347 AS DateTime), CAST(0x07201270A4C70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (6, N'admin', N' Deleted   Item | code: 1234442', CAST(0x0000A6060189976E AS DateTime), CAST(0x074068E530C80000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (8, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 1245655', CAST(0x0000A60700023F0B AS DateTime), CAST(0x07F0557B24010000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (75, N'admin', N'sales Payment |  Invoice No: 1025 | Total amount 6.28', CAST(0x0000A76900EAA9BF AS DateTime), CAST(0x07205B165B770000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (98, N'admin', N'POS Return Payment |  Invoice No: 4002', CAST(0x0000A7A300101980 AS DateTime), CAST(0x0750984D30080000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (9, N'admin', N'Update Item info : Diaper | Item Code  : 3000200', CAST(0x0000A6070005EA08 AS DateTime), CAST(0x07506F1302030000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (14, N'admin', N'order Payment |  Invoice No: 1002 | Total amount 9.38', CAST(0x0000A607001006F2 AS DateTime), CAST(0x07C0ABDD26080000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (15, N'admin', N'sales Payment |  Invoice No: 1003 | Total amount 13.91', CAST(0x0000A60700116C25 AS DateTime), CAST(0x0760FC8BDC080000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (16, N'admin', N'Purchased Payment |  Invoice No: 99102', CAST(0x0000A6070011A7D7 AS DateTime), CAST(0x0720D0E8FA080000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (17, N'admin', N'POS Payment |  Invoice No: 1004', CAST(0x0000A60700122F1A AS DateTime), CAST(0x0710F7B43F090000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (28, N'admin', N'POS Payment |  Invoice No: 1010', CAST(0x0000A60800C6B8DF AS DateTime), CAST(0x07D0CE4713650000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (39, N'admin', N'POS Payment |  Invoice No: 1015', CAST(0x0000A60A012D40AF AS DateTime), CAST(0x0720AD9B39990000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (44, N'admin', N'POS Payment |  Invoice No: 1018', CAST(0x0000A61201134D75 AS DateTime), CAST(0x07B02AB0068C0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (65, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 909905', CAST(0x0000A7680147BB0F AS DateTime), CAST(0x07708944B1A60000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (69, N'admin', N'Update Item info : Rice | Item Code  : 3000200', CAST(0x0000A76801495594 AS DateTime), CAST(0x07D0991182A70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (70, N'admin', N'Update Item info : burger king | Item Code  : 89099808', CAST(0x0000A76801497647 AS DateTime), CAST(0x07D069B392A70000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (73, N'admin', N'POS Payment |  Invoice No: 1024', CAST(0x0000A76900025761 AS DateTime), CAST(0x07E020DC30010000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (77, N'admin', N'sales Payment |  Invoice No: 1027 | Total amount 2.08', CAST(0x0000A769012968C1 AS DateTime), CAST(0x07E0D42745970000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (79, N'admin', N'Update Item info : Banana | Item Code  : 909904', CAST(0x0000A76901390640 AS DateTime), CAST(0x07B02F63369F0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (29, N'admin', N'Update Item info : single-burger-combo | Item Code  : 89099807', CAST(0x0000A60800C70DE4 AS DateTime), CAST(0x07300F863E650000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (32, N'admin', N'Purchased Payment |  Invoice No: 99103', CAST(0x0000A609013B4D14 AS DateTime), CAST(0x07D029D45EA00000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (36, N'5', N'order Payment |  Invoice No: 1013 | Total amount 2.08', CAST(0x0000A6090152ED28 AS DateTime), CAST(0x0700630A63AC0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (37, N'admin', N'Change Status : Pending | Invoice No: 1013', CAST(0x0000A60901532AE6 AS DateTime), CAST(0x07F0C87182AC0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (38, N'admin', N'POS Payment |  Invoice No: 1014', CAST(0x0000A60A012B8DC1 AS DateTime), CAST(0x07E034645C980000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (40, N'admin', N'POS Payment |  Invoice No: 1016', CAST(0x0000A60A01629662 AS DateTime), CAST(0x0770253D5AB40000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (41, N'admin', N'order Payment |  Invoice No: 1017 | Total amount 9.07', CAST(0x0000A60A0162E08B AS DateTime), CAST(0x07A05DF57FB40000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (74, N'admin', N'Update Item info : Advance POS | Item Code  : 112233', CAST(0x0000A769000C94D3 AS DateTime), CAST(0x0750343266060000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (81, N'admin', N'Update Item info : New Test | Item Code  : 909902', CAST(0x0000A76901415F8E AS DateTime), CAST(0x0740047A75A30000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (78, N'admin', N'sales Payment |  Invoice No: 1028 | Total amount 1.32', CAST(0x0000A76901298E23 AS DateTime), CAST(0x07A0622B58970000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (87, N'admin', N'Update Item info : newtest | Item Code  : 85697569', CAST(0x0000A76B014D218D AS DateTime), CAST(0x0760977070A90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (88, N'admin', N'Update Item info : CocaCola-1litter | Item Code  : 45681', CAST(0x0000A76B014D889A AS DateTime), CAST(0x0770B6DAA4A90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (89, N'admin', N'Update Item info : single-burger-Lg | Item Code  : 909905', CAST(0x0000A76B014D9C89 AS DateTime), CAST(0x07903BFEAEA90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (93, N'admin', N'POS Payment |  Invoice No: 1030', CAST(0x0000A76E0154F491 AS DateTime), CAST(0x07E0EA396BAD0000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (83, N'admin', N' Added New Item : newtest', CAST(0x0000A76B014BC1FB AS DateTime), CAST(0x07F0279FBDA80000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (84, N'admin', N' Added New Item : Newimageresize', CAST(0x0000A76B014CFB4C AS DateTime), CAST(0x0730AAFB5CA90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (85, N'admin', N' Deleted   Item | code: 4345465', CAST(0x0000A76B014D0EA0 AS DateTime), CAST(0x07004CD066A90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (86, N'admin', N' Deleted   Item | code: 43986593444', CAST(0x0000A76B014D12A1 AS DateTime), CAST(0x0790B0D968A90000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (100, N'fakim', N'order Payment |  Invoice No: 1036 | Total amount 4.20', CAST(0x0000A7A400EDD48F AS DateTime), CAST(0x0770D97CF7780000 AS Time))
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (101, N'admin', N'Update Item info : Advance POS | Item Code  : 112233', CAST(0x0000A7A701869FB0 AS DateTime), CAST(0x07B0B178AEC60000 AS Time))
GO
print 'Processed 100 total records'
INSERT [dbo].[tbl_accesslog] ([ID], [UserID], [Operations], [datetime], [timelog]) VALUES (99, N'5', N'order Payment |  Invoice No: 1035 | Total amount 51.45', CAST(0x0000A7A40007E569 AS DateTime), CAST(0x07907F2404040000 AS Time))
SET IDENTITY_INSERT [dbo].[tbl_accesslog] OFF
/****** Object:  StoredProcedure [dbo].[SP_A_Language_chnage]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SP_A_Language_chnage]		  
AS
 	
					 -- do in existing table 
			--Alter TABLE test
			--(
			--		col1 VARCHAR(100) COLLATE Latin1_General_100_CI_AI,
			--		col2 VARCHAR(100) COLLATE Arabic_CI_AI_KS_WS,  -- this one for arabic language
			--		col3 NVARCHAR(100);
			--)
			--INSERT INTO  test VALUES(N'لا أتكلم العربية',N'لا أتكلم العربية',N'لا أتكلم العربية')
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_A_Note]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_A_Note] 	 
	 	     
AS
 
		--	Create Userpanel- >> Customer Search
		--                  -->> Items Search 
			
			
			
		---- Some Quote from Bill Gates	
		---			Your most unhappy customers are your greatest source of learning. -----  Bill Gates
		--			If you can't make it good, at least make it look good.
		----	“If you are born poor its not your mistake, But if you die poor its your mistake.”
GO
/****** Object:  StoredProcedure [dbo].[SP_mail_DataBind_mailBody]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_mail_DataBind_mailBody]	 
	@id	   int 
AS
BEGIN
		select mailbody ,Subject , mailFrom ,mailTo ,maildate from  tbl_email	 where [ID] = @id					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_mail_DataBind_index]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_mail_DataBind_index]
		 	     
AS
BEGIN
		select ID , [Subject]  from dbo.tbl_email order by ID desc	 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Load_ChartSales_ValueXY]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Load_ChartSales_ValueXY]	   
	 @Date1 varchar(111),
	 @Date2 varchar(111),
	 @ReportType int   -- 1 =  for Last 30 days 2 = for Custom report 	     
AS
BEGIN
		SET NOCOUNT ON;
		if(@ReportType = 1)
		Begin
			SELECT  CONVERT(date,[Logtime],113) as [DATE]  ,SUM([totalpayable]) as [Total]
			FROM tbl_SalesPayment		
		    WHERE CONVERT(date,[Logtime],113) between   GETDATE() - 30    and  GETDATE()
			group by CONVERT(date,[Logtime],113)		
		END
		ELSE
		BEGIN
			SELECT CONVERT(date,[Logtime],113) as [DATE]  ,SUM([totalpayable]) as [Total]
			FROM tbl_SalesPayment		
		    WHERE CONVERT(date,[Logtime],113) between @Date1 and @Date2
			group by CONVERT(date,[Logtime],113)
		END
			
			 						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Load_ChartProfit_ValueXY]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Load_ChartProfit_ValueXY]	   
	 @Date1 varchar(111),
	 @Date2 varchar(111)	,
	 @ReportType int   -- 1 =  for Last 30 days 2 = for Custom report 	     
AS
BEGIN
		SET NOCOUNT ON;
			if(@ReportType = 1)
		Begin
			SELECT   CONVERT(date,[Logtime],113) as [DATE] ,SUM(total) as [Total] ,SUM([Profit]) as Profit
			FROM tbl_sales		
		    WHERE CONVERT(date,[Logtime],113) between   GETDATE() - 30    and  GETDATE()
			group by CONVERT(date,[Logtime],113)
			
		END
		ELSE
		BEGIN
			SELECT   CONVERT(date,[Logtime],113) as [DATE] ,SUM(total) as [Total] ,SUM([Profit]) as Profit
			FROM tbl_sales		
		    WHERE CONVERT(date,[Logtime],113) between @Date1 and @Date2
			group by CONVERT(date,[Logtime],113)
		END
				
	 
			 						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Update_Supplier]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_INV_Update_Supplier]
		 @id			bigint
		,@Name			varchar(250)
		,@Phone			varchar(150)
		,@Email			varchar(150)
		,@Address		varchar(450)
		,@Type			varchar(150)		 
		,@LastUpdateBy	varchar(150)
		,@CompanyName	varchar(150)
		,@City			varchar(150)
	
AS
BEGIN
	Update   tbl_supplier set 
	Name  = @Name, Phone  = @Phone ,  Email = @Email , [Address] = @Address , City = @City ,
	[Type] = @Type  , CompanyName = @CompanyName , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(), [Status]  = 1 
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Update_changestatus]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Update_changestatus]        	 
	   @date 				varchar(50)
      ,@orderstatus			varchar(450)
      ,@payment_status		varchar(150)      
      ,@deliveryaddress		varchar(350) 
      ,@comment				varchar(490)
      ,@InvoiceNo			BIGINT
     -- ,@CustID				varchar(50)
     -- ,@CustName			varchar(450)
      ,@ServedBy			varchar(450)
       
AS
BEGIN		
		SET NOCOUNT ON; 	
		 
			Update  tbl_SalesPayment set 
			order_status = @orderstatus , payment_status = @payment_status , ordedate = @date
			,shippingaddress = @deliveryaddress , comment = @comment ,  ServedBy = 'ChangeStatus_' + @ServedBy
			Where  ID =  @InvoiceNo
			
			--- Tracking Log  
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'Change Status : ' + @orderstatus + ' | Invoice No: ' + CAST(@InvoiceNo as varchar(450))  )
			
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_Supplier]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_Supplier]
		 @Name			varchar(250)
		,@Phone			varchar(150)
		,@Email			varchar(250)
		,@Address		varchar(450)
		,@Type			varchar(150)		 
		,@LogBy			varchar(150)
		,@CompanyName	varchar(250)
		,@City			varchar(250)    
AS
BEGIN	
		Insert into	  tbl_supplier
		([Name] ,[Phone],[Email],[Address],[Type],CompanyName,  City ,[LogBy])
Values	(@Name , @Phone ,@Email ,@Address ,@Type,@CompanyName, @City , @LogBy)
				 
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_ReceivePayment]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_ReceivePayment]  -- used in Newsale and NewOrder form
	   @SalesQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
	  ,@discount		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalpayable	decimal(18,2)
      ,@payType			varchar(50)
      ,@paidAmount		decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)	 
	  ,@date 			varchar(50)
      ,@orderstatus		varchar(450)
      ,@paystatus		varchar(150)
      ,@trxtype			varchar(150) 	 
      ,@deliveryaddress			varchar(350)     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_SalesPayment
			([SalesQty],[Subtotal],[discount], [Vat] ,[totalpayable] ,[payType] , [paidAmount] ,[changeAmount] ,[dueAmount]
			,[comment] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy], 
			[ordedate],[order_status] ,[payment_status] ,  [trxtype], shippingaddress )
			values 
			(@SalesQty, @Subtotal ,@discount ,@Vat ,@totalpayable ,@payType ,@paidAmount ,@changeAmount ,
			@dueAmount,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy 
			,@date   ,@orderstatus ,@paystatus ,@trxtype , @deliveryaddress) 
			
			
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_SalesPayment order by ID desc)	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,@trxtype ,@date  ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )

			--- Tracking Log  Sales and order
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , @trxtype + ' Payment |  Invoice No: ' +  CAST(@spid as varchar(450)) + ' | Total amount ' +   CAST(@totalpayable as varchar(450)) ) 
			-- (@ServedBy , @trxtype + ' Payment |  Invoice No: ' + @spid + ' | Total amount ' + @totalpayable) 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_ReceiveDuePayment]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_ReceiveDuePayment]
	   @payType			varchar(150)
      ,@paidAmount		decimal(18,2)
      ,@ServedBy		varchar(150)	 
	  ,@date 			varchar(50)     
      ,@trxtype			varchar(150)    
      ,@spid			bigint   
      ,@dueAmount		decimal(18,2)
AS
BEGIN		
		SET NOCOUNT ON; 	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,@trxtype ,@date  , @paidAmount ,@payType  ,@ServedBy)
			
			--- Tracking Log   
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			 (@ServedBy , @trxtype + ' Payment | Invoice No ' + CAST(@spid as varchar(450)) )
			 
				 
			-- Update   tbl_SalesPayment table
			Update  tbl_SalesPayment set 
			dueAmount = (@dueAmount - @paidAmount)
			Where  ID =  @spid
			
		
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_PurchasePayment]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad  
		--- Email :			citkar@live.com
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_PurchasePayment]	
	   @total			decimal(18,2)
      ,@method			varchar(150)
      ,@paidAmount	    decimal(18,2)  
      ,@Comment			varchar(450)      
      ,@supplierid		varchar(50)
      ,@supplier		varchar(450)      
      ,@purchaseby		varchar(150)	 
	  ,@puchase_date 	varchar(50)  
	  ,@ShopId 			varchar(150)   
	  ,@itemQty			decimal(18,2)
 
AS
BEGIN		
		SET NOCOUNT ON;
			insert into   tbl_purchase_payment
			(supplierid , supplier , puchase_date , total , payment  , method   , purchaseby , Comment , itemQty  , ShopId)
			 values 
			(@supplierid , @supplier , @puchase_date , @total , @paidAmount  , @method   , @purchaseby , @Comment , @itemQty , @ShopId )
	 		
	 		
	 		
			--- Tracking Log 
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    purchaseCode   as purchaseCode  FROM tbl_purchase_payment order by purchaseCode desc)	
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@purchaseby , 'Purchased Payment |  Invoice No: ' + @spid)
	 		
	 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Insert_PurchaseItems]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_Insert_PurchaseItems]
	   @product_id			varchar(150)
	  ,@ItemName			varchar(450)
	  ,@Qty					decimal(18,2)
	  ,@Price				decimal(18,2) 
	  ,@Total				decimal(18,2)
	  ,@puchase_date	    varchar(50)	
	  ,@supplierid			varchar(50)	
	  ,@InvoiceNoOutPut		varchar(150) output   
 
AS
BEGIN		
			SET NOCOUNT ON;			 
			Declare @purchaseCode bigint = isnull((Select top 1 purchaseCode + 1  from  tbl_purchase_payment  order by purchaseCode desc),99101) 
			
			insert into   tbl_purchase
			( [product_id] , [product_name]   ,[Qty] , [unit_price]    ,[total] , [purchaseCode]  , [supplierid] , puchase_date) 
			values (@product_id, @ItemName ,@Qty ,@Price ,   @Total , @purchaseCode , @supplierid , @puchase_date)				
			
			
			--Item Quantity Increase in tbl_item 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @product_id) + @Qty 
			where ItemCode = @product_id
			
			-- This is output Parameter send to Invoice number for Invoice printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 purchaseCode   + 1  from tbl_purchase_payment   order by purchaseCode  desc), 99101)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Inactive_Supplier]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_INV_Inactive_Supplier]
		 @id				bigint	 	 
		,@LastUpdateBy		varchar(150)
	
AS
BEGIN
	Update    tbl_supplier	 set 
    [Status] = 2 , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE()
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SuppliersEvent]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SuppliersEvent]	-- 59101
	@ID	varchar(50)     
AS
BEGIN			
			Select ID,  Phone , Name		 
			from  tbl_supplier
			where ID = @ID and Status = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Suppliers_name]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Suppliers_name]	 	     
AS
BEGIN	
			Select distinct '' as Name , '' as ID
			union all 
			Select  Name + ' (' + CompanyName + ')' as Name  , ID		 
			from   tbl_supplier
			where Status = 1						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SupplierListSearch]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SupplierListSearch]
				@value varchar(50)		 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID) As [S.N.], ID,   [Name] , [CompanyName] as [Company Name] , [Phone] ,[Email] ,[Address],[City] ,[Type] 
	 	from  tbl_supplier
	 	where    ID like '%' + @value + '%' or [Name] like '%' + @value + '%'  
	 				or [CompanyName] like '%' + @value + '%' 
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SupplierList]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SupplierList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], [ID] ,[Name] ,  [CompanyName] as [Company Name] ,[Phone] ,[Email] ,[Address],[City] ,[Type] 
	 	from  tbl_supplier
	 	where Status = 1
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SoldListToCustomer]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SoldListToCustomer]	 
  	     @custid varchar(50)
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], ID   as [Invoice No] , totalpayable as	Total , CONVERT(varchar(10), Logdate)	  as [Date]	 
	 	from  tbl_SalesPayment
	    where   CustID = 	@custid				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport_Search]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport_Search]	-- 'POS'
	  @value varchar(450)
	 	     
AS
BEGIN
	 	select    ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due, ShopId
	 	from  tbl_SalesPayment	 	
		where CONVERT(varchar(111), ID ) = @value  or   trxtype like '%' + @value + '%' or   ShopId like '%' + @value + '%'
		order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport_DateToDate]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport_DateToDate]	
	  @DateFrom varchar(250)
	 ,@DateTo varchar(250) 	     
AS
BEGIN
	 	select   ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment	 	
		where Logdate between   @DateFrom     and   @DateTo   
		order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesReport]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesReport]	 	     
AS
BEGIN
	 	select top 20  ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer , trxtype [Type]	 ,CONVERT(varchar(11),	Logdate,109)  as [Date] 	,   totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	order by ID desc
	 --	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesPaymentlist]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_SalesPaymentlist] -- show on Sales invoice	 
	@InvoiceNo bigint       
AS
BEGIN
			select ROW_NUMBER() Over (Order by [paydate] desc) As [Payments], [paydate] as [Date]  ,[paidamount] as [Amount]  ,[paytype] + ' | '+ receivedby as [Payment type]
			from    tbl_salespaid
			where  [SP_ID]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Saleslist_search]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Saleslist_search]
	@value varchar(50)	 	     
AS
BEGIN 
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total  , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where ( ID like '%' + @value + '%' and  trxtype = 'sales' ) or (CustName like '%' + @value + '%' and  trxtype = 'sales')						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_SalesList]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_SalesList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where trxtype = 'sales'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Salary]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Salary]	
	@value varchar(50) 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.], [ID], CONVERT(varchar(11),saldate,106) as [Date] ,[saltype] as [Type] ,  salaryamount as [Amount]  ,salmonth as [Month] , salyr as [Year] , paidby 
	 	from  tbl_empSalary
	 	where Empid =  @value and   Status = 1
	 --	order by ID desc
	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseListSupplier]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseListSupplier]	 
  	     @supplierid varchar(50)
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code] , 	Total ,	puchase_date as [Date]	 
	 	from tbl_purchase_payment	
	 	where supplierid = 	@supplierid				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItemList]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItemList]	--  DataBind  in Printpage	
	@InvoiceNo bigint      
AS
BEGIN
	SET NOCOUNT ON;
			Select [product_id] as Item , [product_name] as [Description] ,  Qty , [unit_price] as [Unit Price], Total
			from     [tbl_purchase]
			where  [purchaseCode]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItemCustomerPanel]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItemCustomerPanel]	 	
	@CusID varchar(150)     
AS
BEGIN	 	
	    select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due
	 	from  tbl_SalesPayment
	 	where trxtype = 'sales' and CustID =  @CusID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItem_search]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItem_search]
	@value varchar(50)	 	     
AS
BEGIN
		select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code]	
		, 	Supplier,	puchase_date as [Date] , Total, itemQty as Qty	 
		from tbl_purchase_payment
	 	where purchaseCode like '%' + @value + '%' or Supplier like '%' + @value + '%'  
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseItem]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_PurchaseItem]	 	     
AS
BEGIN
	SET NOCOUNT ON;
	 	select ROW_NUMBER() Over (Order by purchaseCode desc) As [S.N.],  purchaseCode as [Code]	
	 	, 	Supplier,	puchase_date as [Date]  , Total, itemQty as Qty
	 	from tbl_purchase_payment
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_PurchaseInvoice]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

 CREATE    PROCEDURE [dbo].[SP_INV_DataBind_PurchaseInvoice]	  
	      @InvoiceNo bigint 
AS
BEGIN
	SET NOCOUNT ON;
			Select  pp.supplierid ,s.ID , pp.method ,pp.payment, pp.puchase_date, pp.supplier , pp.total, pp.comment
			, s.Name , s.Address ,s.City ,s.CompanyName , s.Email , s.Phone , s.Status 
			from     [tbl_purchase_payment] pp
			left join dbo.tbl_supplier s
			on pp.supplierid = s.ID 
			where  pp.purchaseCode	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_Orderlist_search]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_Orderlist_search]
	@value varchar(50)	 	     
AS
BEGIN
 
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total
	 	from  tbl_SalesPayment
	 	where ( ID like '%' + @value + '%' and  trxtype = 'order' ) or (CustName like '%' + @value + '%' and  trxtype = 'order')
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderList_CustomerPanel]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

Create   PROCEDURE [dbo].[SP_INV_DataBind_OrderList_CustomerPanel]	
	@custID varchar(150) 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due , order_status as [order status]
	 	from  tbl_SalesPayment
	 	where trxtype = 'order' and CustID = @custID 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderList]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_INV_DataBind_OrderList]	 	     
AS
BEGIN
	 	select ROW_NUMBER() Over (Order by ID desc) As [S.N.],  ID as [Code]	, CustName as Customer	 ,	ordedate  as [Date]	,  totalpayable as Total , dueAmount as Due , order_status as [order status]
	 	from  tbl_SalesPayment
	 	where trxtype = 'order'
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderItemList]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_OrderItemList]	--1030 
	@InvoiceNo bigint      
AS
BEGIN
	SET NOCOUNT ON;
			Select [ID] as Item ,   ItemName as [Description] ,  Qty , Price  as [Unit Price], Total
			from   tbl_sales
			where  [SP_ID]	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_OrderInvoice]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

 CREATE    PROCEDURE [dbo].[SP_INV_DataBind_OrderInvoice] -- 1006   
	      @InvoiceNo bigint 
AS 
BEGIN  
			Select  sp.CustID ,c.ID , sp.payType ,(sp.totalpayable - sp.dueAmount) as paidAmount,sp.dueAmount ,  sp.ordedate, sp.CustName , sp.order_status ,sp.payment_status
			, sp.totalpayable,sp.Subtotal, sp.comment	, sp.Vat , sp.vatrate , sp.discount ,sp.dsirate , sp.shippingFee  , sp.shippingaddress
			, c.CustName , c.CustAddress  ,  c.CustEmail , c.CustPhone , c.Status 
			from     tbl_SalesPayment sp
			left join tbl_Customer c
			on sp.CustID = c.ID 
			where  sp.ID	= @InvoiceNo
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Inv_DataBind_Item_Purchase]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_Inv_DataBind_Item_Purchase]	--  DataBind team in Sales Register 	
	@category varchar(150)      
AS
BEGIN
SET NOCOUNT ON;
	IF(@category = 'All')
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total , Photo			  
					from    tbl_Item
					where   Status = 1 	
					order by Qty	
								 
		END
	ELSE
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total ,  Photo			  
					from    tbl_Item
					where ItemCategory = @category and Status = 1 
					order by Qty		
								 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_DashBoardSummary]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_INV_DataBind_DashBoardSummary]
AS
BEGIN
	SET NOCOUNT ON;
		select CAST(COUNT(*)as varchar(50)) + ' Customers ' Total from tbl_Customer
		union all
		select CAST(COUNT(*)as varchar(50)) + ' Suppliers ' Total from tbl_supplier
		Union all
		select CAST(COUNT(*)as varchar(50)) + ' Items ' Total from tbl_Item where Status = 1 

		Union all
		select CAST(SUM(total) as varchar(50)) + ' Purchases ' Total from  tbl_purchase_payment 
		Union all
		select CAST(SUM(totalpayable)as varchar(50)) + ' Orders ' Total from tbl_SalesPayment 	 	where trxtype = 'order' 

		Union all
		select CAST(SUM(totalpayable)as varchar(50)) + ' Sales ' Total from tbl_SalesPayment 	 	where trxtype = 'sales' 

		Union all
		select CAST(SUM(totalpayable) as varchar(50)) + ' POS Transactions ' Total from tbl_SalesPayment 	 	where trxtype = 'POS' 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_AccessLogByUser]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_AccessLogByUser]	 
   @UserID varchar(50)
AS
BEGIN
		SET NOCOUNT ON;
		 SELECT top 5 *  FROM 
		 [tbl_accesslog] 
		 where UserID = @UserID
		 order by 1 desc
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_DataBind_AccessLog]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_INV_DataBind_AccessLog]	 
   
AS
BEGIN
		SET NOCOUNT ON;
		 SELECT top 1000 *  FROM 
		 [tbl_accesslog] order by 1 desc
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INV_Add_Salary]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_INV_Add_Salary] 	 
	   @Empid		Varchar(30) 
      ,@saltype		Varchar(50) 
      ,@salaryamount decimal(18,2)
      ,@saldate		Varchar(50) 
      ,@salmonth	Varchar(210) 
      ,@salyr		Varchar(20) 
      ,@paidby		Varchar(210) 
      ,@note		Varchar(450)
  
          
AS
BEGIN
   insert into   tbl_empSalary	(Empid ,saltype ,salaryamount ,saldate ,salmonth ,salyr ,paidby, note) 
   values 
   (@Empid ,@saltype ,@salaryamount ,@saldate ,@salmonth ,@salyr ,@paidby , @note )
					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Auto_role_insert]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SP_Auto_role_insert]	
	    @userID		varchar(150),
	    @UserName		varchar(150) 
AS
BEGIN		
			--first truncate tbl_UserRole	
		 		-- Access allow
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '1'  FROM  [tbl_Page]
			where PageID    in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 ,144,145)
			
			-- Access Deny
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '0'  FROM  [tbl_Page]
			where PageID  not in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133 ,142 , 144, 145)
		
					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Authentication_Customer]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_User_Authentication_Customer] 
		@LOGINID as varchar(50),
		@PASSWORD as varchar(50)
 
AS
	SELECT * FROM   tbl_Customer
	WHERE   CustID  = @LOGINID AND [PASSWORD] = @password
GO
/****** Object:  StoredProcedure [dbo].[SP_User_Authentication]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_User_Authentication] 
		@LOGINID as varchar(50),
		@PASSWORD as varchar(50)
 
AS
	SELECT * FROM  tbl_user  
	WHERE  UserID  = @LOGINID AND [PASSWORD] = @password
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_UserRole_UpdateAccess]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- Author:			Tuaha Mohammad 
						---- Company Name:   DynamicSoft    
						---- http://www.citputer.com/
						---- http://codecanyon.net/user/dynamicsoft
						----		We all need people who will give us feedback. That's how we improve.

CREATE    PROCEDURE [dbo].[SP_POS_UserRole_UpdateAccess] 
		@UserName varchar(50)
		,@status   int
		,@PageID  bigint
		  
AS
BEGIN
		  update  tbl_UserRole set
		 [Status] = @status
		 where UserName = @UserName and PageID = @PageID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_UserRole_Access]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- Author:			Tuaha Mohammad 
						---- Company Name:   DynamicSoft    
						---- http://www.citputer.com/
						---- http://codecanyon.net/user/dynamicsoft
						----		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_UserRole_Access] 
	@UserName varchar(50)
	,@PageName	varchar(50)
	,@OutPut varchar(5) Output 
AS
BEGIN
-- occur in Authentication
SET NOCOUNT ON;
		If Exists ((Select  PageName  , *  from  tbl_UserRole where UserName = @UserName and PageName = @PageName and status = 1))
		begin
			set @OutPut = 'Y'
		end
		else
		begin
			set @OutPut = 'N'
		end  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_UserProfile]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_UserProfile]
		 @UserID		varchar(150)
		,@Fname			varchar(250)
		,@LName  		varchar(150)
		,@UserPhone  	varchar(150)	
		,@Password		varchar(120)
		,@OldPassword		varchar(120)
		,@LastUpdateBy	varchar(150)
		,@User_Photo	varchar(250)
		,@ResultOutPut  varchar(120) output 
		 	
AS
BEGIN
		if exists (select [Password] from tbl_user Where UserID =  @UserID and [Password] = @OldPassword )
		 BEGIN
			Update  tbl_user  set  Fname = @Fname, LName =@LName , UserPhone =  @UserPhone , 
			User_Photo = @User_Photo , [Password] = @Password 
			,LastUpdateBy = @LastUpdateBy  ,   LastUpdate = GETDATE()
			Where UserID =  @UserID 
			
			--  Password Compare Validation parameter
			select	@ResultOutPut = 1		 
		END	 
		ELSE
		
		BEGIN
			select	@ResultOutPut = 0		
		END
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_User]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_User]
		 @id			bigint
		,@Fname			varchar(250)
		,@LName  		varchar(150)
		,@UserPhone  	varchar(150)
		,@Designation 	varchar(150)	
		,@Department 	varchar(150)	 
		,@UserAddress	varchar(450)	
		,@Email			varchar(150)			 
		,@LastUpdateBy	varchar(150)
		,@Password		varchar(120)
		,@DOB			varchar(10)
		,@ShopID		varchar(50)
		,@Supervisor	varchar(120)	
		,@User_Photo 	varchar(Max)
AS
BEGIN
		Update   tbl_user  set  Fname = @Fname, LName =@LName , UserPhone =  @UserPhone ,
		Designation = @Designation,   [Password] = @Password  ,	
		UserAddress = @UserAddress , Email = @Email, DateofBirth = @DOB, Department =@Department ,
		User_Photo = @User_Photo  ,ShopID = @ShopID , Supervisor = @Supervisor , 
		LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(), [Status]  = 1 
		Where ID =  @ID	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Settings]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_POS_Update_Settings]      
		  
		 @CompanyName		varchar(250)	
		,@CompanyAddress	varchar(450) 
		,@Phone				varchar(450)	 
		,@EmailAddress		varchar(450)	 
		,@WebAddress		varchar(450)
		,@VatRate           decimal(18,2)	 
		,@VATRegistration	varchar(450)	 
		,@Footermsg			nvarchar(250)	 	 
		,@LastUpdateBy		varchar(250)	     
AS
BEGIN
			Update dbo.[tbl_settings] set	  
			CompanyAddress = @CompanyAddress, CompanyName = @CompanyName,  Phone =	@Phone,	
			[EmailAddress] = @EmailAddress , [WebAddress] = @WebAddress ,[VatRate] = @VatRate ,
			 [VATRegistration] = @VATRegistration,
			[Footermsg] =  @Footermsg , LastUpdate =  GETDATE() , LastUpdateBy =	@LastUpdateBy
			where ID = 1				
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Item]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.



CREATE   PROCEDURE [dbo].[SP_POS_Update_Item]
	 @ItemCode varchar(450)
	,@ItemName varchar(450) 
	--,@ItemQty  decimal(18,2)
	,@PurchasePrice decimal(18,2)
	,@RetailPrice  decimal(18,2)	 
	,@Discount decimal(18,2)
	,@ItemCategory  varchar(250)
	,@Lastupdateby   varchar(150)
	,@Itemphoto     varchar(450)
	,@Description     varchar(499)
AS
BEGIN
			Update  tbl_Item	 set  
			 ItemName		= @ItemName 
			,PurchasePrice  = @PurchasePrice
			,RetailPrice	= @RetailPrice 
			,ItemCategory	= @ItemCategory  
			,Discount		= @Discount
			,Lastupdateby	= @Lastupdateby 
			,Photo			= @Itemphoto 
			,[Description]  = @Description
			,Lastupdate		= GETDATE() 
			,Status			= 1
			where ItemCode = @ItemCode 
			
				--- Tracking Log  
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@Lastupdateby, 'Update Item info : ' + @ItemName + ' | Item Code  : ' +  @ItemCode  )
			
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Update_Customer]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_Update_Customer]
		 @id				bigint
		,@CustName			varchar(250)
		,@CustPhone			varchar(150)
		,@CustEmail			varchar(150)
		,@CustAddress		varchar(450)
		,@CustType			varchar(150)
		,@DiscountRate		decimal(18,2)	 
		,@LastUpdateBy		varchar(150)
		,@custpaword 		varchar(150) 
	
AS
BEGIN
	Update   tbl_Customer	 set 
	CustName = @CustName, CustPhone = @CustPhone , CustEmail = @CustEmail ,CustAddress = @CustAddress ,
	CustType = @CustType  , DiscountRate = @DiscountRate  ,  LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE(),[Password] = @custpaword , [Status]  = 1 
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_terminalInfo]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_POS_terminalInfo] --SYS89 -- only for POS printing perpose 
	@ShopID  varchar(500)
AS
BEGIN

	select st.CompanyName  , TL.Location ,  tl.Phone , tl.EmailAddress, tl.VatRate, tl.VATRegistration, usr.ShopID , st.WebAddress , st.Footermsg  
	from tbl_terminalLocation TL
	inner join tbl_user usr
	on
	TL.TerminalID = usr.ShopID
	inner join tbl_settings st
	on 
	TL.CompanyID = st.ID
	where usr.ShopID = @ShopID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_ReturnReport_DateToDate]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_ReturnReport_DateToDate]
	 @DateFrom varchar(250)
	 ,@DateTo varchar(250)
AS
BEGIN 
	
		select distinct ID as Invoice , SalesInvoiceID as [Sales ID], CONVERT(varchar, Logdate,109) as Date, ReturnQty as [Qty] , Subtotal, Vat, totalReturnable as [Total], 
		payType as [Pay Type],  ReturnAmount as [Return Amount], dueAmount as [Due] , note , ShopId , CustContact as [Cust.Contact], ServedBy
		from  tbl_ReturnPayment	  
		where  Logdate between   @DateFrom     and   @DateTo 
		order by  ID desc 
	  
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_SalesDetails]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
								---  Please Don't Forget to Rate Us http://codecanyon.net/downloads
		---		We all need people who will give us feedback. That's how we improve.
		

CREATE   PROCEDURE [dbo].[SP_POS_Report_SalesDetails]
	 @Value varchar(250)
AS
BEGIN
	 Select ItemCode as [Item Code] , ItemName as [Item Name],	Qty,	Price ,	DiscRate as [Disc%],	total as [Total] , Profit	 
	  from tbl_sales 
	 where SP_ID = @Value
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_Monthly]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report_Monthly]
	 
AS
BEGIN
	 --select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type],  paidAmount as [Paid], dueAmount as [Due] , note , ShopId ,  CustContact as [Cust.Contact], ServedBy
	 --from  tbl_SalesPayment  
	 
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID	
		where p.Logdate between   GETDATE() - 30    and  GETDATE() and trxtype = 'POS'
		order by p.ID desc 
	
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report_DateToDate]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/test45
						--- http://codecanyon.net/user/dynamicsoft
						---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report_DateToDate]
	 @DateFrom varchar(250)
	 ,@DateTo varchar(250)
AS
BEGIN
	-- select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type] ,  paidAmount as [Paid], dueAmount as [Due] , note , ShopId ,  CustContact as [Cust.Contact], ServedBy
	-- from  tbl_SalesPayment  
	-- where Logdate between   @DateFrom     and   @DateTo  
	---- WHERE fromDate BETWEEN LIKE '%12/06/2012%' AND LIKE '%16/06/2012%'
	---- where CONVERT(VARCHAR(25), Your_DATE, 126) BETWEEN 'Start_date%' AND 'EndDate%'";
	--order by ID desc 
	
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID	 
		where p.Logdate between   @DateFrom     and   @DateTo  and trxtype = 'POS'
		order by p.ID desc 
	  
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Report]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/test45
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Report]
	 @Value varchar(250)
AS
BEGIN
		--select ID as Invoice , CONVERT(varchar, Logdate,109) as Date,[SalesQty] as [Sold item] ,  Subtotal, Vat, totalpayable as [Total], payType as [Pay Type],  paidAmount as [Paid], dueAmount as [Due] , note , ShopId , CustContact as [Cust.Contact], ServedBy
		--from  tbl_SalesPayment
		--where ID like  @Value  + '%' or ServedBy  like  '%' + @Value  + '%' 
		--or CustName   like    @Value  + '%' or ShopId   like '%' + @Value  + '%'   
		--or payType   like '%' + @Value  + '%'  
		---- select * from tbl_SalesPayment
		
	
		--DECLARE @Profit varchar(30) = (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  = 1018)

		--select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], p.ID as [Invoice],  p.payType  as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		--from  tbl_SalesPayment p
		--left join tbl_sales s 
		--on p.ID = s.SP_ID		 
		--where p.ID like  @Value  + '%' or p.ServedBy  like  '%' + @Value  + '%' 
		--or p.CustName   like    @Value  + '%' or p.ShopId   =  @Value     
		--or p.payType =  @Value  
		
		select distinct p.ID as Invoice , CONVERT(varchar, p.Logdate,109) as Date,p.SalesQty as [Sold item] ,  p.Subtotal, p.Vat, p.totalpayable as [Total], (SELECT       Sum([Profit])  FROM [tbl_sales] where SP_ID  =  s.SP_ID) as [Profit],  p.payType as [Pay Type],  p.paidAmount as [Paid], p.dueAmount as [Due] , p.note , p.ShopId , p.CustContact as [Cust.Contact], p.ServedBy
		from  tbl_SalesPayment p
		left join tbl_sales s 
		on p.ID = s.SP_ID		 
		where (p.ID =  @Value  and trxtype = 'POS') or (p.ServedBy  like  '%' + @Value  + '%'  and trxtype = 'POS')
		or  (p.CustName   like    @Value  + '%'  and trxtype = 'POS') 
		or (p.ShopId   =  @Value   and trxtype = 'POS')   
		or (p.payType =  @Value  and trxtype = 'POS')
		
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_PageDatabind]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---		We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_PageDatabind] 
	  @UserName varchar(150)
AS
BEGIN
	Select p.PageID as [Page ID] , p.Module , p.PageName as [Page Name], 
		case 	ur.Status
			 WHEN 1 THEN 'Allow' 
			 else 'Deny'
		 END as [Access]	 
	from	tbl_Page  p
	left join   
	tbl_UserRole ur on 
	p.PageID = ur.PageID
	where ur.UserName = @UserName 
	and  p.PageID  not in (108,110,111,114,118,119, 144)	 
	order by p.Module

END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_LogStat]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		-- EPOS Link -->  http://codecanyon.net/item/easy-pos-point-of-sale/10141609

CREATE   PROCEDURE [dbo].[SP_POS_LogStat]
       
AS
BEGIN	
		 select ROW_NUMBER() Over (Order by ID desc) As [S.N.], userID as [Log by]	, [datetime], IPaddress	  
		 from tbl_hit_counter	 
		 order by [datetime] desc 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Users]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
						--- Company Name:   DynamicSoft    
						--- http://www.citputer.com/
						--- http://codecanyon.net/user/dynamicsoft
						---	We all need people who will give us feedback. That's how we improve.

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Users]
		 --@UserID		bigint
		 @UserName		varchar(150)	
		,@Fname  		varchar(150)
		,@LName  		varchar(150)
		,@UserPhone 	varchar(150)
		,@UserAddress 	varchar(150)
		,@Supervisor  	varchar(150)		 
		,@Password 		varchar(150)
		,@Designation  	varchar(150)
		,@Department	varchar(150)	 
		,@DateofBirth 	varchar(150)
		,@ShopID		varchar(150)
		,@Logby			varchar(150)
		,@Email			varchar(150)
		,@User_Photo 	varchar(150)      
AS
BEGIN	 

	----Insert new user  
	Insert into	 tbl_user (Fname ,LName ,UserPhone  ,UserAddress ,Supervisor ,UserID  ,[Password], Designation ,Department ,DateofBirth,[ShopID] , Logby, Email,User_Photo)
	values (@Fname ,@LName,@UserPhone ,@UserAddress ,@Supervisor ,@UserName  ,@Password , @Designation, @Department   , @DateofBirth, @ShopID ,@Logby , @Email,@User_Photo  )
 
		Declare @UserID varchar(150) =  (Select top 1 ID  from tbl_user order by 1 desc) 

--- Default Page access for every user - when  user has been created 

		 		-- Access allow
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '1'  FROM  [tbl_Page]
			where PageID    in	(101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 , 144,145)
			 
			-- Access Deny
			Insert into	 tbl_UserRole
			([UserID] , [UserName] , [PageID] , [PageName] , [Status])
			SELECT @userID , @UserName, [PageID] ,PageUrl , '0'  FROM  [tbl_Page]
			where PageID  not in (101,103, 104 , 107,108,110,111,114,118,119,124, 129 , 133, 142 ,144,145)
			
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '101', 'AddItem.aspx' , 1)	
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '102', 'ManageItems.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '103', 'AddCustomer.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '104', 'ManageCustomers.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '105', 'Adduser.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '106', 'ManageUsers.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '107', 'SalesRegister.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '108', 'POS_printPage.aspx' , 1)		
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '109', 'Reports.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '110', 'ProfilePage.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '111', 'Default.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '112', 'Settings.aspx' , 0)
		
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '113', 'Category.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '114', 'TaskList.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '115', 'AddTask.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '116', 'StockItemReport.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '117', 'SalesReports.aspx' , 0)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '118', '404.aspx' , 1)
		
		--Insert into	 tbl_UserRole
		--([UserID] , [UserName] , [PageID] , [PageName] , [Status])
		--Values ( @UserID , @UserName , '119', '500.aspx' , 1)
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Task]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---We all need people who will give us feedback. That's how we improve.
 

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Task]
			@task_title		varchar(250)
			,@task_Description  varchar(550)
			,@taskfrom			varchar(150)
			,@taskto			varchar(150)	     
AS
BEGIN	
		Insert into		  tbl_tasklist 
				([task_title] ,	[task_Description] ,[taskfrom],	[taskto]  ) 
		 values  (@task_title,	@task_Description ,	@taskfrom	,@taskto )
		 
	 				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_SRsalesPayment]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_SRsalesPayment]
	   @SalesQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalpayable	decimal(18,2)
      ,@payType			varchar(50)
      ,@paidAmount		decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)
       	 	     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_SalesPayment
			([SalesQty],[Subtotal],[Vat],[totalpayable] ,[payType] , [paidAmount] ,[changeAmount] ,[dueAmount]
			,[note] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy],trxtype) 
			values 
			(@SalesQty, @Subtotal ,@Vat ,@totalpayable ,@payType ,@paidAmount ,@changeAmount ,@dueAmount
			,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy , 'POS') 
			
			
						
			--- first time single payment info record
			DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_SalesPayment order by ID desc)	
			insert into   tbl_salespaid
			([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
				values
			(@spid ,'POS' ,CONVERT(date, GETDATE(),120) ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )
	
	
		--- Tracking Log  Sales and order
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'POS' + ' Payment |  Invoice No: ' + @spid )
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_SalesItems]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_SalesItems]
	   @Code		varchar(150)
	  ,@ItemName	varchar(450)
	  ,@Qty			decimal(18,2)
	  ,@Price		decimal(18,2)
	  ,@Disc 		decimal(18,2)
	  ,@Total		decimal(18,2)	
	  ,@InvoiceNoOutPut varchar(150) output   
 
AS
BEGIN		
   SET NOCOUNT ON;
			--DECLARE @Profit varchar(30) = (Select     [RetailPrice] - [PurchasePrice] as  [profit] FROM  [tbl_Item] where ItemCode = @Code);
	 
			-- Profit Calculation is  = [RetailPrice] with [Discount Rate] -  PurchasePrice]
			DECLARE @Profit varchar(30) = (Select  CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2))  - [PurchasePrice] as  [profit] FROM  [tbl_Item] where ItemCode = @Code);
			
			insert into  tbl_sales  
			([ItemCode],[ItemName]  ,[Qty] , [Price], [DiscRate] ,[total],[SP_ID],[Profit]) 
			values (@Code, @ItemName ,@Qty ,@Price , @Disc ,@Total , 
			isnull((Select top 1 ID + 1  from tbl_SalesPayment order by ID desc),1001),@Profit)				
			
			
			--Item Quantity Deduction 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @Code) - @Qty 
			where ItemCode = @Code
			
			-- This is output Parameter send to Invoice number for POS printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 ID + 1  from tbl_SalesPayment order by ID desc),1001)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_ReturnPayment]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_ReturnPayment]
	   @ReturnQty		decimal(18,2)
	  ,@Subtotal		decimal(18,2)
      ,@Vat				decimal(18,2)
      ,@totalReturnable	decimal(18,2)
      ,@payType			varchar(50)
      ,@ReturnAmount	decimal(18,2)
      ,@changeAmount	decimal(18,2)
      ,@dueAmount		decimal(18,2)
      ,@note			varchar(450)
      ,@ShopId			varchar(50)
      ,@CustID			varchar(50)
      ,@CustName		varchar(450)
      ,@CustContact		varchar(50)
      ,@ServedBy		varchar(150)
      ,@SalesInvoiceID  bigint
       	 	     
AS
BEGIN		
   SET NOCOUNT ON;
			insert into   tbl_ReturnPayment
			([ReturnQty],[Subtotal],[Vat],[totalReturnable] ,[payType] , [ReturnAmount] ,[changeAmount] ,[dueAmount]
			,[note] ,[ShopId] ,[CustID] ,[CustName] , [CustContact] ,[ServedBy],trxtype , SalesInvoiceID) 
			values 
			(@ReturnQty, @Subtotal ,@Vat ,@totalReturnable ,@payType ,@ReturnAmount ,@changeAmount ,@dueAmount
			,@note, @ShopId ,@CustID ,@CustName ,@CustContact,@ServedBy , 'POS', @SalesInvoiceID) 
			
			
						
			----- first time single payment info record
			--DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_ReturnPayment order by ID desc)	
			--insert into   tbl_salespaid
			--([SP_ID] ,[trxtype] ,[paydate] ,[paidamount] ,[paytype]  ,[receivedby])
			--	values
			--(@spid ,'POS' ,CONVERT(date, GETDATE(),120) ,(@paidAmount  - @changeAmount)  ,@payType  ,@ServedBy )
	
	
		--- Tracking Log  Sales and order
		DECLARE @spid varchar(30) = (SELECT  top 1    ID  as ID  FROM tbl_ReturnPayment order by ID desc)
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@ServedBy , 'POS' + ' Return Payment |  Invoice No: ' + @spid )
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_ReturnItems]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_ReturnItems]
	   @Code		varchar(150)
	  ,@ItemName	varchar(450)
	  ,@Qty			decimal(18,2)
	  ,@Price		decimal(18,2)
	  ,@Disc 		decimal(18,2)
	  ,@Total		decimal(18,2)	
	  ,@InvoiceNoOutPut varchar(150) output   
 
AS
BEGIN		
   SET NOCOUNT ON;		
		
			insert into  tbl_Return
			([ItemCode],[ItemName]  ,[Qty] , [Price], [DiscRate] ,[total],[RP_ID]) 
			values (@Code, @ItemName ,@Qty ,@Price , @Disc ,@Total , 
			isnull((Select top 1 ID + 1  from tbl_ReturnPayment order by ID desc),4001))				
			 
			
			--Item Quantity Increase  -- for return Policy 
			Update    tbl_Item	 set 
			ItemQty = (select sum(ItemQty) from  tbl_Item where ItemCode = @Code) + @Qty 
			where ItemCode = @Code
			
			-- This is output Parameter send to Invoice number for POS printing Page
			select	@InvoiceNoOutPut = isnull((Select top 1 ID + 1  from tbl_ReturnPayment order by ID desc), 4001)
			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Items]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft
		---We all need people who will give us feedback. That's how we improve.
 

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Items]
		 @ItemCode			varchar(450)
		,@ItemName			varchar(450)
		,@PurchasePrice		decimal(18,2)	
		,@RetailPrice		decimal(18,2)	
		--,@ItemQty			decimal(18,2)	
		,@ItemCategory		varchar(150)		
		,@Discount			decimal(18,2)	
		,@LogBy 			varchar(150)
		,@Itemphoto 		varchar(350)	 
		,@Description  		varchar(499)   
AS
BEGIN	
		Insert into		 tbl_Item 
		([ItemCode],[ItemName],[PurchasePrice] ,[RetailPrice], [ItemCategory], [Discount],[LogBy],[Photo],[Description])
		values  
		(@ItemCode,@ItemName,@PurchasePrice,@RetailPrice, @ItemCategory,@Discount,@LogBy,@Itemphoto,@Description)	


			--- Tracking Log 
			insert into   tbl_accesslog
			([UserID], [Operations])
			values
			(@LogBy ,   ' Added New Item : ' + @ItemName  )
			 
				 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_HitCounter]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_HitCounter]
	@userID			varchar(150),
	@IPaddress		varchar(150)        
AS
BEGIN	
		Insert into	  tbl_hit_counter	 ([userID], [IPaddress]) 
		values (@userID , @IPaddress)
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Insert_Customers]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_Insert_Customers]
	   @CustName		varchar(250)
      ,@CustPhone		varchar(250)
      ,@CustEmail		varchar(250)
      ,@CustAddress		varchar(450)
      ,@CustType		varchar(150) 
      ,@DiscountRate	decimal(18,2)
      ,@LogBy			varchar(150)
	  ,@CustID			varchar(250)
	  ,@CustPassword	varchar(250)	     
AS
BEGIN	
		Insert into	 tbl_Customer 
		([CustName] ,[CustPhone],[CustEmail],[CustAddress],[CustType],[DiscountRate] ,[LogBy] ,CustID  , [Password])
Values	(@CustName , @CustPhone ,@CustEmail ,@CustAddress ,@CustType,@DiscountRate ,  @LogBy,@CustID,@CustPassword)
				 
 			 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Inactive_Customer]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create   PROCEDURE [dbo].[SP_POS_Inactive_Customer]
		 @id				bigint	 	 
		,@LastUpdateBy		varchar(150)
	
AS
BEGIN
	Update   tbl_Customer	 set 
    [Status] = 2 , LastUpdateBy = @LastUpdateBy  , LastUpdate = GETDATE()
	Where ID =  @ID
	 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Terminal]    Script Date: 07/08/2017 23:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Terminal] 	 
	@TerminalID   varchar(250)
	 	     
AS
BEGIN
		Update tbl_terminalLocation set [Status] = 2, Lastupdate = GETDATE()
		where TerminalID = @TerminalID 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Item]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Item] 	 
	@Code bigint 
   ,@Lastupdateby	varchar(160) 	     
AS
BEGIN
		Update  tbl_Item  set [Status] = 2, Lastupdateby = 'Del_by: ' + @Lastupdateby , Lastupdate = GETDATE()
		where ItemCode = @Code  
		
		--- Tracking Log 
		insert into   tbl_accesslog
		([UserID], [Operations])
		values
		(@Lastupdateby ,   ' Deleted   Item | code: ' +  CAST(@Code as varchar(450)))	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Delete_Category]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Delete_Category] 	 
	@ID bigint 
	 	     
AS
BEGIN
		Update dbo.tbl_Category set [Status] = 2, Lastupdate = GETDATE()
		where ID = @ID 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersProfileDetails]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[SP_POS_DataBind_UsersProfileDetails]	 
	@UserID varchar(150)	     
AS
BEGIN
			Select  *
			from  tbl_user
			where UserID = @UserID and Status = 1 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersProfile]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_UsersProfile]	 
	@ID bigint	     
AS
BEGIN
		SET NOCOUNT ON;
			SELECT ID, [Fname] + ' ' +  [LName] as EmployeeName  ,[UserPhone] as OfficialPhone  ,[Designation]   ,[Department] , User_Photo , 
			Convert(varchar(11), DateofBirth,113) as DateofBirth ,  Supervisor , Email , 
			UserAddress, [ShopID]    
			FROM  [tbl_user]
			where ID = @ID and Status = 1 	
	 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UsersDetails]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[SP_POS_DataBind_UsersDetails]	 
	@ID bigint	     
AS
BEGIN
			Select  *
			from  tbl_user
			where ID = @ID and Status = 1 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Users]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Users]	 	     
AS
BEGIN
			Select ID , UserID , Fname + '  ' + LName as [Name] , UserPhone as [Contact] ,
			email, UserAddress as [Address] , Designation ,    ShopID    , User_Photo
			,Department
			--, CASE [Role]
			--	WHEN 1 THEN 'Admin'
			--	WHEN 2 THEN 'User'
			--    Else 'NA'
			--  END AS [Role] 		 
			  
			from  tbl_user
			where  Status = 1 and ID not IN(23)
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_UserID]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROCEDURE [dbo].[SP_POS_DataBind_UserID]	 	     
AS
BEGIN
			Select 'All' as UserID 
			union all 
			Select   UserID 	from  tbl_user	where Status = 1	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TerminalList]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TerminalList]	 	     
AS
BEGIN
	Select   TerminalID 'Terminal ID' , Location , Phone , EmailAddress Email ,VatRate as VAT, VATRegistration 'VAT NO'
	from    tbl_terminalLocation
	where [Status] = 1
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TaskNotification]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TaskNotification]	
	    @userID		varchar(150) 
AS
BEGIN			
		Select top 1 task_Description as [Description] 		 
		from   tbl_tasklist	
		where 	 taskto =  @userID or taskto = 'all'
		order by ID desc 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_TaskList]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_TaskList]	
	    @userID		varchar(150) 
AS
BEGIN			
			Select taskfrom as [From],	taskto as [To], task_title as [Title] ,	task_Description as [Description],		convert(varchar(50),taskdate,109) as [Date]	 		 
			from   tbl_tasklist	
			where 	 taskto =  @userID or taskto = 'all'
			order by 1 desc 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ShopIdDDL]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_ShopIdDDL]	 	     
AS
BEGIN
	Select  Distinct	  TerminalID
	from    tbl_terminalLocation	
	where [Status] = 1
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_SettingsUpdate]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE PROCEDURE [dbo].[SP_POS_DataBind_SettingsUpdate]      
		-- @ID				bigint	 
		 	     
AS
BEGIN
			Select * from tbl_settings
			where ID = 1			
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_SalesItem_PP]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_SalesItem_PP]	--  DataBind team in Printpage	
	@InvoiceID bigint      
AS
BEGIN
SET NOCOUNT ON;
			Select  ItemName + ' ' +  ItemCode , Qty  , total
			from     tbl_sales
			where  [SP_ID]	= @InvoiceID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ReturnItemBarCodeSearch]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

Create    PROCEDURE [dbo].[SP_POS_DataBind_ReturnItemBarCodeSearch]	--  DataBind team in Sales Register barcode scan	
	@ItemCode  varchar(250)    
AS
BEGIN
	SET NOCOUNT ON;
			Select   ItemCode as [Code],	
			ItemName as [Items Name] , RetailPrice	 as Price,Discount as [Disc%],
			 CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total
			from    tbl_Item
			where  ItemCode = @ItemCode or ItemName like '%'+ @ItemCode +'%'   
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ItemSearch]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft


CREATE   PROCEDURE [dbo].[SP_POS_DataBind_ItemSearch]	
	@value 	varchar(500)     
AS
BEGIN
			Select  ID  	, ItemCode as [Code] ,	ItemName as [ItemName],ItemQty as [Qty], PurchasePrice as [Purchase Price] ,
			RetailPrice as [Retail Price],   Discount as [Discount %],
			CAST((RetailPrice -((RetailPrice * Discount) / 100))* ItemQty	as numeric(18,2))   as Total	,
			ItemCategory as [Category] ,
			CONVERT(VARCHAR(24),Lastupdate,113)    as Lastupdate,  Lastupdateby as [Updated by] ,Photo
			from    tbl_Item
			where ItemCode like '%' + @value + '%'  or
				  ItemName like '%' + @value + '%' or  ItemCategory like '%' + @value + '%'  
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_ItemBarCodeSearch]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_ItemBarCodeSearch]	--  DataBind team in Sales Register barcode scan	
	@ItemCode  varchar(250)    
AS
BEGIN
	SET NOCOUNT ON;
			Select   ItemCode as [Code],	
			ItemName as [Items Name] , RetailPrice	 as Price,Discount as [Disc%],
			 CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total
			from    tbl_Item
			where  ItemCode = @ItemCode or ItemName like '%'+ @ItemCode +'%'  and [Status] = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item_SR]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft
--- http://codecanyon.net/user/dynamicsoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_Item_SR]	--  DataBind team in Sales Register 	
	@category varchar(150)      
AS
BEGIN
SET NOCOUNT ON;
	IF(@category = 'All')
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total , Photo			  
					from    tbl_Item
					where   Status = 1 	
					--order by Logtime	
								 
		END
	ELSE
		BEGIN
					Select ID, ItemCode as [Code],	
					ItemName, CONVERT(int, ItemQty) as [Qty], RetailPrice	 as Price,Discount as [Disc]
					 , CAST((RetailPrice -((RetailPrice * Discount) / 100))	as numeric(18,2)) as Total ,  Photo			  
					from    tbl_Item
					where ItemCategory = @category and Status = 1 		
								 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item_Details]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE    PROCEDURE [dbo].[SP_POS_DataBind_Item_Details]	 
	@ID bigint 	     
AS
BEGIN
		 	Select * from tbl_Item	
		 	where ID = @ID
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Item]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
--- Company Name:   DynamicSoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Item]	 	     
AS
BEGIN
			Select  ID  	, ItemCode as [Code] ,	ItemName as [Item Name],ItemQty as [Qty], PurchasePrice as [Purchase Price] ,
			RetailPrice as [Retail Price],   Discount as [Discount %],
			CAST((RetailPrice -((RetailPrice * Discount) / 100))* ItemQty	as numeric(18,2))   as Total	,
			ItemCategory as [Category] ,
			 CONVERT(varchar(11),Lastupdate,109)    as Lastupdate,  Lastupdateby as [Updated by] ,Photo
			from    tbl_Item
			where Status = 1 
			order by ID desc 		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CustomersEvent]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CustomersEvent]	
	@CustName	varchar(50)     
AS
BEGIN			
			Select ID,  CustPhone , CustAddress		 
			from  tbl_Customer
			where CustName = @CustName and Status = 1 
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Customers_name]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author:			Tuaha Mohammad 
		--- Company Name:   DynamicSoft    
		--- http://www.citputer.com/
		--- http://codecanyon.net/user/dynamicsoft

CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Customers_name]	 	     
AS
BEGIN	
			Select distinct 'Guest' as Name , '0' as ID
			union all 
			Select  CustName as Name 	, ID	 
			from  tbl_Customer
			where Status = 1	
		 					 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_Customers]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_Customers]	 	     
AS
BEGIN
			Select   ID as [ID],	CustName as Name,	CustPhone as Contact,
			CustEmail as Email,	CustAddress as [Address] ,
			CustType as [Customer Type],DiscountRate as [Dsicount %] ,
			 CustID , [password],
				Case [Status] when   1 then 'active'
							  when   2 then 'Inactive'
							  ELSE 'Ban' 
				END as Status		 
			from  tbl_Customer 
			where Status = 1 	
			order   by  ID desc 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryList]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryList]	 	     
AS
BEGIN
	Select  ID,	[ItemCategory] as [Category Name], [LogBy] as [Posted by], CONVERT(VARCHAR(24),Lastupdate,6) as Lastupdate ,	[Status]
	from    tbl_Category
	where Status = 1 
	order by 1 desc		
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryDDL_SR]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryDDL_SR]	 	     
AS	 
BEGIN
			Select 'All' as ItemCategory
			Union all
			Select  Distinct	[ItemCategory]  
			from      tbl_Item
			where Status = 1 
 				 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_DataBind_CategoryDDL]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[SP_POS_DataBind_CategoryDDL]	 	     
AS
BEGIN
	Select  Distinct	[ItemCategory]  
	from    tbl_Category
	where Status = 1 
	order by 1 
	 	
						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Add_Terminal]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Add_Terminal]	
		@TerminalID				Varchar(210),  
		@Location				Varchar(210), 
		@Phone					Varchar(210), 
		@EmailAddress			Varchar(210),
		@VatRate				decimal(18,3),
		@VATRegistration		Varchar(210),   
		@logby					Varchar(210)  
AS
BEGIN
		SET NOCOUNT ON;
			insert into   tbl_terminalLocation	
			( TerminalID ,Location, Phone , EmailAddress , VatRate, VATRegistration , LogBy) 
			values 
		    (@TerminalID ,@Location, @Phone , @EmailAddress , @VatRate , @VATRegistration , @LogBy)						 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POS_Add_Category]    Script Date: 07/08/2017 23:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE  [dbo].[SP_POS_Add_Category] 	 
	@category Varchar(210),
	@logby    Varchar(210) 	     
AS
BEGIN
			insert into   tbl_Category	(ItemCategory , LogBy) values (@category , @logby )
			
						 
END
GO
/****** Object:  Default [DF_tbl_UserRole_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_UserRole] ADD  CONSTRAINT [DF_tbl_UserRole_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_UserRole_lastUpdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_UserRole] ADD  CONSTRAINT [DF_tbl_UserRole_lastUpdate]  DEFAULT (getdate()) FOR [lastUpdate]
GO
/****** Object:  Default [DF_tbl_user_DateofBirth]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_DateofBirth]  DEFAULT (getdate()-(8000)) FOR [DateofBirth]
GO
/****** Object:  Default [DF_tbl_user_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_user_Lastupdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_user_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_user_User_Photo]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_user] ADD  CONSTRAINT [DF_tbl_user_User_Photo]  DEFAULT ('User_Photo/man.png') FOR [User_Photo]
GO
/****** Object:  Default [DF_tbl_terminalLocation_CompanyID]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_CompanyID]  DEFAULT ((1)) FOR [CompanyID]
GO
/****** Object:  Default [DF_tbl_terminalLocation_LastUpdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_terminalLocation_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_terminalLocation_status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_terminalLocation] ADD  CONSTRAINT [DF_tbl_terminalLocation_status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_tasklist_taskdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_taskdate]  DEFAULT (getdate()) FOR [taskdate]
GO
/****** Object:  Default [DF_tbl_tasklist_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_tasklist_status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_tasklist] ADD  CONSTRAINT [DF_tbl_tasklist_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_supplier_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_supplier_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_supplier_LastUpdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_supplier] ADD  CONSTRAINT [DF_tbl_supplier_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_settings_LastUpdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_settings] ADD  CONSTRAINT [DF_tbl_settings_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_settings_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_settings] ADD  CONSTRAINT [DF_tbl_settings_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_SalesPayment_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_SalesPayment] ADD  CONSTRAINT [DF_tbl_SalesPayment_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_SalesPayment_Logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_SalesPayment] ADD  CONSTRAINT [DF_tbl_SalesPayment_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_salespaid_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_salespaid] ADD  CONSTRAINT [DF_tbl_salespaid_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_salespaid_logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_salespaid] ADD  CONSTRAINT [DF_tbl_salespaid_logdate]  DEFAULT (getdate()) FOR [logdate]
GO
/****** Object:  Default [DF_tbl_sales_SP_ID]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_SP_ID]  DEFAULT ((1001)) FOR [SP_ID]
GO
/****** Object:  Default [DF_tbl_sales_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_sales_Logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_sales] ADD  CONSTRAINT [DF_tbl_sales_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_ReturnPayment_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_ReturnPayment] ADD  CONSTRAINT [DF_tbl_ReturnPayment_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_ReturnPayment_Logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_ReturnPayment] ADD  CONSTRAINT [DF_tbl_ReturnPayment_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_Return_RP_ID]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_RP_ID]  DEFAULT ((1001)) FOR [RP_ID]
GO
/****** Object:  Default [DF_tbl_Return_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_Return_Logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Return] ADD  CONSTRAINT [DF_tbl_Return_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
/****** Object:  Default [DF_tbl_purchase_payment_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_purchase_payment] ADD  CONSTRAINT [DF_tbl_purchase_payment_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_purchase_logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_purchase] ADD  CONSTRAINT [DF_tbl_purchase_logtime]  DEFAULT (getdate()) FOR [logtime]
GO
/****** Object:  Default [DF_tbl_Page_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Page] ADD  CONSTRAINT [DF_tbl_Page_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Item_ItemQty]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_ItemQty]  DEFAULT ((0)) FOR [ItemQty]
GO
/****** Object:  Default [DF_tbl_Item_Tax]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Tax]  DEFAULT ((0)) FOR [Tax]
GO
/****** Object:  Default [DF_tbl_Item_Discount]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Discount]  DEFAULT ((0)) FOR [Discount]
GO
/****** Object:  Default [DF_tbl_Item_Photo]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Photo]  DEFAULT ('ItemsPhoto/item.png') FOR [Photo]
GO
/****** Object:  Default [DF_tbl_Item_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Item_Lastupdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_Item_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Item] ADD  CONSTRAINT [DF_tbl_Item_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_hit_counter_datetime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_datetime]  DEFAULT (getdate()) FOR [datetime]
GO
/****** Object:  Default [DF_tbl_hit_counter_timelog]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_timelog]  DEFAULT (getdate()) FOR [timelog]
GO
/****** Object:  Default [DF_tbl_hit_counter_count]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_hit_counter] ADD  CONSTRAINT [DF_tbl_hit_counter_count]  DEFAULT ((1)) FOR [count]
GO
/****** Object:  Default [DF_tbl_empSalary_saldate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_saldate]  DEFAULT (getdate()) FOR [saldate]
GO
/****** Object:  Default [DF_tbl_empSalary_logdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_logdate]  DEFAULT (getdate()) FOR [logdate]
GO
/****** Object:  Default [DF_tbl_empSalary_status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_empSalary] ADD  CONSTRAINT [DF_tbl_empSalary_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_email_maildate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_email] ADD  CONSTRAINT [DF_tbl_email_maildate]  DEFAULT (getdate()) FOR [maildate]
GO
/****** Object:  Default [DF_tbl_email_status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_email] ADD  CONSTRAINT [DF_tbl_email_status]  DEFAULT ((1)) FOR [status]
GO
/****** Object:  Default [DF_tbl_Customer_DiscountRate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_DiscountRate]  DEFAULT ((0)) FOR [DiscountRate]
GO
/****** Object:  Default [DF_tbl_Customer_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Customer_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_Customer_LastUpdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Customer] ADD  CONSTRAINT [DF_tbl_Customer_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
/****** Object:  Default [DF_tbl_Category_Status]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_Category_Logtime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Logtime]  DEFAULT (getdate()) FOR [Logtime]
GO
/****** Object:  Default [DF_tbl_Category_Lastupdate]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_Category] ADD  CONSTRAINT [DF_tbl_Category_Lastupdate]  DEFAULT (getdate()) FOR [Lastupdate]
GO
/****** Object:  Default [DF_tbl_accesslog_datetime]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_accesslog] ADD  CONSTRAINT [DF_tbl_accesslog_datetime]  DEFAULT (getdate()) FOR [datetime]
GO
/****** Object:  Default [DF_tbl_accesslog_timelog]    Script Date: 07/08/2017 23:58:19 ******/
ALTER TABLE [dbo].[tbl_accesslog] ADD  CONSTRAINT [DF_tbl_accesslog_timelog]  DEFAULT (getdate()) FOR [timelog]
GO
